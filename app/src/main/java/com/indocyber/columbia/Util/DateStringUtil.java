package com.indocyber.columbia.Util;

import android.text.TextUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by Frendi on 31/07/2017.
 */

public class DateStringUtil {

    public static final String TAG = "DateStringUtil";

    public static final String DATE_FORMAT = "dd/MM/yyyy";

    private static String formatting(String inputFormat, String outputFormat, String source) {
        try {
            if (TextUtils.isEmpty(source)) {
                return "";
            }
            SimpleDateFormat simpleInputFormat = new SimpleDateFormat(inputFormat);
            simpleInputFormat.setTimeZone(getTimeZone());
            SimpleDateFormat simpleOutputFormat = new SimpleDateFormat(outputFormat);
            simpleOutputFormat.setTimeZone(getTimeZone());
            return simpleOutputFormat.format(simpleInputFormat.parse(source));
        } catch (ParseException e) {
            Log.e(TAG, "formatting: ", e);
            return source;
        }
    }

    public static TimeZone getTimeZone() {
        return TimeZone.getTimeZone("Asia/Jakarta");
    }

}
