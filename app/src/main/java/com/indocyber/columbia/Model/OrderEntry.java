package com.indocyber.columbia.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class OrderEntry extends RealmObject {

    private String DATA_MAP_NAME;
    @PrimaryKey
    private String ORDER_ID;

    /*Part1*/
    private String NAME = "";
    private String MOTHER_NAME = "";
    private String NICKNAME = "";
    private String NPWP_NO = "";
    private String IDENTIFY_NO = "";
    private String GENDER = "";
    private String FIRST_TITLE = "";
    private String MARITAL_STATUS = "";
    private String PERS_IDENTIFY_TYPE = "";
    private String JOB_TITLE = ""; //New
    private String DEPENDANTS = ""; //New

    /*Part2*/
    private String ELECTRICITY_NO = ""; //new
    private String BIRTH_PLACE = "";
    private String CUST_BIRTH_DATE = "";
    private String PROVINCE = "";
    private String RT = "";
    private String RW = "";
    private String KELURAHAN = "";
    private String KECAMATAN = "";
    private String KABKODYA = "";
    private String PERS_RELIGION = "";
    private String ADDRESS_KTP = "";

    /*Part3*/
    private String SAME_CUSTOMER_ID_ADDRESS = "";
    private String CUSTOMER_RESI_POSTCODE = "";
    private String SANDIDATIII = "";
    private String CUSTOMER_RESI_REGENCY = "";
    private String CUSTOMER_RESI_RT = "";
    private String CUSTOMER_RESI_RW = "";
    private String CUSTOMER_RESI_VILLAGE = "";
    private String CUSTOMER_RESI_CITY = "";
    private String CUSTOMER_RESI_ADDRESS = "";
    private String CUSTOMER_RESI_DISTRICT = "";
    private String CUSTOMER_RESI_PROVINCE = "";
    private String CUSTOMER_RESI_POSCODE_OTHER = ""; //New
    private String SANDIDATITWO_OTHER = ""; //New

    /*Part4*/
    private String CUSTOMER_MAIL_ADDRESS = ""; //New
    private String HOME_STATUS = "";
    private String CUSTOMER_EDUCATION = "";
    private String TELEPHONE_AREA = "";
    private String TELEPHONE_NUMBER = "";
    private String HANDPHONE_PREFIX = "";
    private String HANDPHONE_NUMBER = "";
    private String DURATION_STAY_YEAR = "";
    private String DURATION_STAY_MONTH = "";

    /*Part5*/
    private String BUSSINESS_ESTABLISHED_SINCE = ""; //new
    private String BUSSINESS_TYPE = ""; //new
    private String OCCUPATION = "";
    private String OFFICE_PROVINCE = "";
    private String OFFICE_CITY = "";
    private String OFFICE_NAME = "";
    private String JOBADDRESS = "";
    private String JOBPOSTALCODE = "";
    private String OFFICE_RT = ""; //New
    private String OFFICE_RW = ""; //New
    private String OFFICE_KAB = ""; //New
    private String OFFICE_TELP_AREACODE = ""; //New
    private String OFFICE_TELP_NO = ""; //New

    /*Part6*/
    private String FAX_AREACODE = ""; //New
    private String FAX_NO = ""; //New
    private String LENGHOFWORK = "";
    private String DURATION_JOB_MONTH = "";

    /*Part7*/
    private String SPOUSE_NAME = "";
    private String SPOUSE_NICK_NAME = ""; //New
    private String SPOUSE_TITLE = ""; //New
    private String SPOUSE_ID_TYPE = "";
    private String SPOUSE_ID_NO = "";
    private String SPOUSE_GENDER = ""; //New
    private String SPOUSE_BIRTH_PLACE = ""; //New
    private String SPOUSE_BIRTH_DATE = ""; //New
    private String SPOUSE_RELIGION = ""; //New

    /*Part8*/
    private String GROSS_FIX_INCOME = "0";
    private String ADDITIONAL_INCOME = "0";
    private String ADD_INCOME_SOURCE = "";

    /*Part9*/
    private String FAM_CONTACT = ""; //New
    private String FAM_NAME = ""; //New
    private String FAM_ADDRESS = ""; //New
    private String FAM_RT = ""; //New
    private String FAM_RW = ""; //New
    private String FAM_KAB = ""; //New
    private String FAM_PROVINCE = ""; //New
    private String FAM_POSCODE = ""; //New
    private String FAM_TELP_AREACODE = ""; //New
    private String FAM_TELP_NO = ""; //New
    private String FAM_RELATION = ""; //New

    /*Part10*/
    private String CHILD_NAME = "";
    private String CHILD_SCHOOL_NAME = "";
    private String CHILD_SCHOOL_ADDRESS = "";
    private String CHILD_RT = "";
    private String CHILD_RW = "";
    private String CHILD_KELURAHAN = "";
    private String CHILD_KECAMATAN = "";
    private String CHILD_CITY = "";
    private String CHILD_POSCODE = "";
    private String CHILD_PHONEONEAREA = "";
    private String CHILD_PHONEONE = "";
    private String CHILD_PHONETWOAREA = "";
    private String CHILD_PHONETWO = "";

    /*Part11*/
    private String PRODUCT_CODE = "";
    private String ALREADY_TAKE = ""; //new
    private String SALES_TYPE = ""; //new
    private String PRODUCT_CONDITION = ""; //new

    /*Part12*/
    private String BRANCH = "";
    private String DEALER_NAME = "";
    private String CITY = "";

    /*Part13*/
    private String BRAND = "";
    private String MODEL = "";
    private String TYPE = "";
    private String ORDER_DATE = "";
    private String CASH_PRICE = "0";
    private String DOWNPAYMENT_AMT = "0";
    private String INSTALLMENT_TYPE = "";
    private String RENT_PAYMENT_TYPE = "";
    private String INSTALLMENT_AMT = "0";
    private String TENOR = "0";
    private String JUMLAH = "0"; //new

    /*Part14*/
    private String SPOUSE_MARITAL_STATUS = ""; //new
    private String JENIS_PEKERJAAN = "";
    private String SPOUSE_OFFICE_NAME = ""; //new
    private String SPOUSE_ADDRESS = ""; //new
    private String SPOUSE_KAB = ""; //new
    private String SPOUSE_POSCODE = ""; //new
    private String SPOUSE_BUSSINESS_FIELD = ""; //new
    private String SPOUSE_TITLE_P14 = ""; //new
    private String SPOUSE_DURATION_OF_WORK_YEARS = ""; //new
    private String SPOUSE_DURATION_OF_WORK_MONTH = ""; //new
    private String EARNING_EACH_MONTH = "0"; //new

    /*Part15*/
    private String HAVE_ROUTINE_INCOME = ""; //new
    private String INCOME_TYPE = ""; //new
    private String AVERAGE_INCOME_EACH_MONTH = ""; //new

    /*Part16*/
    private String SECOND_HOUSE = ""; //new
    private String CAR = ""; //New
    private String OTHER_ASSET = ""; //New
    private String ASSET_DESCRIPTION_OTHER = ""; //new
    private String ASSET_PRICE = ""; //new
    private String ASSET_DESCRIPTION = ""; //new

    /*Part17*/
    private String HAVE_BCA_ACCOUNT = ""; //New
    private String HAVE_OTHER_CREDIT_CARD = ""; //New
    private String ACCOUNT_LATTER = ""; //New
    private String HAVE_SAVING = ""; //New
    private String HAVE_DEPOSITO = ""; //New
    private String FASILITY_TYPE = ""; //new
    private String OTHER_BANK_OR_FINACE = ""; //New
    private String INSTALLMENT = ""; //new

    /*Part18*/
    private String HAVE_CREDIT_CARD = ""; //New
    private String BANK_OF_CREDIT_CARD = ""; //New
    private String CARD_TYPE = ""; //New
    private String CARD_SERVICE = ""; //New
    private String CARD_LIMIT = ""; //New
    private String CARD_AVERAGE_USAGE = ""; //New

    /*Part19*/
    private String PENGHASILAN_TETAP = "";
    private String PENGHASILAN_NON_TETAP = ""; //New
    private String TOTAL_PENDAPATAN = ""; //New
    private String PENGELUARAN_RUTIN = ""; //New
    private String PENGELUARAN_LAIN = ""; //New
    private String PENGELUARAN_TOTAL = ""; //New

    /*Part20*/
    private String HOUSE = ""; //new
    private String OLD_OCCUPY = ""; //new
    private String BUILDING_CONDITION = ""; //new
    private String BUILDING_AREA = ""; //new
    private String LAND_AREA = ""; //new
    private String ELECTRICITY = ""; //new
    private String WATER = ""; //new
    private String TRANSPORT = ""; //new
    private String FURNITURE = ""; //new
    private String ELECTRONIC = ""; //new
    private String BUSSINESS_TYPE_P20 = ""; //new

    /*Part21*/
    private String NEIGHBOARD = ""; //new
    private String HOBBY = ""; //new
    private String OPEN_GIVE_INFORMATION = ""; //new
    private String RELIGIUS = ""; //new
    private String ACCEPT_CREDIT_RULE = ""; //new
    private String ETIKA_WHEN_SURVEY = ""; //new

    /*PArt22*/
    private String SURVEY_DATE = ""; //new
    private String SURVEY_PLACE = ""; //new
    private String STATUS_KUNJUNGAN = "";
    private String MEET_WITH = ""; //new

    /*Part23*/
    private String CUST_OLD = ""; //new
    private String CUST_HEALTY = ""; //new
    private String CUST_HEALTY_SPOUSEORCHILD = ""; //new
    private String HEALTY_GUAR = ""; //new
    private String HOUSE_CONDITION = ""; //new
    private String FURNITURE_CONDITION = ""; //new
    private String TRANSORT_CONDITION = ""; //new
    private String BUSSINESS_CONDITION = ""; //new
    private String GUAR_LETTER = ""; //new
    private String GUARD_RELATION = ""; //new

    /*Part24*/
    private String DESCRIPTION = "";
    private String HOUSE_FILE = ""; //new
    private String DOCUMENT_FILE = ""; //new
    private String CONSUMENT_FILE = ""; //new
    private String GPS = ""; //new
    private String GPS_DESCRIPTION = ""; //new
    //==============================================//
    private String ADD_INCOME_SRC;
    private String MODULE_ID;
    private String ITEM_TYPE;
    private String ITEM_KEY;
    private String USER_ID;
    private String STATUS;
    private String STATUS_UPDATE;
    private String MOBILE_STATUS;
    private String TIMESTAMPS;
    private String LAST_TRX;
    private String BRANCH_ID;
    private String BLANK;
    private String PERSPOSTALCODE;
    private String FAM_IDENTIFY_TYPE;
    private String FAM_BIRTH_DATE;
    private String GUAR_ADDRESS;
    private String GUAR_RELIGION;
    private String PENJ_ADDRESS;
    private String YEAR;
    private String FLAG_DELIVER;
    private String FLAG_UPDATE_CLOSE;
    private String INDUSTRY_CODE;
    private String TGL_DELIVERED;
    private String TGL_CLOSE;
    private String TGL_CLOSE_SEND;
    private String DOWNPAYMENT_PERC;
    private String ASSET_NO;
    private String ASSET_CONDITION;
    private String INVOICE_CORRESPONDENCE_NO;

    //======================Start Setter Getter====================//


    public String getSECOND_HOUSE() {
        return SECOND_HOUSE;
    }

    public void setSECOND_HOUSE(String SECOND_HOUSE) {
        this.SECOND_HOUSE = SECOND_HOUSE;
    }

    public String getCAR() {
        return CAR;
    }

    public void setCAR(String CAR) {
        this.CAR = CAR;
    }

    public String getOTHER_ASSET() {
        return OTHER_ASSET;
    }

    public void setOTHER_ASSET(String OTHER_ASSET) {
        this.OTHER_ASSET = OTHER_ASSET;
    }

    public String getJOB_TITLE() {
        return JOB_TITLE;
    }

    public void setJOB_TITLE(String JOB_TITLE) {
        this.JOB_TITLE = JOB_TITLE;
    }

    public String getDEPENDANTS() {
        return DEPENDANTS;
    }

    public void setDEPENDANTS(String DEPENDANTS) {
        this.DEPENDANTS = DEPENDANTS;
    }

    public String getELECTRICITY_NO() {
        return ELECTRICITY_NO;
    }

    public void setELECTRICITY_NO(String ELECTRICITY_NO) {
        this.ELECTRICITY_NO = ELECTRICITY_NO;
    }

    public String getCUSTOMER_RESI_POSCODE_OTHER() {
        return CUSTOMER_RESI_POSCODE_OTHER;
    }

    public void setCUSTOMER_RESI_POSCODE_OTHER(String CUSTOMER_RESI_POSCODE_OTHER) {
        this.CUSTOMER_RESI_POSCODE_OTHER = CUSTOMER_RESI_POSCODE_OTHER;
    }

    public String getSANDIDATITWO_OTHER() {
        return SANDIDATITWO_OTHER;
    }

    public void setSANDIDATITWO_OTHER(String SANDIDATITWO_OTHER) {
        this.SANDIDATITWO_OTHER = SANDIDATITWO_OTHER;
    }

    public String getCUSTOMER_MAIL_ADDRESS() {
        return CUSTOMER_MAIL_ADDRESS;
    }

    public void setCUSTOMER_MAIL_ADDRESS(String CUSTOMER_MAIL_ADDRESS) {
        this.CUSTOMER_MAIL_ADDRESS = CUSTOMER_MAIL_ADDRESS;
    }

    public String getBUSSINESS_ESTABLISHED_SINCE() {
        return BUSSINESS_ESTABLISHED_SINCE;
    }

    public void setBUSSINESS_ESTABLISHED_SINCE(String BUSSINESS_ESTABLISHED_SINCE) {
        this.BUSSINESS_ESTABLISHED_SINCE = BUSSINESS_ESTABLISHED_SINCE;
    }

    public String getBUSSINESS_TYPE() {
        return BUSSINESS_TYPE;
    }

    public void setBUSSINESS_TYPE(String BUSSINESS_TYPE) {
        this.BUSSINESS_TYPE = BUSSINESS_TYPE;
    }

    public String getOFFICE_RT() {
        return OFFICE_RT;
    }

    public void setOFFICE_RT(String OFFICE_RT) {
        this.OFFICE_RT = OFFICE_RT;
    }

    public String getOFFICE_RW() {
        return OFFICE_RW;
    }

    public void setOFFICE_RW(String OFFICE_RW) {
        this.OFFICE_RW = OFFICE_RW;
    }

    public String getOFFICE_KAB() {
        return OFFICE_KAB;
    }

    public void setOFFICE_KAB(String OFFICE_KAB) {
        this.OFFICE_KAB = OFFICE_KAB;
    }

    public String getOFFICE_TELP_AREACODE() {
        return OFFICE_TELP_AREACODE;
    }

    public void setOFFICE_TELP_AREACODE(String OFFICE_TELP_AREACODE) {
        this.OFFICE_TELP_AREACODE = OFFICE_TELP_AREACODE;
    }

    public String getOFFICE_TELP_NO() {
        return OFFICE_TELP_NO;
    }

    public void setOFFICE_TELP_NO(String OFFICE_TELP_NO) {
        this.OFFICE_TELP_NO = OFFICE_TELP_NO;
    }

    public String getFAX_AREACODE() {
        return FAX_AREACODE;
    }

    public void setFAX_AREACODE(String FAX_AREACODE) {
        this.FAX_AREACODE = FAX_AREACODE;
    }

    public String getFAX_NO() {
        return FAX_NO;
    }

    public void setFAX_NO(String FAX_NO) {
        this.FAX_NO = FAX_NO;
    }

    public String getSPOUSE_NICK_NAME() {
        return SPOUSE_NICK_NAME;
    }

    public void setSPOUSE_NICK_NAME(String SPOUSE_NICK_NAME) {
        this.SPOUSE_NICK_NAME = SPOUSE_NICK_NAME;
    }

    public String getSPOUSE_TITLE() {
        return SPOUSE_TITLE;
    }

    public void setSPOUSE_TITLE(String SPOUSE_TITLE) {
        this.SPOUSE_TITLE = SPOUSE_TITLE;
    }

    public String getSPOUSE_GENDER() {
        return SPOUSE_GENDER;
    }

    public void setSPOUSE_GENDER(String SPOUSE_GENDER) {
        this.SPOUSE_GENDER = SPOUSE_GENDER;
    }

    public String getSPOUSE_BIRTH_PLACE() {
        return SPOUSE_BIRTH_PLACE;
    }

    public void setSPOUSE_BIRTH_PLACE(String SPOUSE_BIRTH_PLACE) {
        this.SPOUSE_BIRTH_PLACE = SPOUSE_BIRTH_PLACE;
    }

    public String getSPOUSE_BIRTH_DATE() {
        return SPOUSE_BIRTH_DATE;
    }

    public void setSPOUSE_BIRTH_DATE(String SPOUSE_BIRTH_DATE) {
        this.SPOUSE_BIRTH_DATE = SPOUSE_BIRTH_DATE;
    }

    public String getSPOUSE_RELIGION() {
        return SPOUSE_RELIGION;
    }

    public void setSPOUSE_RELIGION(String SPOUSE_RELIGION) {
        this.SPOUSE_RELIGION = SPOUSE_RELIGION;
    }

    public String getFAM_CONTACT() {
        return FAM_CONTACT;
    }

    public void setFAM_CONTACT(String FAM_CONTACT) {
        this.FAM_CONTACT = FAM_CONTACT;
    }

    public String getFAM_NAME() {
        return FAM_NAME;
    }

    public void setFAM_NAME(String FAM_NAME) {
        this.FAM_NAME = FAM_NAME;
    }

    public String getFAM_ADDRESS() {
        return FAM_ADDRESS;
    }

    public void setFAM_ADDRESS(String FAM_ADDRESS) {
        this.FAM_ADDRESS = FAM_ADDRESS;
    }

    public String getFAM_RT() {
        return FAM_RT;
    }

    public void setFAM_RT(String FAM_RT) {
        this.FAM_RT = FAM_RT;
    }

    public String getFAM_RW() {
        return FAM_RW;
    }

    public void setFAM_RW(String FAM_RW) {
        this.FAM_RW = FAM_RW;
    }

    public String getFAM_KAB() {
        return FAM_KAB;
    }

    public void setFAM_KAB(String FAM_KAB) {
        this.FAM_KAB = FAM_KAB;
    }

    public String getFAM_PROVINCE() {
        return FAM_PROVINCE;
    }

    public void setFAM_PROVINCE(String FAM_PROVINCE) {
        this.FAM_PROVINCE = FAM_PROVINCE;
    }

    public String getFAM_POSCODE() {
        return FAM_POSCODE;
    }

    public void setFAM_POSCODE(String FAM_POSCODE) {
        this.FAM_POSCODE = FAM_POSCODE;
    }

    public String getFAM_TELP_AREACODE() {
        return FAM_TELP_AREACODE;
    }

    public void setFAM_TELP_AREACODE(String FAM_TELP_AREACODE) {
        this.FAM_TELP_AREACODE = FAM_TELP_AREACODE;
    }

    public String getFAM_TELP_NO() {
        return FAM_TELP_NO;
    }

    public void setFAM_TELP_NO(String FAM_TELP_NO) {
        this.FAM_TELP_NO = FAM_TELP_NO;
    }

    public String getFAM_RELATION() {
        return FAM_RELATION;
    }

    public void setFAM_RELATION(String FAM_RELATION) {
        this.FAM_RELATION = FAM_RELATION;
    }

    public String getALREADY_TAKE() {
        return ALREADY_TAKE;
    }

    public void setALREADY_TAKE(String ALREADY_TAKE) {
        this.ALREADY_TAKE = ALREADY_TAKE;
    }

    public String getSALES_TYPE() {
        return SALES_TYPE;
    }

    public void setSALES_TYPE(String SALES_TYPE) {
        this.SALES_TYPE = SALES_TYPE;
    }

    public String getPRODUCT_CONDITION() {
        return PRODUCT_CONDITION;
    }

    public void setPRODUCT_CONDITION(String PRODUCT_CONDITION) {
        this.PRODUCT_CONDITION = PRODUCT_CONDITION;
    }

    public String getJUMLAH() {
        return JUMLAH;
    }

    public void setJUMLAH(String JUMLAH) {
        this.JUMLAH = JUMLAH;
    }

    public String getSPOUSE_MARITAL_STATUS() {
        return SPOUSE_MARITAL_STATUS;
    }

    public void setSPOUSE_MARITAL_STATUS(String SPOUSE_MARITAL_STATUS) {
        this.SPOUSE_MARITAL_STATUS = SPOUSE_MARITAL_STATUS;
    }

    public String getSPOUSE_OFFICE_NAME() {
        return SPOUSE_OFFICE_NAME;
    }

    public void setSPOUSE_OFFICE_NAME(String SPOUSE_OFFICE_NAME) {
        this.SPOUSE_OFFICE_NAME = SPOUSE_OFFICE_NAME;
    }

    public String getSPOUSE_ADDRESS() {
        return SPOUSE_ADDRESS;
    }

    public void setSPOUSE_ADDRESS(String SPOUSE_ADDRESS) {
        this.SPOUSE_ADDRESS = SPOUSE_ADDRESS;
    }

    public String getSPOUSE_KAB() {
        return SPOUSE_KAB;
    }

    public void setSPOUSE_KAB(String SPOUSE_KAB) {
        this.SPOUSE_KAB = SPOUSE_KAB;
    }

    public String getSPOUSE_POSCODE() {
        return SPOUSE_POSCODE;
    }

    public void setSPOUSE_POSCODE(String SPOUSE_POSCODE) {
        this.SPOUSE_POSCODE = SPOUSE_POSCODE;
    }

    public String getSPOUSE_BUSSINESS_FIELD() {
        return SPOUSE_BUSSINESS_FIELD;
    }

    public void setSPOUSE_BUSSINESS_FIELD(String SPOUSE_BUSSINESS_FIELD) {
        this.SPOUSE_BUSSINESS_FIELD = SPOUSE_BUSSINESS_FIELD;
    }

    public String getSPOUSE_TITLE_P14() {
        return SPOUSE_TITLE_P14;
    }

    public void setSPOUSE_TITLE_P14(String SPOUSE_TITLE_P14) {
        this.SPOUSE_TITLE_P14 = SPOUSE_TITLE_P14;
    }

    public String getSPOUSE_DURATION_OF_WORK_YEARS() {
        return SPOUSE_DURATION_OF_WORK_YEARS;
    }

    public void setSPOUSE_DURATION_OF_WORK_YEARS(String SPOUSE_DURATION_OF_WORK_YEARS) {
        this.SPOUSE_DURATION_OF_WORK_YEARS = SPOUSE_DURATION_OF_WORK_YEARS;
    }

    public String getSPOUSE_DURATION_OF_WORK_MONTH() {
        return SPOUSE_DURATION_OF_WORK_MONTH;
    }

    public void setSPOUSE_DURATION_OF_WORK_MONTH(String SPOUSE_DURATION_OF_WORK_MONTH) {
        this.SPOUSE_DURATION_OF_WORK_MONTH = SPOUSE_DURATION_OF_WORK_MONTH;
    }

    public String getEARNING_EACH_MONTH() {
        return EARNING_EACH_MONTH;
    }

    public void setEARNING_EACH_MONTH(String EARNING_EACH_MONTH) {
        this.EARNING_EACH_MONTH = EARNING_EACH_MONTH;
    }

    public String getHAVE_ROUTINE_INCOME() {
        return HAVE_ROUTINE_INCOME;
    }

    public void setHAVE_ROUTINE_INCOME(String HAVE_ROUTINE_INCOME) {
        this.HAVE_ROUTINE_INCOME = HAVE_ROUTINE_INCOME;
    }

    public String getINCOME_TYPE() {
        return INCOME_TYPE;
    }

    public void setINCOME_TYPE(String INCOME_TYPE) {
        this.INCOME_TYPE = INCOME_TYPE;
    }

    public String getAVERAGE_INCOME_EACH_MONTH() {
        return AVERAGE_INCOME_EACH_MONTH;
    }

    public void setAVERAGE_INCOME_EACH_MONTH(String AVERAGE_INCOME_EACH_MONTH) {
        this.AVERAGE_INCOME_EACH_MONTH = AVERAGE_INCOME_EACH_MONTH;
    }

    public String getASSET_DESCRIPTION_OTHER() {
        return ASSET_DESCRIPTION_OTHER;
    }

    public void setASSET_DESCRIPTION_OTHER(String ASSET_DESCRIPTION_OTHER) {
        this.ASSET_DESCRIPTION_OTHER = ASSET_DESCRIPTION_OTHER;
    }

    public String getASSET_PRICE() {
        return ASSET_PRICE;
    }

    public void setASSET_PRICE(String ASSET_PRICE) {
        this.ASSET_PRICE = ASSET_PRICE;
    }

    public String getASSET_DESCRIPTION() {
        return ASSET_DESCRIPTION;
    }

    public void setASSET_DESCRIPTION(String ASSET_DESCRIPTION) {
        this.ASSET_DESCRIPTION = ASSET_DESCRIPTION;
    }

    public String getHAVE_BCA_ACCOUNT() {
        return HAVE_BCA_ACCOUNT;
    }

    public void setHAVE_BCA_ACCOUNT(String HAVE_BCA_ACCOUNT) {
        this.HAVE_BCA_ACCOUNT = HAVE_BCA_ACCOUNT;
    }

    public String getHAVE_OTHER_CREDIT_CARD() {
        return HAVE_OTHER_CREDIT_CARD;
    }

    public void setHAVE_OTHER_CREDIT_CARD(String HAVE_OTHER_CREDIT_CARD) {
        this.HAVE_OTHER_CREDIT_CARD = HAVE_OTHER_CREDIT_CARD;
    }

    public String getACCOUNT_LATTER() {
        return ACCOUNT_LATTER;
    }

    public void setACCOUNT_LATTER(String ACCOUNT_LATTER) {
        this.ACCOUNT_LATTER = ACCOUNT_LATTER;
    }

    public String getHAVE_SAVING() {
        return HAVE_SAVING;
    }

    public void setHAVE_SAVING(String HAVE_SAVING) {
        this.HAVE_SAVING = HAVE_SAVING;
    }

    public String getHAVE_DEPOSITO() {
        return HAVE_DEPOSITO;
    }

    public void setHAVE_DEPOSITO(String HAVE_DEPOSITO) {
        this.HAVE_DEPOSITO = HAVE_DEPOSITO;
    }

    public String getFASILITY_TYPE() {
        return FASILITY_TYPE;
    }

    public void setFASILITY_TYPE(String FASILITY_TYPE) {
        this.FASILITY_TYPE = FASILITY_TYPE;
    }

    public String getOTHER_BANK_OR_FINACE() {
        return OTHER_BANK_OR_FINACE;
    }

    public void setOTHER_BANK_OR_FINACE(String OTHER_BANK_OR_FINACE) {
        this.OTHER_BANK_OR_FINACE = OTHER_BANK_OR_FINACE;
    }

    public String getINSTALLMENT() {
        return INSTALLMENT;
    }

    public void setINSTALLMENT(String INSTALLMENT) {
        this.INSTALLMENT = INSTALLMENT;
    }

    public String getHAVE_CREDIT_CARD() {
        return HAVE_CREDIT_CARD;
    }

    public void setHAVE_CREDIT_CARD(String HAVE_CREDIT_CARD) {
        this.HAVE_CREDIT_CARD = HAVE_CREDIT_CARD;
    }

    public String getBANK_OF_CREDIT_CARD() {
        return BANK_OF_CREDIT_CARD;
    }

    public void setBANK_OF_CREDIT_CARD(String BANK_OF_CREDIT_CARD) {
        this.BANK_OF_CREDIT_CARD = BANK_OF_CREDIT_CARD;
    }

    public String getCARD_TYPE() {
        return CARD_TYPE;
    }

    public void setCARD_TYPE(String CARD_TYPE) {
        this.CARD_TYPE = CARD_TYPE;
    }

    public String getCARD_SERVICE() {
        return CARD_SERVICE;
    }

    public void setCARD_SERVICE(String CARD_SERVICE) {
        this.CARD_SERVICE = CARD_SERVICE;
    }

    public String getCARD_LIMIT() {
        return CARD_LIMIT;
    }

    public void setCARD_LIMIT(String CARD_LIMIT) {
        this.CARD_LIMIT = CARD_LIMIT;
    }

    public String getCARD_AVERAGE_USAGE() {
        return CARD_AVERAGE_USAGE;
    }

    public void setCARD_AVERAGE_USAGE(String CARD_AVERAGE_USAGE) {
        this.CARD_AVERAGE_USAGE = CARD_AVERAGE_USAGE;
    }

    public String getPENGHASILAN_NON_TETAP() {
        return PENGHASILAN_NON_TETAP;
    }

    public void setPENGHASILAN_NON_TETAP(String PENGHASILAN_NON_TETAP) {
        this.PENGHASILAN_NON_TETAP = PENGHASILAN_NON_TETAP;
    }

    public String getTOTAL_PENDAPATAN() {
        return TOTAL_PENDAPATAN;
    }

    public void setTOTAL_PENDAPATAN(String TOTAL_PENDAPATAN) {
        this.TOTAL_PENDAPATAN = TOTAL_PENDAPATAN;
    }

    public String getPENGELUARAN_RUTIN() {
        return PENGELUARAN_RUTIN;
    }

    public void setPENGELUARAN_RUTIN(String PENGELUARAN_RUTIN) {
        this.PENGELUARAN_RUTIN = PENGELUARAN_RUTIN;
    }

    public String getPENGELUARAN_LAIN() {
        return PENGELUARAN_LAIN;
    }

    public void setPENGELUARAN_LAIN(String PENGELUARAN_LAIN) {
        this.PENGELUARAN_LAIN = PENGELUARAN_LAIN;
    }

    public String getPENGELUARAN_TOTAL() {
        return PENGELUARAN_TOTAL;
    }

    public void setPENGELUARAN_TOTAL(String PENGELUARAN_TOTAL) {
        this.PENGELUARAN_TOTAL = PENGELUARAN_TOTAL;
    }

    public String getHOUSE() {
        return HOUSE;
    }

    public void setHOUSE(String HOUSE) {
        this.HOUSE = HOUSE;
    }

    public String getOLD_OCCUPY() {
        return OLD_OCCUPY;
    }

    public void setOLD_OCCUPY(String OLD_OCCUPY) {
        this.OLD_OCCUPY = OLD_OCCUPY;
    }

    public String getBUILDING_CONDITION() {
        return BUILDING_CONDITION;
    }

    public void setBUILDING_CONDITION(String BUILDING_CONDITION) {
        this.BUILDING_CONDITION = BUILDING_CONDITION;
    }

    public String getBUILDING_AREA() {
        return BUILDING_AREA;
    }

    public void setBUILDING_AREA(String BUILDING_AREA) {
        this.BUILDING_AREA = BUILDING_AREA;
    }

    public String getLAND_AREA() {
        return LAND_AREA;
    }

    public void setLAND_AREA(String LAND_AREA) {
        this.LAND_AREA = LAND_AREA;
    }

    public String getELECTRICITY() {
        return ELECTRICITY;
    }

    public void setELECTRICITY(String ELECTRICITY) {
        this.ELECTRICITY = ELECTRICITY;
    }

    public String getWATER() {
        return WATER;
    }

    public void setWATER(String WATER) {
        this.WATER = WATER;
    }

    public String getTRANSPORT() {
        return TRANSPORT;
    }

    public void setTRANSPORT(String TRANSPORT) {
        this.TRANSPORT = TRANSPORT;
    }

    public String getFURNITURE() {
        return FURNITURE;
    }

    public void setFURNITURE(String FURNITURE) {
        this.FURNITURE = FURNITURE;
    }

    public String getELECTRONIC() {
        return ELECTRONIC;
    }

    public void setELECTRONIC(String ELECTRONIC) {
        this.ELECTRONIC = ELECTRONIC;
    }

    public String getBUSSINESS_TYPE_P20() {
        return BUSSINESS_TYPE_P20;
    }

    public void setBUSSINESS_TYPE_P20(String BUSSINESS_TYPE_P20) {
        this.BUSSINESS_TYPE_P20 = BUSSINESS_TYPE_P20;
    }

    public String getNEIGHBOARD() {
        return NEIGHBOARD;
    }

    public void setNEIGHBOARD(String NEIGHBOARD) {
        this.NEIGHBOARD = NEIGHBOARD;
    }

    public String getHOBBY() {
        return HOBBY;
    }

    public void setHOBBY(String HOBBY) {
        this.HOBBY = HOBBY;
    }

    public String getOPEN_GIVE_INFORMATION() {
        return OPEN_GIVE_INFORMATION;
    }

    public void setOPEN_GIVE_INFORMATION(String OPEN_GIVE_INFORMATION) {
        this.OPEN_GIVE_INFORMATION = OPEN_GIVE_INFORMATION;
    }

    public String getRELIGIUS() {
        return RELIGIUS;
    }

    public void setRELIGIUS(String RELIGIUS) {
        this.RELIGIUS = RELIGIUS;
    }

    public String getACCEPT_CREDIT_RULE() {
        return ACCEPT_CREDIT_RULE;
    }

    public void setACCEPT_CREDIT_RULE(String ACCEPT_CREDIT_RULE) {
        this.ACCEPT_CREDIT_RULE = ACCEPT_CREDIT_RULE;
    }

    public String getETIKA_WHEN_SURVEY() {
        return ETIKA_WHEN_SURVEY;
    }

    public void setETIKA_WHEN_SURVEY(String ETIKA_WHEN_SURVEY) {
        this.ETIKA_WHEN_SURVEY = ETIKA_WHEN_SURVEY;
    }

    public String getSURVEY_DATE() {
        return SURVEY_DATE;
    }

    public void setSURVEY_DATE(String SURVEY_DATE) {
        this.SURVEY_DATE = SURVEY_DATE;
    }

    public String getSURVEY_PLACE() {
        return SURVEY_PLACE;
    }

    public void setSURVEY_PLACE(String SURVEY_PLACE) {
        this.SURVEY_PLACE = SURVEY_PLACE;
    }

    public String getMEET_WITH() {
        return MEET_WITH;
    }

    public void setMEET_WITH(String MEET_WITH) {
        this.MEET_WITH = MEET_WITH;
    }

    public String getCUST_OLD() {
        return CUST_OLD;
    }

    public void setCUST_OLD(String CUST_OLD) {
        this.CUST_OLD = CUST_OLD;
    }

    public String getCUST_HEALTY() {
        return CUST_HEALTY;
    }

    public void setCUST_HEALTY(String CUST_HEALTY) {
        this.CUST_HEALTY = CUST_HEALTY;
    }

    public String getCUST_HEALTY_SPOUSEORCHILD() {
        return CUST_HEALTY_SPOUSEORCHILD;
    }

    public void setCUST_HEALTY_SPOUSEORCHILD(String CUST_HEALTY_SPOUSEORCHILD) {
        this.CUST_HEALTY_SPOUSEORCHILD = CUST_HEALTY_SPOUSEORCHILD;
    }

    public String getHEALTY_GUAR() {
        return HEALTY_GUAR;
    }

    public void setHEALTY_GUAR(String HEALTY_GUAR) {
        this.HEALTY_GUAR = HEALTY_GUAR;
    }

    public String getHOUSE_CONDITION() {
        return HOUSE_CONDITION;
    }

    public void setHOUSE_CONDITION(String HOUSE_CONDITION) {
        this.HOUSE_CONDITION = HOUSE_CONDITION;
    }

    public String getFURNITURE_CONDITION() {
        return FURNITURE_CONDITION;
    }

    public void setFURNITURE_CONDITION(String FURNITURE_CONDITION) {
        this.FURNITURE_CONDITION = FURNITURE_CONDITION;
    }

    public String getTRANSORT_CONDITION() {
        return TRANSORT_CONDITION;
    }

    public void setTRANSORT_CONDITION(String TRANSORT_CONDITION) {
        this.TRANSORT_CONDITION = TRANSORT_CONDITION;
    }

    public String getBUSSINESS_CONDITION() {
        return BUSSINESS_CONDITION;
    }

    public void setBUSSINESS_CONDITION(String BUSSINESS_CONDITION) {
        this.BUSSINESS_CONDITION = BUSSINESS_CONDITION;
    }

    public String getGUAR_LETTER() {
        return GUAR_LETTER;
    }

    public void setGUAR_LETTER(String GUAR_LETTER) {
        this.GUAR_LETTER = GUAR_LETTER;
    }

    public String getGUARD_RELATION() {
        return GUARD_RELATION;
    }

    public void setGUARD_RELATION(String GUARD_RELATION) {
        this.GUARD_RELATION = GUARD_RELATION;
    }

    public String getHOUSE_FILE() {
        return HOUSE_FILE;
    }

    public void setHOUSE_FILE(String HOUSE_FILE) {
        this.HOUSE_FILE = HOUSE_FILE;
    }

    public String getDOCUMENT_FILE() {
        return DOCUMENT_FILE;
    }

    public void setDOCUMENT_FILE(String DOCUMENT_FILE) {
        this.DOCUMENT_FILE = DOCUMENT_FILE;
    }

    public String getCONSUMENT_FILE() {
        return CONSUMENT_FILE;
    }

    public void setCONSUMENT_FILE(String CONSUMENT_FILE) {
        this.CONSUMENT_FILE = CONSUMENT_FILE;
    }

    public String getGPS() {
        return GPS;
    }

    public void setGPS(String GPS) {
        this.GPS = GPS;
    }

    public String getGPS_DESCRIPTION() {
        return GPS_DESCRIPTION;
    }

    public void setGPS_DESCRIPTION(String GPS_DESCRIPTION) {
        this.GPS_DESCRIPTION = GPS_DESCRIPTION;
    }

    public String getMOBILE_STATUS() {
        return MOBILE_STATUS;
    }

    public void setMOBILE_STATUS(String MOBILE_STATUS) {
        this.MOBILE_STATUS = MOBILE_STATUS;
    }

    public String getCHILD_RW() {
        return CHILD_RW;
    }

    public void setCHILD_RW(String CHILD_RW) {
        this.CHILD_RW = CHILD_RW;
    }

    public String getCHILD_KELURAHAN() {
        return CHILD_KELURAHAN;
    }

    public void setCHILD_KELURAHAN(String CHILD_KELURAHAN) {
        this.CHILD_KELURAHAN = CHILD_KELURAHAN;
    }

    public String getCHILD_KECAMATAN() {
        return CHILD_KECAMATAN;
    }

    public void setCHILD_KECAMATAN(String CHILD_KECAMATAN) {
        this.CHILD_KECAMATAN = CHILD_KECAMATAN;
    }

    public String getCHILD_CITY() {
        return CHILD_CITY;
    }

    public void setCHILD_CITY(String CHILD_CITY) {
        this.CHILD_CITY = CHILD_CITY;
    }

    public String getCHILD_POSCODE() {
        return CHILD_POSCODE;
    }

    public void setCHILD_POSCODE(String CHILD_POSCODE) {
        this.CHILD_POSCODE = CHILD_POSCODE;
    }

    public String getCHILD_PHONEONEAREA() {
        return CHILD_PHONEONEAREA;
    }

    public void setCHILD_PHONEONEAREA(String CHILD_PHONEONEAREA) {
        this.CHILD_PHONEONEAREA = CHILD_PHONEONEAREA;
    }

    public String getCHILD_PHONEONE() {
        return CHILD_PHONEONE;
    }

    public void setCHILD_PHONEONE(String CHILD_PHONEONE) {
        this.CHILD_PHONEONE = CHILD_PHONEONE;
    }

    public String getCHILD_PHONETWOAREA() {
        return CHILD_PHONETWOAREA;
    }

    public void setCHILD_PHONETWOAREA(String CHILD_PHONETWOAREA) {
        this.CHILD_PHONETWOAREA = CHILD_PHONETWOAREA;
    }

    public String getCHILD_PHONETWO() {
        return CHILD_PHONETWO;
    }

    public void setCHILD_PHONETWO(String CHILD_PHONETWO) {
        this.CHILD_PHONETWO = CHILD_PHONETWO;
    }

    public String getDATA_MAP_NAME() {
        return DATA_MAP_NAME;
    }

    public void setDATA_MAP_NAME(String DATA_MAP_NAME) {
        this.DATA_MAP_NAME = DATA_MAP_NAME;
    }

    public String getORDERID() {
        return ORDER_ID;
    }

    public void setORDERID(String ORDERID) {
        this.ORDER_ID = ORDERID;
    }

    public String getMODULE_ID() {
        return MODULE_ID;
    }

    public void setMODULE_ID(String MODULE_ID) {
        this.MODULE_ID = MODULE_ID;
    }

    public String getITEM_TYPE() {
        return ITEM_TYPE;
    }

    public void setITEM_TYPE(String ITEM_TYPE) {
        this.ITEM_TYPE = ITEM_TYPE;
    }

    public String getITEM_KEY() {
        return ITEM_KEY;
    }

    public void setITEM_KEY(String ITEM_KEY) {
        this.ITEM_KEY = ITEM_KEY;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_UPDATE() {
        return STATUS_UPDATE;
    }

    public void setSTATUS_UPDATE(String STATUS_UPDATE) {
        this.STATUS_UPDATE = STATUS_UPDATE;
    }

    public String getTIMESTAMPS() {
        return TIMESTAMPS;
    }

    public void setTIMESTAMPS(String TIMESTAMPS) {
        this.TIMESTAMPS = TIMESTAMPS;
    }

    public String getLAST_TRX() {
        return LAST_TRX;
    }

    public void setLAST_TRX(String LAST_TRX) {
        this.LAST_TRX = LAST_TRX;
    }

    public String getBRANCH_ID() {
        return BRANCH_ID;
    }

    public void setBRANCH_ID(String BRANCH_ID) {
        this.BRANCH_ID = BRANCH_ID;
    }

    public String getSTATUS_KUNJUNGAN() {
        return STATUS_KUNJUNGAN;
    }

    public void setSTATUS_KUNJUNGAN(String STATUS_KUNJUNGAN) {
        this.STATUS_KUNJUNGAN = STATUS_KUNJUNGAN;
    }

    public String getBLANK() {
        return BLANK;
    }

    public void setBLANK(String BLANK) {
        this.BLANK = BLANK;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPERS_IDENTIFY_TYPE() {
        return PERS_IDENTIFY_TYPE;
    }

    public void setPERS_IDENTIFY_TYPE(String PERS_IDENTIFY_TYPE) {
        this.PERS_IDENTIFY_TYPE = PERS_IDENTIFY_TYPE;
    }

    public String getIDENTIFY_NO() {
        return IDENTIFY_NO;
    }

    public void setIDENTIFY_NO(String IDENTIFY_NO) {
        this.IDENTIFY_NO = IDENTIFY_NO;
    }

    public String getNPWP_NO() {
        return NPWP_NO;
    }

    public void setNPWP_NO(String NPWP_NO) {
        this.NPWP_NO = NPWP_NO;
    }

    public String getCUST_BIRTH_DATE() {
        return CUST_BIRTH_DATE;
    }

    public void setCUST_BIRTH_DATE(String CUST_BIRTH_DATE) {
        this.CUST_BIRTH_DATE = CUST_BIRTH_DATE;
    }

    public String getPERS_RELIGION() {
        return PERS_RELIGION;
    }

    public void setPERS_RELIGION(String PERS_RELIGION) {
        this.PERS_RELIGION = PERS_RELIGION;
    }

    public String getADDRESS_KTP() {
        return ADDRESS_KTP;
    }

    public void setADDRESS_KTP(String ADDRESS_KTP) {
        this.ADDRESS_KTP = ADDRESS_KTP;
    }

    public String getRT() {
        return RT;
    }

    public void setRT(String RT) {
        this.RT = RT;
    }

    public String getRW() {
        return RW;
    }

    public void setRW(String RW) {
        this.RW = RW;
    }

    public String getKELURAHAN() {
        return KELURAHAN;
    }

    public void setKELURAHAN(String KELURAHAN) {
        this.KELURAHAN = KELURAHAN;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public void setKECAMATAN(String KECAMATAN) {
        this.KECAMATAN = KECAMATAN;
    }

    public String getKABKODYA() {
        return KABKODYA;
    }

    public void setKABKODYA(String KABKODYA) {
        this.KABKODYA = KABKODYA;
    }

    public String getPROVINCE() {
        return PROVINCE;
    }

    public void setPROVINCE(String PROVINCE) {
        this.PROVINCE = PROVINCE;
    }

    public String getPERSPOSTALCODE() {
        return PERSPOSTALCODE;
    }

    public void setPERSPOSTALCODE(String PERSPOSTALCODE) {
        this.PERSPOSTALCODE = PERSPOSTALCODE;
    }

    public String getSANDIDATIII() {
        return SANDIDATIII;
    }

    public void setSANDIDATIII(String SANDIDATIII) {
        this.SANDIDATIII = SANDIDATIII;
    }

    public String getHOME_STATUS() {
        return HOME_STATUS;
    }

    public void setHOME_STATUS(String HOME_STATUS) {
        this.HOME_STATUS = HOME_STATUS;
    }

    public String getTELEPHONE_AREA() {
        return TELEPHONE_AREA;
    }

    public void setTELEPHONE_AREA(String TELEPHONE_AREA) {
        this.TELEPHONE_AREA = TELEPHONE_AREA;
    }

    public String getTELEPHONE_NUMBER() {
        return TELEPHONE_NUMBER;
    }

    public void setTELEPHONE_NUMBER(String TELEPHONE_NUMBER) {
        this.TELEPHONE_NUMBER = TELEPHONE_NUMBER;
    }

    public String getHANDPHONE_PREFIX() {
        return HANDPHONE_PREFIX;
    }

    public void setHANDPHONE_PREFIX(String HANDPHONE_PREFIX) {
        this.HANDPHONE_PREFIX = HANDPHONE_PREFIX;
    }

    public String getHANDPHONE_NUMBER() {
        return HANDPHONE_NUMBER;
    }

    public void setHANDPHONE_NUMBER(String HANDPHONE_NUMBER) {
        this.HANDPHONE_NUMBER = HANDPHONE_NUMBER;
    }

    public String getOCCUPATION() {
        return OCCUPATION;
    }

    public void setOCCUPATION(String OCCUPATION) {
        this.OCCUPATION = OCCUPATION;
    }

    public String getJOBADDRESS() {
        return JOBADDRESS;
    }

    public void setJOBADDRESS(String JOBADDRESS) {
        this.JOBADDRESS = JOBADDRESS;
    }

    public String getJOBPOSTALCODE() {
        return JOBPOSTALCODE;
    }

    public void setJOBPOSTALCODE(String JOBPOSTALCODE) {
        this.JOBPOSTALCODE = JOBPOSTALCODE;
    }

    public String getLENGHOFWORK() {
        return LENGHOFWORK;
    }

    public void setLENGHOFWORK(String LENGHOFWORK) {
        this.LENGHOFWORK = LENGHOFWORK;
    }

    public String getFAM_IDENTIFY_TYPE() {
        return FAM_IDENTIFY_TYPE;
    }

    public void setFAM_IDENTIFY_TYPE(String FAM_IDENTIFY_TYPE) {
        this.FAM_IDENTIFY_TYPE = FAM_IDENTIFY_TYPE;
    }

    public String getFAM_BIRTH_DATE() {
        return FAM_BIRTH_DATE;
    }

    public void setFAM_BIRTH_DATE(String FAM_BIRTH_DATE) {
        this.FAM_BIRTH_DATE = FAM_BIRTH_DATE;
    }

    public String getGUAR_ADDRESS() {
        return GUAR_ADDRESS;
    }

    public void setGUAR_ADDRESS(String GUAR_ADDRESS) {
        this.GUAR_ADDRESS = GUAR_ADDRESS;
    }

    public String getGUAR_RELIGION() {
        return GUAR_RELIGION;
    }

    public void setGUAR_RELIGION(String GUAR_RELIGION) {
        this.GUAR_RELIGION = GUAR_RELIGION;
    }

    public String getPENJ_ADDRESS() {
        return PENJ_ADDRESS;
    }

    public void setPENJ_ADDRESS(String PENJ_ADDRESS) {
        this.PENJ_ADDRESS = PENJ_ADDRESS;
    }

    public String getGROSS_FIX_INCOME() {
        return GROSS_FIX_INCOME;
    }

    public void setGROSS_FIX_INCOME(String GROSS_FIX_INCOME) {
        this.GROSS_FIX_INCOME = GROSS_FIX_INCOME;
    }

    public String getADDITIONAL_INCOME() {
        return ADDITIONAL_INCOME;
    }

    public void setADDITIONAL_INCOME(String ADDITIONAL_INCOME) {
        this.ADDITIONAL_INCOME = ADDITIONAL_INCOME;
    }

    public String getPRODUCT_CODE() {
        return PRODUCT_CODE;
    }

    public void setPRODUCT_CODE(String PRODUCT_CODE) {
        this.PRODUCT_CODE = PRODUCT_CODE;
    }

    public String getDEALER_NAME() {
        return DEALER_NAME;
    }

    public void setDEALER_NAME(String DEALER_NAME) {
        this.DEALER_NAME = DEALER_NAME;
    }

    public String getBRANCH() {
        return BRANCH;
    }

    public void setBRANCH(String BRANCH) {
        this.BRANCH = BRANCH;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    public String getMODEL() {
        return MODEL;
    }

    public void setMODEL(String MODEL) {
        this.MODEL = MODEL;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getYEAR() {
        return YEAR;
    }

    public void setYEAR(String YEAR) {
        this.YEAR = YEAR;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getORDER_DATE() {
        return ORDER_DATE;
    }

    public void setORDER_DATE(String ORDER_DATE) {
        this.ORDER_DATE = ORDER_DATE;
    }

    public String getCASH_PRICE() {
        return CASH_PRICE;
    }

    public void setCASH_PRICE(String CASH_PRICE) {
        this.CASH_PRICE = CASH_PRICE;
    }

    public String getTENOR() {
        return TENOR;
    }

    public void setTENOR(String TENOR) {
        this.TENOR = TENOR;
    }

    public String getJENIS_PEKERJAAN() {
        return JENIS_PEKERJAAN;
    }

    public void setJENIS_PEKERJAAN(String JENIS_PEKERJAAN) {
        this.JENIS_PEKERJAAN = JENIS_PEKERJAAN;
    }

    public String getPENGHASILAN_TETAP() {
        return PENGHASILAN_TETAP;
    }

    public void setPENGHASILAN_TETAP(String PENGHASILAN_TETAP) {
        this.PENGHASILAN_TETAP = PENGHASILAN_TETAP;
    }

    public String getADD_INCOME_SRC() {
        return ADD_INCOME_SRC;
    }

    public void setADD_INCOME_SRC(String ADD_INCOME_SRC) {
        this.ADD_INCOME_SRC = ADD_INCOME_SRC;
    }

    public String getFLAG_DELIVER() {
        return FLAG_DELIVER;
    }

    public void setFLAG_DELIVER(String FLAG_DELIVER) {
        this.FLAG_DELIVER = FLAG_DELIVER;
    }

    public String getFLAG_UPDATE_CLOSE() {
        return FLAG_UPDATE_CLOSE;
    }

    public void setFLAG_UPDATE_CLOSE(String FLAG_UPDATE_CLOSE) {
        this.FLAG_UPDATE_CLOSE = FLAG_UPDATE_CLOSE;
    }

    public String getNICKNAME() {
        return NICKNAME;
    }

    public void setNICKNAME(String NICKNAME) {
        this.NICKNAME = NICKNAME;
    }

    public String getMOTHER_NAME() {
        return MOTHER_NAME;
    }

    public void setMOTHER_NAME(String MOTHER_NAME) {
        this.MOTHER_NAME = MOTHER_NAME;
    }

    public String getFIRST_TITLE() {
        return FIRST_TITLE;
    }

    public void setFIRST_TITLE(String FIRST_TITLE) {
        this.FIRST_TITLE = FIRST_TITLE;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    public String getBIRTH_PLACE() {
        return BIRTH_PLACE;
    }

    public void setBIRTH_PLACE(String BIRTH_PLACE) {
        this.BIRTH_PLACE = BIRTH_PLACE;
    }

    public String getDURATION_STAY_YEAR() {
        return DURATION_STAY_YEAR;
    }

    public void setDURATION_STAY_YEAR(String DURATION_STAY_YEAR) {
        this.DURATION_STAY_YEAR = DURATION_STAY_YEAR;
    }

    public String getDURATION_STAY_MONTH() {
        return DURATION_STAY_MONTH;
    }

    public void setDURATION_STAY_MONTH(String DURATION_STAY_MONTH) {
        this.DURATION_STAY_MONTH = DURATION_STAY_MONTH;
    }

    public String getINDUSTRY_CODE() {
        return INDUSTRY_CODE;
    }

    public void setINDUSTRY_CODE(String INDUSTRY_CODE) {
        this.INDUSTRY_CODE = INDUSTRY_CODE;
    }

    public String getOFFICE_NAME() {
        return OFFICE_NAME;
    }

    public void setOFFICE_NAME(String OFFICE_NAME) {
        this.OFFICE_NAME = OFFICE_NAME;
    }

    public String getTGL_DELIVERED() {
        return TGL_DELIVERED;
    }

    public void setTGL_DELIVERED(String TGL_DELIVERED) {
        this.TGL_DELIVERED = TGL_DELIVERED;
    }

    public String getTGL_CLOSE() {
        return TGL_CLOSE;
    }

    public void setTGL_CLOSE(String TGL_CLOSE) {
        this.TGL_CLOSE = TGL_CLOSE;
    }

    public String getTGL_CLOSE_SEND() {
        return TGL_CLOSE_SEND;
    }

    public void setTGL_CLOSE_SEND(String TGL_CLOSE_SEND) {
        this.TGL_CLOSE_SEND = TGL_CLOSE_SEND;
    }

    public String getMARITAL_STATUS() {
        return MARITAL_STATUS;
    }

    public void setMARITAL_STATUS(String MARITAL_STATUS) {
        this.MARITAL_STATUS = MARITAL_STATUS;
    }

    public String getOFFICE_PROVINCE() {
        return OFFICE_PROVINCE;
    }

    public void setOFFICE_PROVINCE(String OFFICE_PROVINCE) {
        this.OFFICE_PROVINCE = OFFICE_PROVINCE;
    }

    public String getOFFICE_CITY() {
        return OFFICE_CITY;
    }

    public void setOFFICE_CITY(String OFFICE_CITY) {
        this.OFFICE_CITY = OFFICE_CITY;
    }

    public String getCUSTOMER_EDUCATION() {
        return CUSTOMER_EDUCATION;
    }

    public void setCUSTOMER_EDUCATION(String CUSTOMER_EDUCATION) {
        this.CUSTOMER_EDUCATION = CUSTOMER_EDUCATION;
    }

    public String getDURATION_JOB_MONTH() {
        return DURATION_JOB_MONTH;
    }

    public void setDURATION_JOB_MONTH(String DURATION_JOB_MONTH) {
        this.DURATION_JOB_MONTH = DURATION_JOB_MONTH;
    }

    public String getCUSTOMER_RESI_ADDRESS() {
        return CUSTOMER_RESI_ADDRESS;
    }

    public void setCUSTOMER_RESI_ADDRESS(String CUSTOMER_RESI_ADDRESS) {
        this.CUSTOMER_RESI_ADDRESS = CUSTOMER_RESI_ADDRESS;
    }

    public String getCUSTOMER_RESI_RT() {
        return CUSTOMER_RESI_RT;
    }

    public void setCUSTOMER_RESI_RT(String CUSTOMER_RESI_RT) {
        this.CUSTOMER_RESI_RT = CUSTOMER_RESI_RT;
    }

    public String getCUSTOMER_RESI_RW() {
        return CUSTOMER_RESI_RW;
    }

    public void setCUSTOMER_RESI_RW(String CUSTOMER_RESI_RW) {
        this.CUSTOMER_RESI_RW = CUSTOMER_RESI_RW;
    }

    public String getCUSTOMER_RESI_VILLAGE() {
        return CUSTOMER_RESI_VILLAGE;
    }

    public void setCUSTOMER_RESI_VILLAGE(String CUSTOMER_RESI_VILLAGE) {
        this.CUSTOMER_RESI_VILLAGE = CUSTOMER_RESI_VILLAGE;
    }

    public String getCUSTOMER_RESI_DISTRICT() {
        return CUSTOMER_RESI_DISTRICT;
    }

    public void setCUSTOMER_RESI_DISTRICT(String CUSTOMER_RESI_DISTRICT) {
        this.CUSTOMER_RESI_DISTRICT = CUSTOMER_RESI_DISTRICT;
    }

    public String getCUSTOMER_RESI_REGENCY() {
        return CUSTOMER_RESI_REGENCY;
    }

    public void setCUSTOMER_RESI_REGENCY(String CUSTOMER_RESI_REGENCY) {
        this.CUSTOMER_RESI_REGENCY = CUSTOMER_RESI_REGENCY;
    }

    public String getCUSTOMER_RESI_PROVINCE() {
        return CUSTOMER_RESI_PROVINCE;
    }

    public void setCUSTOMER_RESI_PROVINCE(String CUSTOMER_RESI_PROVINCE) {
        this.CUSTOMER_RESI_PROVINCE = CUSTOMER_RESI_PROVINCE;
    }

    public String getCUSTOMER_RESI_POSTCODE() {
        return CUSTOMER_RESI_POSTCODE;
    }

    public void setCUSTOMER_RESI_POSTCODE(String CUSTOMER_RESI_POSTCODE) {
        this.CUSTOMER_RESI_POSTCODE = CUSTOMER_RESI_POSTCODE;
    }

    public String getCUSTOMER_RESI_CITY() {
        return CUSTOMER_RESI_CITY;
    }

    public void setCUSTOMER_RESI_CITY(String CUSTOMER_RESI_CITY) {
        this.CUSTOMER_RESI_CITY = CUSTOMER_RESI_CITY;
    }

    public String getDOWNPAYMENT_PERC() {
        return DOWNPAYMENT_PERC;
    }

    public void setDOWNPAYMENT_PERC(String DOWNPAYMENT_PERC) {
        this.DOWNPAYMENT_PERC = DOWNPAYMENT_PERC;
    }

    public String getDOWNPAYMENT_AMT() {
        return DOWNPAYMENT_AMT;
    }

    public void setDOWNPAYMENT_AMT(String DOWNPAYMENT_AMT) {
        this.DOWNPAYMENT_AMT = DOWNPAYMENT_AMT;
    }

    public String getINSTALLMENT_AMT() {
        return INSTALLMENT_AMT;
    }

    public void setINSTALLMENT_AMT(String INSTALLMENT_AMT) {
        this.INSTALLMENT_AMT = INSTALLMENT_AMT;
    }

    public String getINSTALLMENT_TYPE() {
        return INSTALLMENT_TYPE;
    }

    public void setINSTALLMENT_TYPE(String INSTALLMENT_TYPE) {
        this.INSTALLMENT_TYPE = INSTALLMENT_TYPE;
    }

    public String getRENT_PAYMENT_TYPE() {
        return RENT_PAYMENT_TYPE;
    }

    public void setRENT_PAYMENT_TYPE(String RENT_PAYMENT_TYPE) {
        this.RENT_PAYMENT_TYPE = RENT_PAYMENT_TYPE;
    }

    public String getASSET_NO() {
        return ASSET_NO;
    }

    public void setASSET_NO(String ASSET_NO) {
        this.ASSET_NO = ASSET_NO;
    }

    public String getADD_INCOME_SOURCE() {
        return ADD_INCOME_SOURCE;
    }

    public void setADD_INCOME_SOURCE(String ADD_INCOME_SOURCE) {
        this.ADD_INCOME_SOURCE = ADD_INCOME_SOURCE;
    }

    public String getSPOUSE_NAME() {
        return SPOUSE_NAME;
    }

    public void setSPOUSE_NAME(String SPOUSE_NAME) {
        this.SPOUSE_NAME = SPOUSE_NAME;
    }

    public String getSPOUSE_ID_TYPE() {
        return SPOUSE_ID_TYPE;
    }

    public void setSPOUSE_ID_TYPE(String SPOUSE_ID_TYPE) {
        this.SPOUSE_ID_TYPE = SPOUSE_ID_TYPE;
    }

    public String getSPOUSE_ID_NO() {
        return SPOUSE_ID_NO;
    }

    public void setSPOUSE_ID_NO(String SPOUSE_ID_NO) {
        this.SPOUSE_ID_NO = SPOUSE_ID_NO;
    }

    public String getCHILD_NAME() {
        return CHILD_NAME;
    }

    public void setCHILD_NAME(String CHILD_NAME) {
        this.CHILD_NAME = CHILD_NAME;
    }

    public String getCHILD_SCHOOL_NAME() {
        return CHILD_SCHOOL_NAME;
    }

    public void setCHILD_SCHOOL_NAME(String CHILD_SCHOOL_NAME) {
        this.CHILD_SCHOOL_NAME = CHILD_SCHOOL_NAME;
    }

    public String getCHILD_SCHOOL_ADDRESS() {
        return CHILD_SCHOOL_ADDRESS;
    }

    public void setCHILD_SCHOOL_ADDRESS(String CHILD_SCHOOL_ADDRESS) {
        this.CHILD_SCHOOL_ADDRESS = CHILD_SCHOOL_ADDRESS;
    }

    public String getSAME_CUSTOMER_ID_ADDRESS() {
        return SAME_CUSTOMER_ID_ADDRESS;
    }

    public void setSAME_CUSTOMER_ID_ADDRESS(String SAME_CUSTOMER_ID_ADDRESS) {
        this.SAME_CUSTOMER_ID_ADDRESS = SAME_CUSTOMER_ID_ADDRESS;
    }

    public String getASSET_CONDITION() {
        return ASSET_CONDITION;
    }

    public void setASSET_CONDITION(String ASSET_CONDITION) {
        this.ASSET_CONDITION = ASSET_CONDITION;
    }

    public String getINVOICE_CORRESPONDENCE_NO() {
        return INVOICE_CORRESPONDENCE_NO;
    }

    public void setINVOICE_CORRESPONDENCE_NO(String INVOICE_CORRESPONDENCE_NO) {
        this.INVOICE_CORRESPONDENCE_NO = INVOICE_CORRESPONDENCE_NO;
    }

    public String getCHILD_RT() {
        return CHILD_RT;
    }

    public void setCHILD_RT(String CHILD_RT) {
        this.CHILD_RT = CHILD_RT;
    }
}
