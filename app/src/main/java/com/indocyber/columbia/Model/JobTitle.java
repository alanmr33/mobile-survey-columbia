package com.indocyber.columbia.Model;

import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class JobTitle extends RealmObject {
    private String CUST_JOB_TITLE_ID;
    private String CUST_JOB_TITLE_NAME;
    private String ACTIVE_STATUS;
    private String CREATED_BY;
    private String CREATED_DATE;
    private String MODIFIED_BY;
    private String MODIFIED_DATE;
    public String getCUST_JOB_TITLE_ID() {
        return CUST_JOB_TITLE_ID;
    }

    public void setCUST_JOB_TITLE_ID(String CUST_JOB_TITLE_ID) {
        this.CUST_JOB_TITLE_ID = CUST_JOB_TITLE_ID;
    }

    public String getCUST_JOB_TITLE_NAME() {
        return CUST_JOB_TITLE_NAME;
    }

    public void setCUST_JOB_TITLE_NAME(String CUST_JOB_TITLE_NAME) {
        this.CUST_JOB_TITLE_NAME = CUST_JOB_TITLE_NAME;
    }

    public String getACTIVE_STATUS() {
        return ACTIVE_STATUS;
    }

    public void setACTIVE_STATUS(String ACTIVE_STATUS) {
        this.ACTIVE_STATUS = ACTIVE_STATUS;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getMODIFIED_BY() {
        return MODIFIED_BY;
    }

    public void setMODIFIED_BY(String MODIFIED_BY) {
        this.MODIFIED_BY = MODIFIED_BY;
    }

    public String getMODIFIED_DATE() {
        return MODIFIED_DATE;
    }

    public void setMODIFIED_DATE(String MODIFIED_DATE) {
        this.MODIFIED_DATE = MODIFIED_DATE;
    }

}
