package com.indocyber.columbia.Model;

import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class Zip extends RealmObject {
    private String idzip;
    private String jenis;
    private String kabupaten;
    private String propinsi;
    private String kecamatan;
    private String kelurahan;
    private String zip;
    public String getIdzip() {
        return idzip;
    }

    public void setIdzip(String idzip) {
        this.idzip = idzip;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

}
