package com.indocyber.columbia.Model;

import io.realm.RealmObject;

/**
 * Created by Indocyber on 17/07/2017.
 */

public class Occupation extends RealmObject {
    private String OCCUPATION_ID;
    private String OCCUPATION_NAME;
    private String ACTIVE_STATUS;
    private String CREATED_BY;
    private String CREATED_DATE;
    private String MODIFIED_BY;
    private String MODIFIED_DATE;

    public String getOCCUPATION_ID() {
        return OCCUPATION_ID;
    }

    public void setOCCUPATION_ID(String OCCUPATION_ID) {
        this.OCCUPATION_ID = OCCUPATION_ID;
    }

    public String getOCCUPATION_NAME() {
        return OCCUPATION_NAME;
    }

    public void setOCCUPATION_NAME(String OCCUPATION_NAME) {
        this.OCCUPATION_NAME = OCCUPATION_NAME;
    }

    public String getACTIVE_STATUS() {
        return ACTIVE_STATUS;
    }

    public void setACTIVE_STATUS(String ACTIVE_STATUS) {
        this.ACTIVE_STATUS = ACTIVE_STATUS;
    }

    public String getCREATED_BY() {
        return CREATED_BY;
    }

    public void setCREATED_BY(String CREATED_BY) {
        this.CREATED_BY = CREATED_BY;
    }

    public String getCREATED_DATE() {
        return CREATED_DATE;
    }

    public void setCREATED_DATE(String CREATED_DATE) {
        this.CREATED_DATE = CREATED_DATE;
    }

    public String getMODIFIED_BY() {
        return MODIFIED_BY;
    }

    public void setMODIFIED_BY(String MODIFIED_BY) {
        this.MODIFIED_BY = MODIFIED_BY;
    }

    public String getMODIFIED_DATE() {
        return MODIFIED_DATE;
    }

    public void setMODIFIED_DATE(String MODIFIED_DATE) {
        this.MODIFIED_DATE = MODIFIED_DATE;
    }
}
