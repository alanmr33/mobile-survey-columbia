package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

public class NewOrder4 extends AppFragment implements Step {
    private static final String TAG = "NewOrder4";
    private EditText txtstayyear,txtphoneareacode,txtphonenumber,txthpprefix,txthpnumber;
    private Spinner cbcustmailaddridemto,cbhomestatus,cbeducation,txtstaymonth;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder4 newInstance() {
        return new NewOrder4();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order4, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        txtstayyear= (EditText) v.findViewById(R.id.txtstayyear);
        txtstaymonth= (Spinner) v.findViewById(R.id.txtstaymonth);
        txtphoneareacode= (EditText) v.findViewById(R.id.txtphoneareacode);
        txtphonenumber= (EditText) v.findViewById(R.id.txtphonenumber);
        txthpprefix= (EditText) v.findViewById(R.id.txthpprefix);
        txthpnumber= (EditText) v.findViewById(R.id.txthpnumber);
        cbcustmailaddridemto= (Spinner) v.findViewById(R.id.cbcustmailaddridemto);
        cbhomestatus= (Spinner) v.findViewById(R.id.cbhomestatus);
        cbeducation= (Spinner) v.findViewById(R.id.cbeducation);

        //init adapter
        new NewOrderActivity().setMonthAdapter(getActivity(),txtstaymonth);
        //set value list
        try {
            setAdapterCombo("24772",cbcustmailaddridemto,true,"");
            setAdapterCombo("24773",cbhomestatus,true, orderEntry.getHOME_STATUS());
            setAdapterCombo("24780",cbeducation,true, orderEntry.getCUSTOMER_EDUCATION());
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }

        //watcher Edittext and Spinner
        cbcustmailaddridemto.setOnItemSelectedListener(new NewSpinner(cbcustmailaddridemto));
        cbhomestatus.setOnItemSelectedListener(new NewSpinner(cbhomestatus));
        txtstaymonth.setOnItemSelectedListener(new NewSpinner(txtstaymonth));
        cbeducation.setOnItemSelectedListener(new NewSpinner(cbeducation));
        txtstayyear.addTextChangedListener(new NewWatcher(txtstayyear));
        txtphoneareacode.addTextChangedListener(new NewWatcher(txtphoneareacode));
        txtphonenumber.addTextChangedListener(new NewWatcher(txtphonenumber));
        txthpprefix.addTextChangedListener(new NewWatcher(txthpprefix));
        txthpnumber.addTextChangedListener(new NewWatcher(txthpnumber));

        return v;
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";

        if (TextUtils.isEmpty(txtphoneareacode.getText())){
            txtphoneareacode.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txtphonenumber.getText())){
            txtphonenumber.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txthpprefix.getText())){
            txthpprefix.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txthpnumber.getText())){
            txthpnumber.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtstayyear.setEnabled(false);
            txtphoneareacode.setEnabled(false);
            txtphonenumber.setEnabled(false);
            txthpprefix.setEnabled(false);
            txthpnumber.setEnabled(false);
            cbcustmailaddridemto.setEnabled(false);
            cbhomestatus.setEnabled(false);
            cbeducation.setEnabled(false);
            txtstaymonth.setEnabled(false);
        }
        txtstayyear.setText(orderEntry.getDURATION_STAY_YEAR());
        txtphoneareacode.setText(orderEntry.getTELEPHONE_AREA());
        txtphonenumber.setText(orderEntry.getTELEPHONE_NUMBER());
        txthpprefix.setText(orderEntry.getHANDPHONE_PREFIX());
        txthpnumber.setText(orderEntry.getHANDPHONE_NUMBER());
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbcustmailaddridemto :
                    orderEntry.setCUSTOMER_MAIL_ADDRESS(String.valueOf(cbcustmailaddridemto.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbcustmailaddridemto.getSelectedItemId());
                    break;
                case R.id.cbhomestatus :
                    orderEntry.setHOME_STATUS(String.valueOf(cbhomestatus.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbhomestatus.getSelectedItemId());
                    break;
                case R.id.txtstaymonth :
                    orderEntry.setDURATION_STAY_MONTH(String.valueOf(txtstaymonth.getSelectedItemId()));
                    Log.d(TAG, "execute: "+txtstaymonth.getSelectedItemId());
                    break;
                case R.id.cbeducation :
                    orderEntry.setCUSTOMER_EDUCATION(String.valueOf(cbeducation.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbeducation.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtstayyear :
                    orderEntry.setDURATION_STAY_YEAR(result);
                    break;
                case R.id.txtphoneareacode :
                    orderEntry.setTELEPHONE_AREA(result);
                    break;
                case R.id.txtphonenumber :
                    orderEntry.setTELEPHONE_NUMBER(result);
                    break;
                case R.id.txthpprefix :
                    orderEntry.setHANDPHONE_PREFIX(result);
                    break;
                case R.id.txthpnumber :
                    orderEntry.setHANDPHONE_NUMBER(result);
                    break;
            }
        }
    }
}
