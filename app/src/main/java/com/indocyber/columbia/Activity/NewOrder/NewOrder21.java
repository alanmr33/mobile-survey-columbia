package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

public class NewOrder21 extends AppFragment implements Step {
    private static final String TAG = "NewOrder21";
    private Spinner rbtsikapkooperatif,rbtitikadmembayar,rbtkonsistendlminterview,rbttradebankchecking,
            rbtagresivitasdlminvestasi,txtketsisipositifnegatif;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder21 newInstance() {
        return new NewOrder21();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order21, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init componnet
        rbtsikapkooperatif= (Spinner) v.findViewById(R.id.rbtsikapkooperatif);
        rbtitikadmembayar= (Spinner) v.findViewById(R.id.rbtitikadmembayar);
        rbtkonsistendlminterview= (Spinner) v.findViewById(R.id.rbtkonsistendlminterview);
        rbttradebankchecking= (Spinner) v.findViewById(R.id.rbttradebankchecking);
        rbtagresivitasdlminvestasi= (Spinner) v.findViewById(R.id.rbtagresivitasdlminvestasi);
        txtketsisipositifnegatif= (Spinner) v.findViewById(R.id.txtketsisipositifnegatif);
        //end init component

        try {
            setAdapterCombo("24632",rbtsikapkooperatif,true,"");
            setAdapterCombo("24633",rbtitikadmembayar,true,"");
            setAdapterCombo("24634",rbtkonsistendlminterview,true,"");
            setAdapterCombo("24635",rbttradebankchecking,true,"");
            setAdapterCombo("24636",rbtagresivitasdlminvestasi,true,"");
            setAdapterCombo("25905",txtketsisipositifnegatif,true,"");

        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }

        rbtsikapkooperatif.setOnItemSelectedListener(new NewSpinner(rbtsikapkooperatif));
        rbtitikadmembayar.setOnItemSelectedListener(new NewSpinner(rbtitikadmembayar));
        rbtkonsistendlminterview.setOnItemSelectedListener(new NewSpinner(rbtkonsistendlminterview));
        rbttradebankchecking.setOnItemSelectedListener(new NewSpinner(rbttradebankchecking));
        rbtagresivitasdlminvestasi.setOnItemSelectedListener(new NewSpinner(rbtagresivitasdlminvestasi));
        txtketsisipositifnegatif.setOnItemSelectedListener(new NewSpinner(txtketsisipositifnegatif));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            rbtsikapkooperatif.setEnabled(false);
            rbtitikadmembayar.setEnabled(false);
            rbtkonsistendlminterview.setEnabled(false);
            rbttradebankchecking.setEnabled(false);
            rbtagresivitasdlminvestasi.setEnabled(false);
            txtketsisipositifnegatif.setEnabled(false);
        }
        return v;
    }

    private void setupView (){
        Log.d(TAG, "setupView: ");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;
        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.rbtsikapkooperatif :
                    orderEntry.setNEIGHBOARD(String.valueOf(rbtsikapkooperatif.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbtsikapkooperatif.getSelectedItemId());
                    break;
                case R.id.rbtitikadmembayar :
                    orderEntry.setHOBBY(String.valueOf(rbtitikadmembayar.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbtitikadmembayar.getSelectedItemId());
                    break;
                case R.id.rbtkonsistendlminterview :
                    orderEntry.setOPEN_GIVE_INFORMATION(String.valueOf(rbtkonsistendlminterview.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbtkonsistendlminterview.getSelectedItemId());
                    break;
                case R.id.rbttradebankchecking :
                    orderEntry.setRELIGIUS(String.valueOf(rbttradebankchecking.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbttradebankchecking.getSelectedItemId());
                    break;
                case R.id.rbtagresivitasdlminvestasi :
                    orderEntry.setACCEPT_CREDIT_RULE(String.valueOf(rbtagresivitasdlminvestasi.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbtagresivitasdlminvestasi.getSelectedItemId());
                    break;
                case R.id.txtketsisipositifnegatif :
                    orderEntry.setETIKA_WHEN_SURVEY(String.valueOf(txtketsisipositifnegatif.getSelectedItemId()));
                    Log.d(TAG, "execute: "+txtketsisipositifnegatif.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
