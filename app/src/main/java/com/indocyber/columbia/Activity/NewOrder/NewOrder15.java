package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;

public class NewOrder15 extends AppFragment implements Step {

    private static final String TAG = "NewOrder15";
    private EditText txtjenispenghasilan, txtavgincomepermonth;
    private Spinner rbtpenghslntdkttp;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder15 newInstance() {
        return new NewOrder15();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order15, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        rbtpenghslntdkttp = (Spinner) view.findViewById(R.id.rbtpenghslntdkttp);
        txtjenispenghasilan = (EditText) view.findViewById(R.id.txtjenispenghasilan);
        txtavgincomepermonth = (EditText) view.findViewById(R.id.txtavgincomepermonth);

        try {
            setAdapterComboStringAll("24542", rbtpenghslntdkttp, true, "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        txtjenispenghasilan.addTextChangedListener(new NewWatcher(txtjenispenghasilan));
        txtavgincomepermonth.addTextChangedListener(new NewWatcher(txtavgincomepermonth));
        rbtpenghslntdkttp.setOnItemSelectedListener(NewSpinner);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView() {
        Log.i(TAG, "setupView: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtjenispenghasilan.setEnabled(false);
            txtavgincomepermonth.setEnabled(false);
            rbtpenghslntdkttp.setEnabled(false);
        }
        txtjenispenghasilan.setText(orderEntry.getINCOME_TYPE());
        txtavgincomepermonth.setText(orderEntry.getAVERAGE_INCOME_EACH_MONTH());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private AdapterView.OnItemSelectedListener NewSpinner = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            orderEntry.setHAVE_ROUTINE_INCOME(String.valueOf(rbtpenghslntdkttp.getSelectedItemId()));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private class NewWatcher implements TextWatcher {
        private View view;
        private long value = 0;
        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(view.getId() == R.id.txtavgincomepermonth)
                value = Formating.getCleanResultLong(s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            if (view.getId() == R.id.txtavgincomepermonth) {
                txtavgincomepermonth.removeTextChangedListener(this);
                if(!TextUtils.isEmpty(s.toString())){
                    txtavgincomepermonth.setText(Formating.currency(value));
                    txtavgincomepermonth.setSelection(txtavgincomepermonth.getText().length());
                }
                txtavgincomepermonth.addTextChangedListener(this);
            }
            switch (view.getId()){
                case R.id.txtjenispenghasilan :
                    orderEntry.setINCOME_TYPE(result);
                    break;
                case R.id.txtavgincomepermonth :
                    orderEntry.setEARNING_EACH_MONTH(String.valueOf(value));
                    break;
            }
        }
    }
}
