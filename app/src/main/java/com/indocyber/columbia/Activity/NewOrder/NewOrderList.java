package com.indocyber.columbia.Activity.NewOrder;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.indocyber.columbia.Abstract.UserLoginChildActivity;
import com.indocyber.columbia.Adapter.OrderEntryNewAdapter;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class NewOrderList extends UserLoginChildActivity{
    private RecyclerView recyclerViewNewOrder;
    private OrderEntryNewAdapter adapter;
    private  RealmResults<OrderEntry> results;
    private Spinner filter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_list);
        //get all record
        RealmQuery<OrderEntry> query=realm.where(OrderEntry.class);
        query.equalTo("MOBILE_STATUS","NEW");
        results=query.findAll();
        Log.i("Result",results.toString());
        //init component
        recyclerViewNewOrder= (RecyclerView) findViewById(R.id.recylerViewNewOrder);
        filter= (Spinner) findViewById(R.id.spinnerFilter);
        //configure recycle view
        recyclerViewNewOrder.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewNewOrder.setItemAnimator(new DefaultItemAnimator());
        initAdapter();
        //set update listener
        if(realm.isInTransaction()){
            realm.cancelTransaction();
        }
        results.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<OrderEntry>>() {
            @Override
            public void onChange(RealmResults<OrderEntry> orderEntries, OrderedCollectionChangeSet changeSet) {
                results=orderEntries;
                adapter.notifyDataSetChanged();
            }
        });
        initFilter();
    }
    private void initAdapter(){
        adapter=new OrderEntryNewAdapter(results,R.layout.custom_neworder,getBaseContext(),this);
        //set adapter for view
        recyclerViewNewOrder.setAdapter(adapter);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        results.removeAllChangeListeners();
    }
    //init filter for list
    private void initFilter(){
        ArrayAdapter<CharSequence> adapterF=ArrayAdapter.createFromResource(this,R.array.filter_array,android.R.layout.simple_spinner_item);
        adapterF.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filter.setAdapter(adapterF);
        filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String filterText=filter.getSelectedItem().toString(); RealmQuery<OrderEntry> query=realm.where(OrderEntry.class);
                query.equalTo("MOBILE_STATUS","NEW");
                if(filterText.equals("ID")){
                    results=query.findAllSorted("ORDER_ID");
                }else if(filterText.equals("Nama")){
                    results=query.findAllSorted("NAME");
                }else if(filterText.equals("Last Update")) {
                    results=query.findAllSorted("STATUS_UPDATE",Sort.DESCENDING);
                }else{
                    results=query.findAllSorted("CUSTOMER_RESI_ADDRESS");
                }
                initAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
