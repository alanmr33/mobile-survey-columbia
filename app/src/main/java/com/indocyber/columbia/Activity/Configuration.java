package com.indocyber.columbia.Activity;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.ChildAppActivity;
import com.indocyber.columbia.R;

public class Configuration extends ChildAppActivity implements View.OnClickListener {
    public TextView txtDevID,txtAppID;
    public EditText serverURL;
    public Button buttonSave;
    public ImageButton buttonConnection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        //init component
        serverURL= (EditText) findViewById(R.id.editTextURL);
        buttonSave= (Button) findViewById(R.id.buttonSaveConfiguration);
        buttonConnection= (ImageButton) findViewById(R.id.imageButtonTestConnection);
        txtDevID= (TextView) findViewById(R.id.textViewDevID);
        txtAppID= (TextView) findViewById(R.id.textViewAppID);
        //set on click button
        buttonSave.setOnClickListener(this);
        buttonConnection.setOnClickListener(this);
        /*
        set app information
         */
        TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            txtAppID.setText(pInfo.versionName);
            txtDevID.setText(mngr.getDeviceId());
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        serverURL.setText(session.getUrl());
    }
    /*
    on click all component
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSaveConfiguration :
                if(URLUtil.isValidUrl(serverURL.getText().toString())){
                    //save config & back
                    session.setURL(serverURL.getText().toString());
                    session.setConfigured(true);
                    finish();
                }else{
                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.imageButtonTestConnection :
                if(URLUtil.isValidUrl(serverURL.getText().toString())) {
                    testConnection(serverURL.getText().toString());
                }else{
                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
