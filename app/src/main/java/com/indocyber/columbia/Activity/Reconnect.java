package com.indocyber.columbia.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.indocyber.columbia.Service.SyncRecord;

/**
 * Created by Indocyber on 30/08/2017.
 */

public class Reconnect extends Activity {
    private Intent syncRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        start service order entry
         */
        syncRecord = new Intent(this, SyncRecord.class);
        startService(syncRecord);
        finish();
    }
}
