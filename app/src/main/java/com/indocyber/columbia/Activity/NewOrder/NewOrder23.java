package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

public class NewOrder23 extends AppFragment implements Step {

    private static final String TAG = "NewOrder23";
    private Spinner cb_usia_pemohon,cb_kesehatan_pemohon,cb_kesehatan_pasangan,cb_kesehatan_penjamin,
            cb_kondisi_rumah,cb_kondisi_perabot_rumah,cb_kondisi_kendaraan,cb_kondisi_usaha,
            cb_surat_pernyataan_penjamin,cb_hub_penjamin;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder23 newInstance() {
        return new NewOrder23();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order23, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init all component
        cb_usia_pemohon= (Spinner) v.findViewById(R.id.cb_usia_pemohon);
        cb_kesehatan_pemohon= (Spinner) v.findViewById(R.id.cb_kesehatan_pemohon);
        cb_kesehatan_pasangan= (Spinner) v.findViewById(R.id.cb_kesehatan_pasangan);
        cb_kesehatan_penjamin= (Spinner) v.findViewById(R.id.cb_kesehatan_penjamin);
        cb_kondisi_rumah= (Spinner) v.findViewById(R.id.cb_kondisi_rumah);
        cb_kondisi_perabot_rumah= (Spinner) v.findViewById(R.id.cb_kondisi_perabot_rumah);
        cb_kondisi_kendaraan= (Spinner) v.findViewById(R.id.cb_kondisi_kendaraan);
        cb_kondisi_usaha= (Spinner) v.findViewById(R.id.cb_kondisi_usaha);
        cb_surat_pernyataan_penjamin= (Spinner) v.findViewById(R.id.cb_surat_pernyataan_penjamin);
        cb_hub_penjamin= (Spinner) v.findViewById(R.id.cb_hub_penjamin);
        //end init

        try {
            setAdapterCombo("25922",cb_usia_pemohon,true,"");
            setAdapterCombo("25923",cb_kesehatan_pemohon,true,"");
            setAdapterCombo("25924",cb_kesehatan_pasangan,true,"");
            setAdapterCombo("25925",cb_kesehatan_penjamin,true,"");
            setAdapterCombo("25926",cb_kondisi_rumah,true,"");
            setAdapterCombo("25927",cb_kondisi_perabot_rumah,true,"");
            setAdapterCombo("25928",cb_kondisi_kendaraan,true,"");
            setAdapterCombo("25929",cb_kondisi_usaha,true,"");
            setAdapterCombo("25930",cb_surat_pernyataan_penjamin,true,"");
            setAdapterCombo("25931",cb_hub_penjamin,true,"");
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }

        cb_usia_pemohon.setOnItemSelectedListener(new NewSpinner(cb_usia_pemohon));
        cb_kesehatan_pemohon.setOnItemSelectedListener(new NewSpinner(cb_kesehatan_pemohon));
        cb_kesehatan_pasangan.setOnItemSelectedListener(new NewSpinner(cb_kesehatan_pasangan));
        cb_kesehatan_penjamin.setOnItemSelectedListener(new NewSpinner(cb_kesehatan_penjamin));
        cb_kondisi_rumah.setOnItemSelectedListener(new NewSpinner(cb_kondisi_rumah));
        cb_kondisi_perabot_rumah.setOnItemSelectedListener(new NewSpinner(cb_kondisi_perabot_rumah));
        cb_kondisi_kendaraan.setOnItemSelectedListener(new NewSpinner(cb_kondisi_kendaraan));
        cb_kondisi_usaha.setOnItemSelectedListener(new NewSpinner(cb_kondisi_usaha));
        cb_surat_pernyataan_penjamin.setOnItemSelectedListener(new NewSpinner(cb_surat_pernyataan_penjamin));
        cb_hub_penjamin.setOnItemSelectedListener(new NewSpinner(cb_hub_penjamin));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")) {
            cb_usia_pemohon.setEnabled(false);
            cb_kesehatan_pemohon.setEnabled(false);
            cb_kesehatan_pasangan.setEnabled(false);
            cb_kesehatan_penjamin.setEnabled(false);
            cb_kondisi_rumah.setEnabled(false);
            cb_kondisi_perabot_rumah.setEnabled(false);
            cb_kondisi_kendaraan.setEnabled(false);
            cb_kondisi_usaha.setEnabled(false);
            cb_surat_pernyataan_penjamin.setEnabled(false);
            cb_hub_penjamin.setEnabled(false);
        }
        return v;
    }

    private void setupView (){
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{

        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cb_usia_pemohon :
                    orderEntry.setCUST_OLD(String.valueOf(cb_usia_pemohon.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_usia_pemohon.getSelectedItemId());
                    break;
                case R.id.cb_kesehatan_pemohon :
                    orderEntry.setCUST_HEALTY(String.valueOf(cb_kesehatan_pemohon.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kesehatan_pemohon.getSelectedItemId());
                    break;
                case R.id.cb_kesehatan_pasangan :
                    orderEntry.setCUST_HEALTY_SPOUSEORCHILD(String.valueOf(cb_kesehatan_pasangan.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kesehatan_pasangan.getSelectedItemId());
                    break;
                case R.id.cb_kesehatan_penjamin :
                    orderEntry.setHEALTY_GUAR(String.valueOf(cb_kesehatan_penjamin.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kesehatan_penjamin.getSelectedItemId());
                    break;
                case R.id.cb_kondisi_rumah :
                    orderEntry.setHOUSE_CONDITION(String.valueOf(cb_kondisi_rumah.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kondisi_rumah.getSelectedItemId());
                    break;
                case R.id.cb_kondisi_perabot_rumah :
                    orderEntry.setFURNITURE_CONDITION(String.valueOf(cb_kondisi_perabot_rumah.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kondisi_perabot_rumah.getSelectedItemId());
                    break;
                case R.id.cb_kondisi_kendaraan :
                    orderEntry.setTRANSORT_CONDITION(String.valueOf(cb_kondisi_kendaraan.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kondisi_kendaraan.getSelectedItemId());
                    break;
                case R.id.cb_kondisi_usaha :
                    orderEntry.setBUSSINESS_CONDITION(String.valueOf(cb_kondisi_usaha.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kondisi_usaha.getSelectedItemId());
                    break;
                case R.id.cb_surat_pernyataan_penjamin :
                    orderEntry.setGUAR_LETTER(String.valueOf(cb_surat_pernyataan_penjamin.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_surat_pernyataan_penjamin.getSelectedItemId());
                    break;
                case R.id.cb_hub_penjamin :
                    orderEntry.setGUARD_RELATION(String.valueOf(cb_hub_penjamin.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_hub_penjamin.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
