package com.indocyber.columbia.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.indocyber.columbia.Abstract.UserLoginActivity;
import com.indocyber.columbia.R;

public class CheckPin extends UserLoginActivity implements View.OnClickListener {
    private TextView textViewError;
    private EditText txtPIN;
    private Button btnUnlock;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_pin);
        // init component
        textViewError= (TextView) findViewById(R.id.textViewError);
        txtPIN= (EditText) findViewById(R.id.editTextPIN);
        btnUnlock= (Button) findViewById(R.id.buttonUnlock);
        btnUnlock.setOnClickListener(this);
    }
    @Override
    public void onBackPressed() {

    }
    // all on click listener here
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.buttonUnlock :
                    if(session.getPIN().equals(txtPIN.getText().toString())){
                        finish();
                    }else{
                        textViewError.setVisibility(View.VISIBLE);
                        textViewError.setText("PIN not match. Try again !");
                    }
                break;
        }
    }
}
