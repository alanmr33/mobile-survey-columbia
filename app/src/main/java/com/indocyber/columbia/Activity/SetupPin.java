package com.indocyber.columbia.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.indocyber.columbia.Abstract.UserLoginActivity;
import com.indocyber.columbia.R;

public class SetupPin extends UserLoginActivity implements View.OnClickListener {
    private TextView txtError;
    private EditText txtNewPIN,txtCNewPIN;
    private Button btnSetup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_pin);
        // init all componnet here
        txtError= (TextView) findViewById(R.id.textViewError);
        txtNewPIN= (EditText) findViewById(R.id.editTextNewPIN);
        txtCNewPIN= (EditText) findViewById(R.id.editTextNewPINConfirm);
        btnSetup= (Button) findViewById(R.id.buttonSetupPIN);
        btnSetup.setOnClickListener(this);
    }
    @Override
    public void onBackPressed() {

    }
    /*
    all on click listener
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSetupPIN :
                String newPIN=txtNewPIN.getText().toString();
                String newPINC=txtCNewPIN.getText().toString();
                if(newPIN.length()==6) {
                    if (newPIN.equals(newPINC)) {
                        session.setPIN(newPIN);
                        finish();
                    } else {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("PIN not match");
                    }
                }else{
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("PIN too short");
                }
                break;
        }
    }
}
