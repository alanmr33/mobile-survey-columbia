package com.indocyber.columbia.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.indocyber.columbia.Abstract.AppActivity;
import com.indocyber.columbia.Model.Combo;
import com.indocyber.columbia.Model.JobTitle;
import com.indocyber.columbia.Model.Occupation;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.OtherFinancingType;
import com.indocyber.columbia.Model.ProductClasification;
import com.indocyber.columbia.Model.ProductCondition;
import com.indocyber.columbia.Model.Province;
import com.indocyber.columbia.Model.Relation;
import com.indocyber.columbia.Model.Religion;
import com.indocyber.columbia.Model.StatusKunjungan;
import com.indocyber.columbia.Model.Zip;
import com.indocyber.columbia.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.internal.IOException;

public class Splash extends AppActivity implements OnRequestPermissionsResultCallback {
    private TextView textLoading;
    private int currentStep=1;
    private boolean permissionRequest=true;
    public boolean syncStatus=false;
    public String syncMessage="";
    private boolean syncDb;
    private PowerManager.WakeLock wl;
    private JSONArray listObject;
    private long sizeData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        textLoading= (TextView) findViewById(R.id.textViewLoading);
        try {
            /*
            set hash for App
             */
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                session.setHash(Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
            setStep(1);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());

        }
        // save power manager
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Iglo");
        wl.acquire();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!syncDb){
            setStep(1);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /*
    step splash
     */
    private void setStep(final int step){
        currentStep=step;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (step){
                    case 1 :
                        textLoading.setText("Check Permission ...");
                        permitStep();
                        break;
                    case 2 :
                        textLoading.setText("Check App Configuration ...");
                        configStep();
                        break;
                    case 3 :
                        textLoading.setText("Load User Information ...");
                        loginStep();
                        break;
                    case 4 :
                        textLoading.setText("Get Data From Server ...");
                        syncStep();
                        break;
                }
            }
        }, config.SPLASH);
    }
    /*
    step permission 1
     */
    private void permitStep(){
        if(checkPermission()){
            setStep(2);
        }else{
            /*
            marshmallow above
             */
            if(Build.VERSION.SDK_INT>=22){
                requestPermissions();
            }else{
                permissionRequest=false;
                setStep(2);
            }
        }
    }
    /*
    step configuration 2
     */
    private void configStep(){
        if (session.isConfigured()){
            /*
            get device ID
             */
            TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            session.setDevice(mngr.getDeviceId());
            setStep(3);
        }else{
            Intent con=new Intent(this,Configuration.class);
            startActivity(con);
        }
    }
    /*
    step login checking 3
     */
    private void loginStep(){
        if(session.isLogin()){
            setStep(4);
        }else{
            Intent login = new Intent(this,Login.class);
            startActivity(login);
        }
    }
    /*
    step sync database 4
     */
    private void syncStep(){
        sizeData=0;
        //if already synced
        if(session.isSynced()){
            Intent main = new Intent(this,Main.class);
            startActivity(main);
            finish();
        }else {
            sync();
        }
    }
    /*
    step sync 5 check sync
     */
    public void checkSync(){
        /*
        if sync success
         */
        if(syncStatus){
            session.setSynced(true);
            Intent main = new Intent(this,Main.class);
            startActivity(main);
            finish();
        }else{
            settingDialog(syncMessage);
        }
    }
    /*
    check permission in realtime
     */
    private Boolean checkPermission(){
        int phoneState= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int storagePermission= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int storagePermissionW= ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraP= ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA);
        int locationCoarse=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationFine=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        if(phoneState==PackageManager.PERMISSION_GRANTED && storagePermission==PackageManager.PERMISSION_GRANTED && locationCoarse==PackageManager.PERMISSION_GRANTED
                && locationFine==PackageManager.PERMISSION_GRANTED && storagePermissionW==PackageManager.PERMISSION_GRANTED
                &&cameraP==PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }
    }
    //Requesting permission
    private void requestPermissions() {
        if (permissionRequest) {
            permissionRequest=false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE}, config.CODE_PERMISSION);
        }
    }
    /*
    on get permission from intent
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if(requestCode==config.CODE_PERMISSION){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED){
                setStep(2);
            }else{
                Toast.makeText(this,"Please Grant Our Permission !",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
    public void sync(){
        syncDb=true;
        this.realm.beginTransaction();
        this.realm.deleteAll();
        this.realm.commitTransaction();
        httpClient.get(session.getUrl() + config.SYNC_ENDPOINT+"?token="+session.getToken(), new JsonHttpResponseHandler() {
            /*
            on get all object list
             */
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response.getBoolean("status")) {
                        listObject = response.getJSONArray("data");
                        if (listObject.length() > 0) {
                            getObject(0,1);
                        } else {
                            syncStatus=true;
                            checkSync();
                        }
                    } else {
                        Log.e(getClass().getSimpleName(), response.getString("message"));
                        syncStatus=false;
                        syncMessage=response.getString("message");
                        checkSync();
                    }
                } catch (JSONException e) {
                    Log.e(getClass().getSimpleName(), e.getMessage());
                    syncStatus=false;
                    syncMessage=e.getMessage();
                    checkSync();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.e(getClass().getSimpleName(), throwable.getMessage());
                syncStatus=false;
                syncMessage=throwable.getMessage();
                checkSync();
            }

            @Override
            public void onFailure(int status, Header[] headers, Throwable throwable, JSONObject response) {
                Log.e(getClass().getSimpleName(), throwable.getMessage());
                syncStatus=false;
                syncMessage=throwable.getMessage();
                checkSync();
            }
        });
    }
    private void getObject(final int index, final int page){
        try {
            final String objectName=listObject.getString(index);
            httpClient.get(session.getUrl()+config.OBJECT_ENDPOINT+"?token="+session.getToken()+"&object="+objectName+"&page="+page,new JsonHttpResponseHandler(){
                /*
                on get all object list
                 */
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    try {
                        if(response.getBoolean("status")){
                            /*
                            insert Data
                             */
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    try {
                                        /*
                                        switch action to do
                                         */
                                        switch (objectName){
                                            case  "MOBILEORDER" :
                                                try {
                                                    InsertToRealm(OrderEntry.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "CUSTOMER_JOB_TITLE_MST" :
                                                try {
                                                    InsertToRealm(JobTitle.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "OCCUPATION_MST" :
                                                try {
                                                    InsertToRealm(Occupation.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "OTHER_FINANCING_TYPE_MST" :
                                                try {
                                                    InsertToRealm(OtherFinancingType.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "PRODUCT_CLASSIFICATION_MST" :
                                                try {
                                                    InsertToRealm(ProductClasification.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "PRODUCT_CONDITION_MST" :
                                                try {
                                                    InsertToRealm(ProductCondition.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "PROVINCE_MST" :
                                                try {
                                                    InsertToRealm(Province.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "REFERENCE_RELATION_MST" :
                                                try {
                                                    InsertToRealm(Relation.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "RELIGION_MST" :
                                                try {
                                                    InsertToRealm(Religion.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "STATUS_KUNJUNGAN_MST" :
                                                try {
                                                    InsertToRealm(StatusKunjungan.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "COMBOBOX" :
                                                try {
                                                    InsertToRealm(Combo.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                            case  "ZIP" :
                                                try {
                                                    InsertToRealm(Zip.class, response.getJSONArray("data"),page);
                                                } catch (InterruptedException e) {
                                                    Log.e(getClass().getSimpleName(),e.getMessage());
                                                    syncStatus=false;
                                                    syncMessage=e.getMessage();
                                                    checkSync();
                                                }
                                                break;
                                        }
                                    } catch (IOException | JSONException e) {
                                        Log.e(getClass().getSimpleName(),e.getMessage());
                                        syncStatus=false;
                                        syncMessage=e.getMessage();
                                        checkSync();
                                    }
                                }
                            });
                            /*
                            if still a data get new data else get new object
                             */
                            if(response.getJSONArray("data").length()>0){
                                getObject(index,page+1);
                            }else{
                                if(index<listObject.length()-1){
                                    getObject(index+1,1);
                                }else{
                                    syncStatus=true;
                                    checkSync();
                                }
                            }
                        }else{
                            Log.e(getClass().getSimpleName(),response.getString("message"));
                            syncStatus=false;
                            syncMessage=response.getString("message");
                            checkSync();
                        }
                    } catch (JSONException e) {
                        Log.e(getClass().getSimpleName(),e.getMessage());
                        syncStatus=false;
                        syncMessage=e.getMessage();
                        checkSync();
                    }
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e(getClass().getSimpleName(),throwable.getMessage());
                    syncStatus=false;
                    syncMessage=throwable.getMessage();
                    checkSync();
                }
                @Override
                public void onFailure(int status, Header[] headers, Throwable throwable, JSONObject response){
                    Log.e(getClass().getSimpleName(),throwable.getMessage());
                    syncStatus=false;
                    syncMessage=throwable.getMessage();
                    checkSync();
                }
            });
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
            syncStatus=false;
            syncMessage=e.getMessage();
            checkSync();
        }
    }
    private void InsertToRealm(Class<? extends RealmModel> model, JSONArray objectList, int page) throws JSONException, InterruptedException {
        if(page==0){
            realm.delete(model);
        }
        for(int i=0;i<objectList.length();i++){
//            Thread.sleep(1);
            if(model.equals(OrderEntry.class)){
                objectList.getJSONObject(i).put("MOBILE_STATUS","NEW");
            }
            realm.createObjectFromJson(model,objectList.getJSONObject(i));
            setProgressSync(1);
        }
    }
    private void setProgressSync(int size){
        sizeData+=size;
        textLoading.setText("Copied Object "+sizeData+" ...");
    }
}

