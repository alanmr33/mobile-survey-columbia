package com.indocyber.columbia.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppActivity;
import com.indocyber.columbia.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class UserSettings extends AppActivity implements View.OnClickListener {
    private EditText txtUserID,txtFullname,txtOPassword,txtNPassword,txtCPassword;
    private Button btnSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        /*
        init all component
         */
        txtUserID= (EditText) findViewById(R.id.editTextUserID);
        txtFullname= (EditText) findViewById(R.id.editTextFullname);
        txtOPassword= (EditText) findViewById(R.id.editTextOPassword);
        txtCPassword= (EditText) findViewById(R.id.editTextCPassword);
        txtNPassword= (EditText) findViewById(R.id.editTextNPassword);
        btnSave= (Button) findViewById(R.id.buttonSaveSetting);
        /*
        set all on click listener
         */
        btnSave.setOnClickListener(this);

        try {
            txtUserID.setText(session.getUserdata().getString("Username"));
            txtFullname.setText(session.getUserdata().getString("Fullname"));
            txtUserID.setEnabled(false);
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    /*
    all on click event is here
     */
    @Override
    public void onClick(View v) {
        /*
        choose action by event
         */
        switch (v.getId()){
            case R.id.buttonSaveSetting :
                String username=txtUserID.getText().toString();
                final String fullname=txtFullname.getText().toString();
                String opassword=txtOPassword.getText().toString();
                String cpassword=txtCPassword.getText().toString();
                String npassword=txtNPassword.getText().toString();
                if(fullname.equals("")){
                    errorDialog("Fullname is required");
                    return;
                }
                if(!npassword.equals("")){
                    errorDialog("Old Password is required");
                    return;
                }
                if(!npassword.equals(cpassword)){
                    errorDialog("Password not match");
                    return;
                }
                /*
                show loading
                 */
                loading=new ProgressDialog(this);
                loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                loading.setCancelable(false);
                loading.setMessage("Saving changes ...");
                loading.show();
                /*
                set Params
                 */
                RequestParams params=new RequestParams();
                params.put("username",username);
                params.put("opassword",opassword);
                params.put("cpassword",cpassword);
                params.put("npassword",npassword);
                params.put("fullname",fullname);
                params.put("token",session.getToken());
                httpClient.post(session.getUrl()+config.CPASSWORD_ENDPOINT, params, new JsonHttpResponseHandler() {
                    /*
                     handle success save change
                      */
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        loading.hide();
                        try {
                            if(response.getBoolean("status")){
                                session.setUserData(session.getUserdata().put("Fullname",fullname));
                                finish();
                            }else{
                                errorDialog(response.getString("message"));
                            }
                        } catch (JSONException e) {
                            Log.e(getClass().getSimpleName(),e.getMessage());
                        }
                    }
                    /*
                    handle error save
                     */
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        loading.hide();
                        errorDialog("Server Error");
                    }
                });
                break;
        }
    }
}
