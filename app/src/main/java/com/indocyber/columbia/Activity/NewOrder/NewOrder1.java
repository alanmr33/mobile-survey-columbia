package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.JobTitleAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.JobTitle;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NewOrder1 extends AppFragment implements Step {
    private EditText txtcustname,txtnickname,txtmmn,txtnpwppersonal,txtidnumberpersonal,txtdependants;
    private Spinner cbsexpersonal,cbtitlepersonaldata,cbjobtitle,cbmaritalstspersonal,cbidtypepersonaldata;
    private static final String TAG = "NewOrderPart1";
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;

    public static NewOrder1 newInstance (){
        return new NewOrder1();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order1, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        cbsexpersonal = (Spinner) v.findViewById(R.id.cbsexpersonal);
        cbtitlepersonaldata = (Spinner) v.findViewById(R.id.cbtitlepersonaldata);
        cbmaritalstspersonal = (Spinner) v.findViewById(R.id.cbmaritalstspersonal);
        cbidtypepersonaldata = (Spinner) v.findViewById(R.id.cbidtypepersonaldata);
        cbjobtitle = (Spinner) v.findViewById(R.id.cbjobtitle);
        txtcustname = (EditText) v.findViewById(R.id.txtcustname);
        txtnickname = (EditText) v.findViewById(R.id.txtnickname);
        txtmmn = (EditText) v.findViewById(R.id.txtmmn);
        txtnpwppersonal = (EditText) v.findViewById(R.id.txtnpwppersonal);
        txtidnumberpersonal = (EditText) v.findViewById(R.id.txtidnumberpersonal);
        txtdependants = (EditText) v.findViewById(R.id.txtdependants);
        //end init

        //set value list
        try {
            setAdapterCombo("25730",cbsexpersonal,true,orderEntry.getGENDER());
            setAdapterCombo("25703",cbtitlepersonaldata,true,orderEntry.getFIRST_TITLE());
            setAdapterCombo("25705",cbmaritalstspersonal,true,orderEntry.getMARITAL_STATUS());
            setAdapterCombo("25707",cbidtypepersonaldata,false,orderEntry.getPERS_IDENTIFY_TYPE());
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        // note : masih dummy job title - alan mr
        initJobTitle("");

        // Watcher EditText and Spinner
        txtnickname.addTextChangedListener(new NewWatcher(txtnickname));
        txtnpwppersonal.addTextChangedListener(new NewWatcher(txtnpwppersonal));
        txtdependants.addTextChangedListener(new NewWatcher(txtdependants));
        cbtitlepersonaldata.setOnItemSelectedListener(new NewSpinner(cbtitlepersonaldata));
        cbsexpersonal.setOnItemSelectedListener(new NewSpinner(cbsexpersonal));
        cbjobtitle.setOnItemSelectedListener(new NewSpinner(cbjobtitle));
        cbmaritalstspersonal.setOnItemSelectedListener(new NewSpinner(cbmaritalstspersonal));

        return v;
    }


    private void initJobTitle(final String value){
        /*setup combo box job title*/
                RealmQuery<JobTitle> jobTitleRealmQuery = realm.where(JobTitle.class);
                RealmResults<JobTitle> jobTitleRealmResults=jobTitleRealmQuery.findAll();
                //set selected Item
                int selectedIndex=0;
                for (int i=0;i<jobTitleRealmResults.size();i++){
                    if(value.equals(jobTitleRealmResults.get(i).getCUST_JOB_TITLE_ID())){
                        selectedIndex=i;
                        break;
                    }
                }
                JobTitleAdapter jobTitleAdapter=new JobTitleAdapter(getActivity(),R.layout.spinner_item,jobTitleRealmResults);
                cbjobtitle.setAdapter(jobTitleAdapter);
                cbjobtitle.setSelection(selectedIndex);

    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtcustname.setEnabled(false);
            txtnickname.setEnabled(false);
            txtmmn.setEnabled(false);
            txtnpwppersonal.setEnabled(false);
            txtidnumberpersonal.setEnabled(false);
            txtdependants.setEnabled(false);
            cbsexpersonal.setEnabled(false);
            cbtitlepersonaldata.setEnabled(false);
            cbjobtitle.setEnabled(false);
            cbmaritalstspersonal.setEnabled(false);
            cbidtypepersonaldata.setEnabled(false);
        }
        txtcustname.setText(orderEntry.getNAME());
        txtmmn.setText(orderEntry.getMOTHER_NAME());
        txtnickname.setText(orderEntry.getNICKNAME());
        txtnpwppersonal.setText(orderEntry.getNPWP_NO());
        txtidnumberpersonal.setText(orderEntry.getIDENTIFY_NO());
        txtidnumberpersonal.setEnabled(false);
        txtcustname.setEnabled(false);
        txtmmn.setEnabled(false);

    }

    private VerificationError Validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorCode = "";
        if (TextUtils.isEmpty(txtcustname.getText())){
            txtcustname.setError(NewOrderActivity.ErrorMessage);
            errorCode = NewOrderActivity.ErrorMessageLong;
        } if (TextUtils.isEmpty(txtmmn.getText())){
            txtmmn.setError(NewOrderActivity.ErrorMessage);
            errorCode = NewOrderActivity.ErrorMessageLong;
        } if (TextUtils.isEmpty(txtidnumberpersonal.getText())){
            txtidnumberpersonal.setError(NewOrderActivity.ErrorMessage);
            errorCode = NewOrderActivity.ErrorMessageLong;
        }
//
//        if (!TextUtils.isEmpty(errorCode)){
//            Toast.makeText(getContext(),errorCode,Toast.LENGTH_LONG).show();
//        }

        return TextUtils.isEmpty(errorCode) ? null : new VerificationError(errorCode);
    }

    @Override
    public VerificationError verifyStep() {
        return Validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            switch (view.getId()){
                case R.id.txtnickname:
                    orderEntry.setNICKNAME(result);
                    break;
                case R.id.txtnpwppersonal:
                    orderEntry.setNPWP_NO(result);
                    break;
                case R.id.txtdependants:
                    orderEntry.setDEPENDANTS(result);
                    break;
            }
        }
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbtitlepersonaldata :
                    orderEntry.setFIRST_TITLE(String.valueOf(cbtitlepersonaldata.getSelectedItemId()));
                    Log.d(TAG, "onItemSelected: "+orderEntry.getFIRST_TITLE());
                    break;
                case R.id.cbsexpersonal :
                    orderEntry.setGENDER(String.valueOf(cbsexpersonal.getSelectedItemId()));
                    Log.d(TAG, "onItemSelected: "+orderEntry.getGENDER());
                    break;
                case R.id.cbjobtitle :
                    orderEntry.setJOB_TITLE(String.valueOf(cbjobtitle.getSelectedItemId()));
                    Log.d(TAG, "onItemSelected: "+orderEntry.getJOB_TITLE());
                    break;
                case R.id.cbmaritalstspersonal :
                    orderEntry.setMARITAL_STATUS(String.valueOf(cbmaritalstspersonal.getSelectedItemId()));
                    Log.d(TAG, "onItemSelected: "+orderEntry.getMARITAL_STATUS());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

}
