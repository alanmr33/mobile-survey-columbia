package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

public class NewOrder17 extends AppFragment implements Step {

    private static final String TAG = "NewOrder17";
    private Spinner cb_bca_acc_exist, cb_general_checking_acc_exist, cb_saving_exist, cb_deposits_exist,
            cb_other_financing_exist, cb_other_financing_type;
    private EditText txt_bank_credit, txt_other_loan_installment;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder17 newInstance() {
        return new NewOrder17();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order17, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        cb_bca_acc_exist = (Spinner) view.findViewById(R.id.cb_bca_acc_exist);
        cb_general_checking_acc_exist = (Spinner) view.findViewById(R.id.cb_general_checking_acc_exist);
        cb_saving_exist = (Spinner) view.findViewById(R.id.cb_saving_exist);
        cb_deposits_exist = (Spinner) view.findViewById(R.id.cb_deposits_exist);
        cb_other_financing_exist = (Spinner) view.findViewById(R.id.cb_other_financing_exist);
        cb_other_financing_type = (Spinner) view.findViewById(R.id.cb_other_financing_type);
        txt_bank_credit = (EditText) view.findViewById(R.id.txt_bank_credit);
        txt_other_loan_installment = (EditText) view.findViewById(R.id.txt_other_loan_installment);

        try {
            setAdapterComboStringAll("24577",cb_bca_acc_exist,true,"");
            setAdapterComboStringAll("24578",cb_general_checking_acc_exist,true,"");
            setAdapterComboStringAll("24579",cb_saving_exist,true,"");
            setAdapterComboStringAll("24580",cb_deposits_exist,true,"");
            setAdapterComboStringAll("24581",cb_other_financing_exist,true,"");
            setAdapterComboStringAll("24582",cb_other_financing_type,true,"");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        txt_bank_credit.addTextChangedListener(new NewWatcher(txt_bank_credit));
        txt_other_loan_installment.addTextChangedListener(new NewWatcher(txt_other_loan_installment));

        cb_bca_acc_exist.setOnItemSelectedListener(new NewSpinner(cb_bca_acc_exist));
        cb_general_checking_acc_exist.setOnItemSelectedListener(new NewSpinner(cb_general_checking_acc_exist));
        cb_saving_exist.setOnItemSelectedListener(new NewSpinner(cb_saving_exist));
        cb_deposits_exist.setOnItemSelectedListener(new NewSpinner(cb_deposits_exist));
        cb_other_financing_exist.setOnItemSelectedListener(new NewSpinner(cb_other_financing_exist));
        cb_other_financing_type.setOnItemSelectedListener(new NewSpinner(cb_other_financing_type));
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txt_bank_credit.setEnabled(false);
            txt_other_loan_installment.setEnabled(false);
            cb_bca_acc_exist.setEnabled(false);
            cb_general_checking_acc_exist.setEnabled(false);
            cb_saving_exist.setEnabled(false);
            cb_deposits_exist.setEnabled(false);
            cb_other_financing_exist.setEnabled(false);
            cb_other_financing_type.setEnabled(false);
        }
        txt_bank_credit.setText(orderEntry.getOTHER_BANK_OR_FINACE());
        txt_other_loan_installment.setText(orderEntry.getINSTALLMENT());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener {
        private View v;

        private NewSpinner(View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cb_bca_acc_exist :
                    orderEntry.setHAVE_BCA_ACCOUNT(String.valueOf(cb_bca_acc_exist.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_bca_acc_exist.getSelectedItemId());
                    break;
                case R.id.cb_general_checking_acc_exist :
                    orderEntry.setACCOUNT_LATTER(String.valueOf(cb_general_checking_acc_exist.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_general_checking_acc_exist.getSelectedItemId());
                    break;
                case R.id.cb_saving_exist :
                    orderEntry.setHAVE_SAVING(String.valueOf(cb_saving_exist.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_saving_exist.getSelectedItemId());
                    break;
                case R.id.cb_deposits_exist :
                    orderEntry.setHAVE_DEPOSITO(String.valueOf(cb_deposits_exist.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_deposits_exist.getSelectedItemId());
                    break;
                case R.id.cb_other_financing_exist :
                    orderEntry.setHAVE_OTHER_CREDIT_CARD(String.valueOf(cb_other_financing_exist.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_other_financing_exist.getSelectedItemId());
                    break;
                case R.id.cb_other_financing_type :
                    orderEntry.setFASILITY_TYPE(String.valueOf(cb_other_financing_type.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_other_financing_type.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher {
        private View view;
        private long value = 0;

        private NewWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(view.getId() == R.id.txt_other_loan_installment)
                    value = Formating.getCleanResultLong(s.toString());
            Log.d(TAG, "onTextChanged: "+value);
        }

        @Override
        public void afterTextChanged(Editable s) {
            final String text = s.toString();
            Log.d(TAG, "afterTextChanged: "+text);
            if (view.getId() == R.id.txt_other_loan_installment){
                txt_other_loan_installment.removeTextChangedListener(this);
                if(!TextUtils.isEmpty(s.toString())){
                    txt_other_loan_installment.setText(Formating.currency(value));
                    txt_other_loan_installment.setSelection(txt_other_loan_installment.getText().length());
                }
                txt_other_loan_installment.addTextChangedListener(this);
            }
            switch (view.getId()){
                case R.id.txt_bank_credit :
                    orderEntry.setOTHER_BANK_OR_FINACE(text);
                    break;
                case R.id.txt_other_loan_installment :
                    orderEntry.setINSTALLMENT(String.valueOf(value));
                    break;
            }
        }
    }
}
