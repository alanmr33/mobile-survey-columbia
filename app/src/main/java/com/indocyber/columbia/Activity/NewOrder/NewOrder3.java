package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.ProvinceAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.Province;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class NewOrder3 extends AppFragment implements Step {
    private static final String TAG = "NewOrder3";
    private EditText txtcustidzip,txtcustiddatill,txtothercustaddr,txtothercustrt,
            txtothercustrw,txtothercustkel,txtothercustkec,txtothercustkab,txtothercustzip,
            txtothercustsandill;
    private Spinner cbothercustprov,rbtboolothercustaddr;
    private LinearLayout layoutWraperOAdress;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;

    public static NewOrder3 newInstance() {
       return new NewOrder3();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order3, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        txtcustidzip= (EditText) v.findViewById(R.id.txtcustidzip);
        txtcustiddatill= (EditText) v.findViewById(R.id.txtcustiddatill);
        txtothercustaddr= (EditText) v.findViewById(R.id.txtothercustaddr);
        txtothercustrt= (EditText) v.findViewById(R.id.txtothercustrt);
        txtothercustrw= (EditText) v.findViewById(R.id.txtothercustrw);
        txtothercustkel= (EditText) v.findViewById(R.id.txtothercustkel);
        txtothercustkec= (EditText) v.findViewById(R.id.txtothercustkec);
        txtothercustkab= (EditText) v.findViewById(R.id.txtothercustkab);
        txtothercustzip= (EditText) v.findViewById(R.id.txtothercustzip);
        txtothercustsandill= (EditText) v.findViewById(R.id.txtothercustsandill);
        cbothercustprov= (Spinner) v.findViewById(R.id.cbothercustprov);
        rbtboolothercustaddr= (Spinner) v.findViewById(R.id.rbtboolothercustaddr);
        layoutWraperOAdress= (LinearLayout) v.findViewById(R.id.layoutWraperOAdress);
        //end init component
        rbtboolothercustaddr.setOnItemSelectedListener(addresCustomer);
        //set value list
        try {
            setAdapterCombo("26895",rbtboolothercustaddr,true,"1");
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        initProvince("");

        txtcustidzip.addTextChangedListener(new NewWatcher(txtcustidzip));
        txtcustiddatill.addTextChangedListener(new NewWatcher(txtcustiddatill));
        txtothercustaddr.addTextChangedListener(new NewWatcher(txtothercustaddr));
        txtothercustrt.addTextChangedListener(new NewWatcher(txtothercustrt));
        txtothercustrw.addTextChangedListener(new NewWatcher(txtothercustrw));
        txtothercustkel.addTextChangedListener(new NewWatcher(txtothercustkel));
        txtothercustkec.addTextChangedListener(new NewWatcher(txtothercustkec));
        txtothercustkab.addTextChangedListener(new NewWatcher(txtothercustkab));
        txtothercustzip.addTextChangedListener(new NewWatcher(txtothercustzip));
        txtothercustsandill.addTextChangedListener(new NewWatcher(txtothercustsandill));
        cbothercustprov.setOnItemSelectedListener(spinnerPropinsi);

        return v;
    }

    private AdapterView.OnItemSelectedListener addresCustomer = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(position==1){
                layoutWraperOAdress.setVisibility(View.VISIBLE);
            }else{
                layoutWraperOAdress.setVisibility(View.GONE);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {}
    };

    private void initProvince(String value){
        /*
        setup combo box province
         */
        RealmQuery<Province> provinceRealmQuery = realm.where(Province.class);
        RealmResults<Province> provinceRealmResults = provinceRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<provinceRealmResults.size();i++){
            if(value.equals(provinceRealmResults.get(i).getPROVINCE_ID())){
                selectedIndex=i;
                break;
            }
        }
        ProvinceAdapter provinceAdapter = new ProvinceAdapter(getActivity(),R.layout.spinner_item,provinceRealmResults);
        cbothercustprov.setAdapter(provinceAdapter);
        cbothercustprov.setSelection(selectedIndex);
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtcustidzip.setEnabled(false);
            txtcustiddatill.setEnabled(false);
            txtothercustaddr.setEnabled(false);
            txtothercustrt.setEnabled(false);
            txtothercustrw.setEnabled(false);
            txtothercustkel.setEnabled(false);
            txtothercustkec.setEnabled(false);
            txtothercustkab.setEnabled(false);
            txtothercustzip.setEnabled(false);
            txtothercustsandill.setEnabled(false);
            cbothercustprov.setEnabled(false);
            rbtboolothercustaddr.setEnabled(false);
        }
        txtcustidzip.setText(orderEntry.getCUSTOMER_RESI_POSTCODE());
        txtcustiddatill.setText(orderEntry.getSANDIDATIII());
        txtothercustaddr.setText(orderEntry.getCUSTOMER_RESI_ADDRESS());
        txtothercustrt.setText(orderEntry.getCUSTOMER_RESI_RT());
        txtothercustrw.setText(orderEntry.getCUSTOMER_RESI_RW());
        txtothercustkel.setText(orderEntry.getCUSTOMER_RESI_VILLAGE());
        txtothercustkec.setText(orderEntry.getCUSTOMER_RESI_DISTRICT());
        txtothercustkab.setText(orderEntry.getCUSTOMER_RESI_REGENCY());
        txtothercustzip.setText(orderEntry.getCUSTOMER_RESI_POSCODE_OTHER());
        txtothercustsandill.setText(orderEntry.getSANDIDATITWO_OTHER());
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        if (TextUtils.isEmpty(txtcustidzip.getText())){
            txtcustidzip.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txtcustiddatill.getText())){
            txtcustiddatill.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return TextUtils.isEmpty(errorMessage) ? null : new VerificationError(errorMessage);
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private AdapterView.OnItemSelectedListener spinnerPropinsi = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Province province = (Province) cbothercustprov.getSelectedItem();
            orderEntry.setCUSTOMER_RESI_PROVINCE(province.getPROVINCE_ID());
            Log.d(TAG, "execute: "+province.getPROVINCE_ID());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtcustidzip :
                    orderEntry.setCUSTOMER_RESI_POSTCODE(result);
                    break;
                case R.id.txtcustiddatill :
                    orderEntry.setSANDIDATIII(result);
                    break;
                case R.id.txtothercustaddr :
                    orderEntry.setCUSTOMER_RESI_ADDRESS(result);
                    break;
                case R.id.txtothercustrt :
                    orderEntry.setCUSTOMER_RESI_RT(result);
                    break;
                case R.id.txtothercustrw :
                    orderEntry.setCUSTOMER_RESI_RW(result);
                    break;
                case R.id.txtothercustkel :
                    orderEntry.setCUSTOMER_RESI_VILLAGE(result);
                    break;
                case R.id.txtothercustkec :
                    orderEntry.setCUSTOMER_RESI_DISTRICT(result);
                    break;
                case R.id.txtothercustkab :
                    orderEntry.setCUSTOMER_RESI_REGENCY(result);
                    break;
                case R.id.txtothercustzip :
                    orderEntry.setCUSTOMER_RESI_POSCODE_OTHER(result);
                    break;
                case R.id.txtothercustsandill :
                    orderEntry.setSANDIDATITWO_OTHER(result);
                    break;
            }
        }
    }
}
