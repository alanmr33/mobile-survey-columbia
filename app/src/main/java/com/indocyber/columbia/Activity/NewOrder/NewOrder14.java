package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.JobTitleAdapter;
import com.indocyber.columbia.Adapter.OccupationAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.JobTitle;
import com.indocyber.columbia.Model.Occupation;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NewOrder14 extends AppFragment implements Step {

    private static final String TAG = "NewOrder14";
    private EditText txt_spouse_income, txt_spouse_length_of_work_year, txtspousecokab, txtspousecozip,
            txtspousecobidangusaha, txtspousecoaddr, txtspouseconame;
    private Spinner cbmaritalstspenghasilansuami, cbspousejobid, cbspousejabatan,
            cb_spouse_length_of_work_month;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;

    public static NewOrder14 newInstance() {
        return new NewOrder14();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order14, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        txt_spouse_income = (EditText) view.findViewById(R.id.txt_spouse_income);
        txt_spouse_length_of_work_year = (EditText) view.findViewById(R.id.txt_spouse_length_of_work_year);
        txtspousecokab = (EditText) view.findViewById(R.id.txtspousecokab);
        txtspousecozip = (EditText) view.findViewById(R.id.txtspousecozip);
        txtspousecobidangusaha = (EditText) view.findViewById(R.id.txtspousecobidangusaha);
        txtspousecoaddr = (EditText) view.findViewById(R.id.txtspousecoaddr);
        txtspouseconame = (EditText) view.findViewById(R.id.txtspouseconame);
        cbmaritalstspenghasilansuami = (Spinner) view.findViewById(R.id.cbmaritalstspenghasilansuami);
        cbspousejobid = (Spinner) view.findViewById(R.id.cbspousejobid);
        cbspousejabatan = (Spinner) view.findViewById(R.id.cbspousejabatan);
        cb_spouse_length_of_work_month = (Spinner) view.findViewById(R.id.cb_spouse_length_of_work_month);

        try {
            setAdapterCombo("25678",cbmaritalstspenghasilansuami,true,"");
            setAdapterCombo("25679",cbspousejobid,true,"");
            setAdapterCombo("25860",cbspousejabatan,true,"");
            setAdapterCombo("25687",cb_spouse_length_of_work_month,true,"");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new NewOrderActivity().setMonthAdapter(getActivity(),cb_spouse_length_of_work_month);
        initJobTitle("");
        initJobOccupation("");

        txt_spouse_income.addTextChangedListener(new NewWatcher(txt_spouse_income));
        txt_spouse_length_of_work_year.addTextChangedListener(new NewWatcher(txt_spouse_length_of_work_year));
        txtspousecokab.addTextChangedListener(new NewWatcher(txtspousecokab));
        txtspousecozip.addTextChangedListener(new NewWatcher(txtspousecozip));
        txtspousecobidangusaha.addTextChangedListener(new NewWatcher(txtspousecobidangusaha));
        txtspousecoaddr.addTextChangedListener(new NewWatcher(txtspousecoaddr));
        txtspouseconame.addTextChangedListener(new NewWatcher(txtspouseconame));

        cbmaritalstspenghasilansuami.setOnItemSelectedListener(new NewSpinner(cbmaritalstspenghasilansuami));
        cbspousejobid.setOnItemSelectedListener(new NewSpinner(cbspousejobid));
        cbspousejabatan.setOnItemSelectedListener(new NewSpinner(cbspousejabatan));
        cb_spouse_length_of_work_month.setOnItemSelectedListener(new NewSpinner(cb_spouse_length_of_work_month));

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txt_spouse_income.setEnabled(false);
            txt_spouse_length_of_work_year.setEnabled(false);
            txtspousecokab.setEnabled(false);
            txtspousecozip.setEnabled(false);
            txtspousecobidangusaha.setEnabled(false);
            txtspousecoaddr.setEnabled(false);
            txtspouseconame.setEnabled(false);
            cbmaritalstspenghasilansuami.setEnabled(false);
            cbspousejobid.setEnabled(false);
            cbspousejabatan.setEnabled(false);
            cb_spouse_length_of_work_month.setEnabled(false);
        }
    }
    private void initJobTitle(String value){
        /*
        setup combo box job title
         */
        RealmQuery<JobTitle> jobTitleRealmQuery = realm.where(JobTitle.class);
        RealmResults<JobTitle> jobTitleRealmResults=jobTitleRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<jobTitleRealmResults.size();i++){
            if(value.equals(jobTitleRealmResults.get(i).getCUST_JOB_TITLE_ID())){
                selectedIndex=i;
                break;
            }
        }
        JobTitleAdapter jobTitleAdapter=new JobTitleAdapter(getActivity(),R.layout.spinner_item,jobTitleRealmResults);
        cbspousejabatan.setAdapter(jobTitleAdapter);
        cbspousejabatan.setSelection(selectedIndex);
    }
    private void initJobOccupation(String value){
        /*
        setup combo box job title
         */
        RealmQuery<Occupation> occupationRealmQuery = realm.where(Occupation.class);
        RealmResults<Occupation> occupationRealmResults=occupationRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<occupationRealmResults.size();i++){
            if(value.equals(occupationRealmResults.get(i).getOCCUPATION_ID())){
                selectedIndex=i;
                break;
            }
        }
        OccupationAdapter occupationAdapter=new OccupationAdapter(getActivity(),R.layout.spinner_item,occupationRealmResults);
        cbspousejobid.setAdapter(occupationAdapter);
        cbspousejobid.setSelection(selectedIndex);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;
        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbmaritalstspenghasilansuami :
                    orderEntry.setSPOUSE_MARITAL_STATUS(String.valueOf(cbmaritalstspenghasilansuami.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbmaritalstspenghasilansuami.getSelectedItemId());
                    break;
                case R.id.cbspousejobid :
                    orderEntry.setJENIS_PEKERJAAN(String.valueOf(cbspousejobid.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbspousejobid.getSelectedItemId());
                    break;
                case R.id.cbspousejabatan :
                    orderEntry.setSPOUSE_TITLE_P14(String.valueOf(cbspousejabatan.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbspousejabatan.getSelectedItemId());
                    break;
                case R.id.cb_spouse_length_of_work_month :
                    orderEntry.setSPOUSE_DURATION_OF_WORK_MONTH(String.valueOf(cb_spouse_length_of_work_month.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_spouse_length_of_work_month.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher{
        private View view;
        private long value = 0;
        private NewWatcher (View view){
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(view.getId() == R.id.txt_spouse_income)
                value = Formating.getCleanResultLong(s.toString());
            Log.d(TAG, "onTextChanged: "+value);

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            if(view.getId() == R.id.txt_spouse_income) {
                txt_spouse_income.removeTextChangedListener(this);
                if(!TextUtils.isEmpty(s.toString())){
                    txt_spouse_income.setText(Formating.currency(value));
                    txt_spouse_income.setSelection(txt_spouse_income.getText().length());
                }
                txt_spouse_income.addTextChangedListener(this);
            }
            switch (view.getId()) {
                case R.id.txtspouseconame:
                    orderEntry.setSPOUSE_OFFICE_NAME(result);
                    break;
                case R.id.txtspousecoaddr:
                    orderEntry.setSPOUSE_ADDRESS(result);
                    break;
                case R.id.txtspousecokab:
                    orderEntry.setSPOUSE_KAB(result);
                    break;
                case R.id.txtspousecozip:
                    orderEntry.setSPOUSE_POSCODE(result);
                    break;
                case R.id.txtspousecobidangusaha:
                    orderEntry.setSPOUSE_BUSSINESS_FIELD(result);
                    break;
                case R.id.txt_spouse_length_of_work_year:
                    orderEntry.setSPOUSE_DURATION_OF_WORK_YEARS(result);
                    break;
                case R.id.txt_spouse_income :
                    orderEntry.setEARNING_EACH_MONTH(String.valueOf(value));
                    break;
            }
        }
    }
}
