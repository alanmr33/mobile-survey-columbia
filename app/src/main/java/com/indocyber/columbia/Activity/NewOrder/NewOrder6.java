package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class NewOrder6 extends AppFragment implements Step {
    private static final String TAG = "NewOrder6";
    private EditText txtcofaxareacode,txtcofaxnumber,txtlengthofwork;
    private Spinner cbmonthofwork;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder6 newInstance() {
        return new NewOrder6();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order6, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        txtcofaxareacode = (EditText) v.findViewById(R.id.txtcofaxareacode);
        txtcofaxnumber = (EditText) v.findViewById(R.id.txtcofaxnumber);
        txtlengthofwork = (EditText) v.findViewById(R.id.txtlengthofwork);
        cbmonthofwork = (Spinner) v.findViewById(R.id.cbmonthofwork);
        //end init
        new NewOrderActivity().setMonthAdapter(getActivity(),cbmonthofwork);

        txtcofaxareacode.addTextChangedListener(new NewWatcher(txtcofaxareacode));
        txtcofaxnumber.addTextChangedListener(new NewWatcher(txtcofaxnumber));
        txtlengthofwork.addTextChangedListener(new NewWatcher(txtlengthofwork));

        cbmonthofwork.setOnItemSelectedListener(spinnerMonth);

        return v;
    }

    private void setupView(){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtcofaxareacode.setEnabled(false);
            txtcofaxnumber.setEnabled(false);
            txtlengthofwork.setEnabled(false);
            cbmonthofwork.setEnabled(false);
        }
        txtcofaxareacode.setText(orderEntry.getFAX_AREACODE());
        txtcofaxnumber.setText(orderEntry.getFAX_NO());
        txtlengthofwork.setText(orderEntry.getLENGHOFWORK());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();

    }

    private AdapterView.OnItemSelectedListener spinnerMonth = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            orderEntry.setDURATION_JOB_MONTH(String.valueOf(cbmonthofwork.getSelectedItemId()));
            Log.d(TAG, "execute: "+cbmonthofwork.getSelectedItemId());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtstayyear :
                    orderEntry.setDURATION_STAY_YEAR(result);
                    break;
                case R.id.txtphoneareacode :
                    orderEntry.setTELEPHONE_AREA(result);
                    break;
                case R.id.txtphonenumber :
                    orderEntry.setTELEPHONE_NUMBER(result);
                    break;
            }
        }
    }

}
