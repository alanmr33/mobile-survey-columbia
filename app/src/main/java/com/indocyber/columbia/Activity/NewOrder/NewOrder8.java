package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class NewOrder8 extends AppFragment implements Step {
    private static final String TAG = "NewOrder8";
    private EditText txt_gross_income_amt,txt_add_income_amt,txt_add_income_source;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    public static NewOrder8 newInstance() {
        return new NewOrder8();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order8, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init
        txt_gross_income_amt = (EditText) v.findViewById(R.id.txt_gross_income_amt);
        txt_add_income_amt = (EditText) v.findViewById(R.id.txt_add_income_amt);
        txt_add_income_source = (EditText) v.findViewById(R.id.txt_add_income_source);
        //end init

        txt_gross_income_amt.addTextChangedListener(new NewWatcher(txt_gross_income_amt));
        txt_add_income_amt.addTextChangedListener(new NewWatcher(txt_add_income_amt));
        txt_add_income_source.addTextChangedListener(new NewWatcher(txt_add_income_source));
        return v;
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        if (TextUtils.isEmpty(txt_gross_income_amt.getText())){
            txt_gross_income_amt.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txt_gross_income_amt.setEnabled(false);
            txt_add_income_amt.setEnabled(false);
            txt_add_income_source.setEnabled(false);
        }
        txt_gross_income_amt.setText(orderEntry.getGROSS_FIX_INCOME());
        txt_add_income_amt.setText(orderEntry.getADDITIONAL_INCOME());
        txt_add_income_source.setText(orderEntry.getADD_INCOME_SRC());
    }

    private class NewWatcher implements TextWatcher {
        private long result = 0;
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(view.getId() != R.id.txt_add_income_source) {
                result = Formating.getCleanResultLong(s.toString());
                Log.d(TAG, "onTextChanged: "+result);
                switch (view.getId()){
                    case R.id.txt_gross_income_amt :
                        orderEntry.setGROSS_FIX_INCOME(String.valueOf(result));
                        break;
                    case R.id.txt_add_income_amt :
                        orderEntry.setADDITIONAL_INCOME(String.valueOf(result));
                        break;
                }
            }

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String value = s.toString();
            Log.d(TAG, "afterTextChanged: "+value);
            switch (view.getId()){
                case R.id.txt_gross_income_amt :
                    txt_gross_income_amt.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_gross_income_amt.setText(Formating.currency(result));
                        txt_gross_income_amt.setSelection(txt_gross_income_amt.getText().length());
                    }
                    txt_gross_income_amt.addTextChangedListener(this);
                    break;
                case R.id.txt_add_income_amt :
                    txt_add_income_amt.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_add_income_amt.setText(Formating.currency(result));
                        txt_add_income_amt.setSelection(txt_add_income_amt.getText().length());
                    }
                    txt_add_income_amt.addTextChangedListener(this);
                    break;
                case R.id.txt_add_income_source :
                        orderEntry.setADD_INCOME_SOURCE(value);
                    break;
            }
        }
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }
}
