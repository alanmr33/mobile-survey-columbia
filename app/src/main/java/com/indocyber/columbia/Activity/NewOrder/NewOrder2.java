package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.ProvinceAdapter;
import com.indocyber.columbia.Adapter.ReligionAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Contract.DateContract;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.Province;
import com.indocyber.columbia.Model.Religion;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class NewOrder2 extends AppFragment implements DateContract, Step {
    private EditText txtbirthplacepersonal,dpbirthdatepersonaldata,txtcustidaddr,txtcustidrt,txtcustidrw,
            txtcustidkel,txtcustidkec,txtcustidkab, txtnoreklistrik;
    private Spinner cbcustidprov,cbreligionpersonal;
    private static final String TAG = "NewOrder2";
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;
    public static NewOrder2 newInstance() {
        return new NewOrder2();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order2, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init all component
        txtbirthplacepersonal = (EditText) v.findViewById(R.id.txtbirthplacepersonal);
        dpbirthdatepersonaldata = (EditText) v.findViewById(R.id.dpbirthdatepersonaldata);
        txtcustidrw = (EditText) v.findViewById(R.id.txtcustidrw);
        txtcustidkel = (EditText) v.findViewById(R.id.txtcustidkel);
        txtcustidaddr = (EditText) v.findViewById(R.id.txtcustidaddr);
        txtcustidkec = (EditText) v.findViewById(R.id.txtcustidkec);
        txtcustidrt = (EditText) v.findViewById(R.id.txtcustidrt);
        txtcustidkab = (EditText) v.findViewById(R.id.txtcustidkab);
        cbreligionpersonal = (Spinner) v.findViewById(R.id.cbreligionpersonal);
        cbcustidprov = (Spinner) v.findViewById(R.id.cbcustidprov);
        txtnoreklistrik = (EditText) v.findViewById(R.id.txtnoreklistrik);
        //end init component

        //init all spinner
        initReligion(orderEntry.getPERS_RELIGION());
        initProvince(orderEntry.getCUSTOMER_RESI_PROVINCE());

        //Watcher Edittex and Spinner
        txtnoreklistrik.addTextChangedListener(new NewWatcher(txtnoreklistrik));
        txtcustidaddr.addTextChangedListener(new NewWatcher(txtcustidaddr));
        txtcustidrt.addTextChangedListener(new NewWatcher(txtcustidrt));
        txtcustidrw.addTextChangedListener(new NewWatcher(txtcustidrw));
        txtcustidkel.addTextChangedListener(new NewWatcher(txtcustidkel));
        txtcustidkec.addTextChangedListener(new NewWatcher(txtcustidkec));
        txtcustidkab.addTextChangedListener(new NewWatcher(txtcustidkab));
        cbcustidprov.setOnItemSelectedListener(new NewSpinner(cbcustidprov));
        cbreligionpersonal.setOnItemSelectedListener(new NewSpinner(cbreligionpersonal));

        return v;
    }

    private void initReligion(String value){
        /*
        setup combo box religion
         */
        RealmQuery<Religion> religionRealmQuery= realm.where(Religion.class);
        RealmResults<Religion> religionRealmResults=religionRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<religionRealmResults.size();i++){
            if(value.equals(religionRealmResults.get(i).getRELIGION_ID())){
                selectedIndex=i;
                break;
            }
        }
        ReligionAdapter religionAdapter=new ReligionAdapter(getActivity(),R.layout.spinner_item,religionRealmResults);
        cbreligionpersonal.setAdapter(religionAdapter);
        cbreligionpersonal.setSelection(selectedIndex);
    }
    private void initProvince(final String value){
                /*setup combo box province*/
                RealmQuery<Province> provinceRealmQuery = realm.where(Province.class);
                RealmResults<Province> provinceRealmResults=provinceRealmQuery.findAll();
                //set selected Item
                int selectedIndex=0;
                for (int i=0;i<provinceRealmResults.size();i++){
                    if(value.equals(provinceRealmResults.get(i).getPROVINCE_ID())){
                        selectedIndex=i;
                        break;
                    }
                }
                ProvinceAdapter provinceAdapter = new ProvinceAdapter
                        (getActivity(),R.layout.spinner_item,provinceRealmResults);
                cbcustidprov.setAdapter(provinceAdapter);
                cbcustidprov.setSelection(selectedIndex);
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtbirthplacepersonal.setEnabled(false);
            dpbirthdatepersonaldata.setEnabled(false);
            txtcustidaddr.setEnabled(false);
            txtcustidrt.setEnabled(false);
            txtcustidrw.setEnabled(false);
            txtcustidkel.setEnabled(false);
            txtcustidkec.setEnabled(false);
            txtcustidkab.setEnabled(false);
            txtnoreklistrik.setEnabled(false);
            cbcustidprov.setEnabled(false);
            cbreligionpersonal.setEnabled(false);
        }
        txtbirthplacepersonal.setText(orderEntry.getBIRTH_PLACE());
        dpbirthdatepersonaldata.setText(orderEntry.getCUST_BIRTH_DATE());
        txtcustidrw.setText(orderEntry.getRW());
        txtcustidrt.setText(orderEntry.getRT());
        txtcustidkel.setText(orderEntry.getKELURAHAN());
        txtcustidkec.setText(orderEntry.getKECAMATAN());
        txtcustidkab.setText(orderEntry.getKABKODYA());
        txtcustidaddr.setText(orderEntry.getADDRESS_KTP());
    }

    private VerificationError Validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        if(TextUtils.isEmpty(txtcustidaddr.getText())){
            txtcustidaddr.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txtcustidkel.getText())){
            txtcustidkel.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txtcustidkec.getText())){
            txtcustidkec.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txtcustidkab.getText())){
            txtcustidkab.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    @Override
    public void onSetData(String date) {
        final String dates = date;
        Log.d(TAG, "onSetData: "+date);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                orderEntry.setCUST_BIRTH_DATE(dates);
            }
        });
        dpbirthdatepersonaldata.setText(date);
    }

    @Override
    public VerificationError verifyStep() {
        return Validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbreligionpersonal :
                    orderEntry.setPERS_RELIGION(String.valueOf(cbreligionpersonal.getSelectedItemId()));
                    Log.d(TAG, "onItemSelected: "+cbreligionpersonal.getSelectedItemId());
                    break;
                case R.id.cbcustidprov :
                    orderEntry.setPROVINCE(String.valueOf(cbcustidprov.getSelectedItemId()));
                    Log.d(TAG, "onItemSelected: " + cbcustidprov.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher {
        private View view;
        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtnoreklistrik :
                    orderEntry.setELECTRICITY_NO(result);
                    break;
                case R.id.txtcustidaddr :
                    orderEntry.setADDRESS_KTP(result);
                    break;
                case R.id.txtcustidrt :
                    orderEntry.setRT(result);
                    break;
                case R.id.txtcustidrw :
                    orderEntry.setRW(result);
                    break;
                case R.id.txtcustidkel :
                    orderEntry.setKELURAHAN(result);
                    break;
                case R.id.txtcustidkec :
                    orderEntry.setKECAMATAN(result);
                    break;
                case R.id.txtcustidkab :
                    orderEntry.setKABKODYA(result);
                    break;
            }
        }
    }
}
