package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;

public class NewOrder24 extends AppFragment implements View.OnClickListener, Step {
    private static final String TAG = "NewOrder24";
    private static final String ErrorMessage = "Required Take picture";
    private EditText txtnote,txt_gps_coordinate,txt_gps_description;
    private ImageView p24_fotoRumah,p24_fotoDokumen,p24_fotoKonsumen;
    private TextView lblFotoKonsumen, lblFotoDocument, lblFotoRumah;
    private ImageButton button_fotoRumah,button_fotoDokumen,button_fotoKonsumen;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder24 newInstance() {
        return new NewOrder24();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order24, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        txtnote= (EditText) v.findViewById(R.id.txtnote);
        txt_gps_coordinate= (EditText) v.findViewById(R.id.txt_gps_coordinate);
        txt_gps_description= (EditText) v.findViewById(R.id.txt_gps_description);
        lblFotoDocument = (TextView) v.findViewById(R.id.lblFotoDocument);
        lblFotoKonsumen = (TextView) v.findViewById(R.id.lblFotoKonsumen);
        lblFotoRumah = (TextView)  v.findViewById(R.id.lblFotoRumah);
        p24_fotoRumah= (ImageView) v.findViewById(R.id.p24_fotoRumah);
        p24_fotoDokumen= (ImageView) v.findViewById(R.id.p24_fotoDokumen);
        p24_fotoKonsumen= (ImageView) v.findViewById(R.id.p24_fotoKonsumen);
        button_fotoRumah= (ImageButton) v.findViewById(R.id.button_fotoRumah);
        button_fotoDokumen= (ImageButton) v.findViewById(R.id.button_fotoDokumen);
        button_fotoKonsumen= (ImageButton) v.findViewById(R.id.button_fotoKonsumen);
        txt_gps_coordinate.setEnabled(false);
        txt_gps_coordinate.setText(((NewOrderActivity)getActivity()).session.getLocation());
        orderEntry.setGPS(txt_gps_coordinate.getText().toString());
        //end init

        txtnote.addTextChangedListener(new NewWatcher(txtnote));
        txt_gps_coordinate.addTextChangedListener(new NewWatcher(txt_gps_coordinate));
        txt_gps_description.addTextChangedListener(new NewWatcher(txt_gps_description));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")) {
            txtnote.setEnabled(false);
            txt_gps_coordinate.setEnabled(false);
            txt_gps_description.setEnabled(false);
            txt_gps_coordinate.setText(orderEntry.getGPS());
            button_fotoRumah.setVisibility(View.GONE);
            button_fotoDokumen.setVisibility(View.GONE);
            button_fotoKonsumen.setVisibility(View.GONE);
        }else{
            button_fotoRumah.setOnClickListener(this);
            button_fotoDokumen.setOnClickListener(this);
            button_fotoKonsumen.setOnClickListener(this);
        }
        if(!orderEntry.getHOUSE_FILE().equals("")){
            p24_fotoRumah.setImageBitmap(decodeBase64(orderEntry.getHOUSE_FILE()));
        }
        if(!orderEntry.getDOCUMENT_FILE().equals("")){
            p24_fotoDokumen.setImageBitmap(decodeBase64(orderEntry.getDOCUMENT_FILE()));
        }
        if(!orderEntry.getCONSUMENT_FILE().equals("")){
            p24_fotoKonsumen.setImageBitmap(decodeBase64(orderEntry.getCONSUMENT_FILE()));
        }
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }
    /*
    all on click
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_fotoRumah :
                ((NewOrderActivity)getActivity()).requestImage(((NewOrderActivity)getActivity()).RequestRumah);
                break;
            case R.id.button_fotoDokumen :
                ((NewOrderActivity)getActivity()).requestImage(((NewOrderActivity)getActivity()).RequestDokumen);
                break;
            case R.id.button_fotoKonsumen :
                ((NewOrderActivity)getActivity()).requestImage(((NewOrderActivity)getActivity()).RequestKonsumen);
                break;
        }
    }
    //on Resume Fragment
    @Override
    public void onResume() {
        super.onResume();
        txt_gps_coordinate.setText(((NewOrderActivity)getActivity()).session.getLocation());
        orderEntry.setGPS(txt_gps_coordinate.getText().toString());
        if(((NewOrderActivity)getActivity()).bitmapPicker.containsKey("111")){
            p24_fotoRumah.setImageBitmap(((NewOrderActivity)getActivity()).bitmapPicker.get("111"));
            orderEntry.setHOUSE_FILE(encodeToBase64(((NewOrderActivity)getActivity()).bitmapPicker.get("111"), Bitmap.CompressFormat.JPEG,100));
            p24_fotoRumah.refreshDrawableState();
        }
        if(((NewOrderActivity)getActivity()).bitmapPicker.containsKey("112")){
            p24_fotoDokumen.setImageBitmap(((NewOrderActivity)getActivity()).bitmapPicker.get("112"));
            orderEntry.setDOCUMENT_FILE(encodeToBase64(((NewOrderActivity)getActivity()).bitmapPicker.get("112"), Bitmap.CompressFormat.JPEG,100));
            p24_fotoRumah.refreshDrawableState();
        }
        if(((NewOrderActivity)getActivity()).bitmapPicker.containsKey("113")){
            p24_fotoKonsumen.setImageBitmap(((NewOrderActivity)getActivity()).bitmapPicker.get("113"));
            orderEntry.setCONSUMENT_FILE(encodeToBase64(((NewOrderActivity)getActivity()).bitmapPicker.get("113"), Bitmap.CompressFormat.JPEG,100));
            p24_fotoRumah.refreshDrawableState();
        }
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        if (TextUtils.isEmpty(txtnote.getText())){
            txtnote.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (p24_fotoRumah.getDrawable() == null){
            lblFotoRumah.setError(ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (p24_fotoDokumen.getDrawable() == null){
            lblFotoDocument.setError(ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (p24_fotoKonsumen.getDrawable() == null){
            lblFotoKonsumen.setError(ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewWatcher implements TextWatcher {

        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String text = s.toString();
            switch (view.getId()){
                case R.id.txt_gps_coordinate :
                    orderEntry.setGPS(text);
                    break;
                case R.id.txtnote :
                    orderEntry.setDESCRIPTION(text);
                    break;
                case R.id.txt_gps_description :
                    orderEntry.setGPS_DESCRIPTION(text);
                    break;
            }
        }
    }
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
}
