package com.indocyber.columbia.Activity.NewOrder;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.indocyber.columbia.Abstract.UserLoginActivity;
import com.indocyber.columbia.Adapter.ReasonAdapter;
import com.indocyber.columbia.App;
import com.indocyber.columbia.Contract.DialogOnClick;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.DialogFragment.DialogFragmentConfirmation;
import com.indocyber.columbia.Library.Resizer;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.StatusKunjungan;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NewOrderActivity extends UserLoginActivity implements
        StepperLayout.StepperListener, NewOrderInterface {

    private static final String TAG = "NewOrderActivity";
    public static final String ErrorMessage = "Required Fill";
    public static final String ErrorMessageLong = "Please Fill Required Field";
    public int RequestRumah=111,RequestDokumen=112,RequestKonsumen=113;
    public OrderEntry orderEntry = new OrderEntry();
    public HashMap<String,Bitmap> bitmapPicker;
    public HashMap<String,String> imagePath;
    private StepperLayout stepperLayout;
    private TextView title;

    private List<Step> fragments = new ArrayList<>();
    private List<String> titles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);
        //init component
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNewOrder);
        stepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        title = (TextView) findViewById(R.id.title_NewOrder);
        ImageView imgHome = (ImageView) findViewById(R.id.imageViewHome);
        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stepperLayout.setCurrentStepPosition(0);
            }
        });
        setSupportActionBar(toolbar);
        if (fragments.isEmpty()){
            fragments.add(NewOrder1.newInstance());
            fragments.add(NewOrder2.newInstance());
            fragments.add(NewOrder3.newInstance());
            fragments.add(NewOrder4.newInstance());
            fragments.add(NewOrder5.newInstance());
            fragments.add(NewOrder6.newInstance());
            fragments.add(NewOrder7.newInstance());
            fragments.add(NewOrder8.newInstance());
            fragments.add(NewOrder9.newInstance());
            fragments.add(NewOrder10.newInstance());
            fragments.add(NewOrder11.newInstance());
            fragments.add(NewOrder12.newInstance());
            fragments.add(NewOrder13.newInstance());
            fragments.add(NewOrder14.newInstance());
            fragments.add(NewOrder15.newInstance());
            fragments.add(NewOrder16.newInstance());
            fragments.add(NewOrder17.newInstance());
            fragments.add(NewOrder18.newInstance());
            fragments.add(NewOrder19.newInstance());
            fragments.add(NewOrder20.newInstance());
            fragments.add(NewOrder21.newInstance());
            fragments.add(NewOrder22.newInstance());
            fragments.add(NewOrder23.newInstance());
            fragments.add(NewOrder24.newInstance());
        }
        titles.add("Data Pribadi I (1/24)");
        titles.add("Data Pribadi II (2/24)");
        titles.add("Data Pribadi III (3/24)");
        titles.add("Data Pribadi IV (4/24)");
        titles.add("Data Pribadi V (5/24)");
        titles.add("Data Pribadi VI (6/24)");
        titles.add("Data Pasangan I (7/24)");
        titles.add("Data Keuangan I (8/24)");
        titles.add("Kerabat tidak Serumah (9/24)");
        titles.add("Data Anak(10/24)");
        titles.add("General (11/24)");
        titles.add("Dealer (12/24)");
        titles.add("Data Barang & Pinjaman (13/24)");
        titles.add("Penghasilan Suami/Istri Konsumen (14/24)");
        titles.add("Penghasilan Tidak tetap (15/24)");
        titles.add("Pemilikan Aset Lain (16/24)");
        titles.add("Keuangan dan Perbankan I (17/24)");
        titles.add("Keuangan dan Perbankan II (18/24)");
        titles.add("Summary Penghasilan / Pengeluaran (19/24)");
        titles.add("Kekayaan / Harta Konsumen (20/24)");
        titles.add("Kepribadiaan Konsumen(21/24)");
        titles.add("Lain - Lain(22/24)");
        titles.add("Kondisi Konsumen (23/24)");
        titles.add("Memo(24/24)");

        setStepperAdapter();
        /*init order entry*/
        RealmQuery<OrderEntry> query=realm.where(OrderEntry.class);
        query.equalTo("ORDER_ID",getIntent().getStringExtra("ORDER_ID"));
        RealmResults<OrderEntry> results=query.findAll();
        if(results.size()>0){
            orderEntry = results.get(0);
        }else{
            /*finish if not found*/
            finish();
        }
        //empty bitmap
        bitmapPicker=new HashMap<>();
        imagePath=new HashMap<>();
        if(!realm.isInTransaction()){
            realm.beginTransaction();
        }
        stepperLayout.setListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Resizer resizer=new Resizer();
        switch (requestCode){
            case 111 :
                if(data!=null) {
                    Bitmap bitmapR = (Bitmap) data.getExtras().get("data");
                    bitmapPicker.put("111", bitmapR);
                }
                break;
            case 112 :
                if(data!=null) {
                    Bitmap bitmapD = (Bitmap) data.getExtras().get("data");
                    bitmapPicker.put("112", bitmapD);
                }
                break;
            case 113 :
                if(data!=null) {
                    Bitmap bitmapK = (Bitmap) data.getExtras().get("data");
                    bitmapPicker.put("113", bitmapK);
                }
                break;
        }
    }

    @Override
    public void onCompleted(View completeButton) {
        Log.d(TAG, "onCompleted: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            finish();
        }else {
            saveDialog();
        }
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    @Override
    public OrderEntry getEntry() {
        return this.orderEntry;
    }

    @Override
    public Realm getRealm() {
        return realm;
    }

    private class MyStepperAdapter extends AbstractFragmentStepAdapter {

        MyStepperAdapter(FragmentManager fm, Context context) {
            super(fm, context);
        }

        @Override
        public Step createStep(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            title.setText(titles.get(position));
            StepViewModel.Builder builder = new StepViewModel.Builder(context)
                    .setTitle("");
            return builder.create();
        }
    }

    private void setStepperAdapter() {
        stepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), NewOrderActivity.this));
    }

    public void setMonthAdapter (FragmentActivity context, Spinner spinner){
        String[] month={"0","1","2","3","4","5","6","7","8","9","10","11"};
        ArrayAdapter<String> adapter=new ArrayAdapter<>(context,android.R.layout.simple_spinner_item,month);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            finish();
        }else{
            cancelDialog();
        }
    }
    /*save dialog*/
    public void saveDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Would you like to save and Send survey data ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                orderEntry.setMOBILE_STATUS("PENDING");
                orderEntry.setSTATUS_KUNJUNGAN("5");
                realm.commitTransaction();
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    /*show cancel dialog*/
    public void cancelDialog(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_cancel);
        dialog.setTitle("Cancel Survey");
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //set action button
        ImageView closeBtn= (ImageView) dialog.findViewById(R.id.imageViewCloseDialog);
        Button buttonNo= (Button) dialog.findViewById(R.id.buttonCancelNo);
        Button buttonCancelReason= (Button) dialog.findViewById(R.id.buttonCancelSurvey);
        Button buttonCancelDraft= (Button) dialog.findViewById(R.id.buttonCancelDraft);
        buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.cancelTransaction();
                dialog.dismiss();
                finish();
            }
        });
        buttonCancelReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show reason dialog
                reasonDialog();
            }
        });
        buttonCancelDraft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // save to draft
                orderEntry.setMOBILE_STATUS("DRAFT");
                realm.commitTransaction();
                finish();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if(orderEntry.getMOBILE_STATUS().equals("DRAFT")){
            buttonCancelDraft.setVisibility(View.GONE);
        }
        dialog.show();
    }
    /*show cancel Survey Reason dialog*/
    public void reasonDialog(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_reason);
        dialog.setTitle("Cancel Reason");
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        // init spinner
        final Spinner spinnerReason= (Spinner) dialog.findViewById(R.id.spinnerReason);
        RealmQuery<StatusKunjungan> statusKunjunganRealmQuery=realm.where(StatusKunjungan.class);
        RealmResults<StatusKunjungan> statusKunjunganRealmResults=statusKunjunganRealmQuery.findAll();
        ReasonAdapter provinceAdapter=new ReasonAdapter(this,R.layout.spinner_item,statusKunjunganRealmResults);
        spinnerReason.setAdapter(provinceAdapter);
        //set action button
        Button buttonNoCancel= (Button) dialog.findViewById(R.id.buttonReasonNo);
        Button buttonYesCancel= (Button) dialog.findViewById(R.id.buttonReasonYes);
        buttonNoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        buttonYesCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderEntry.setMOBILE_STATUS("PENDING");
                orderEntry.setSTATUS_KUNJUNGAN(String.valueOf(spinnerReason.getSelectedItemId()));
                realm.commitTransaction();
                finish();
            }
        });
        dialog.show();
    }
    /*request image*/
    public void requestImage(int code){
        Intent photo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(photo,code);
    }
}
