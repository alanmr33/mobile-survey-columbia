package com.indocyber.columbia.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.indocyber.columbia.Abstract.UserLoginActivity;
import com.indocyber.columbia.Activity.NewOrder.NewOrderList;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class Main extends UserLoginActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,View.OnClickListener {
    private CoordinatorLayout coordinatorLayout;
    private TextView txtLoginAs;
    private RelativeLayout menuNewOrder;
    private LinearLayout menuSavedOrder;
    private LinearLayout menuSentOrder;
    private LinearLayout menuPendingOrder;
    private LinearLayout menuAccount;
    private LinearLayout menuLogout;
    private Boolean requestTrack=true;
    //location code is here
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public double latitude;
    public double longitude;
    private Activity activity;
    /*
    location configuration
     */
    private static final int UPDATE_INTERVAL = 10000; // 10 sec
    private static final int FATEST_INTERVAL = 5000; // 5 sec
    private static final int DISPLACEMENT = 10; // 10 meters
    /*
    for counting results for notif bubble text
     */
    private RealmResults<OrderEntry> results;
    private int resultSum;
    private TextView textNotifBubble;
    private ImageView imgNotifBubble;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        init component
         */
        txtLoginAs= (TextView) findViewById(R.id.textViewLoginAs);
        menuAccount= (LinearLayout) findViewById(R.id.menuAccount);
        menuNewOrder= (RelativeLayout) findViewById(R.id.menuNewOrder);
        menuSavedOrder= (LinearLayout) findViewById(R.id.menuSavedOrder);
        menuPendingOrder= (LinearLayout) findViewById(R.id.menuPendingOrder);
        menuSentOrder= (LinearLayout) findViewById(R.id.menuSentOrder);
        menuLogout= (LinearLayout) findViewById(R.id.menuLogout);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        /*
        set listener click here
         */
        menuAccount.setOnClickListener(this);
        menuNewOrder.setOnClickListener(this);
        menuPendingOrder.setOnClickListener(this);
        menuSavedOrder.setOnClickListener(this);
        menuSentOrder.setOnClickListener(this);
        menuLogout.setOnClickListener(this);
        setTitle();
        /*
        show Network Snack Bar If Low
         */
        showNetworkStatus();
        /*
        check pin
         */
        if(session.getPIN().equals("")){
            Intent setupPIN=new Intent(this,SetupPin.class);
            startActivity(setupPIN);
        }else{
            Intent checkPIN=new Intent(this,CheckPin.class);
            startActivity(checkPIN);
        }
        //location
        activity=this;
        if (checkPlayServices()) {
            buildGoogleApiClient();
            createLocationRequest();
            checkLocationTurnOn();
        }
        /*
        notification bubble
         */
        RealmQuery<OrderEntry> query=realm.where(OrderEntry.class);
        query.equalTo("MOBILE_STATUS","NEW");
        results=query.findAll();

        notifBubbleChange(results);

        results.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<OrderEntry>>() {
            @Override
            public void onChange(RealmResults<OrderEntry> orderEntries, OrderedCollectionChangeSet changeSet) {
                notifBubbleChange(orderEntries);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
            if(!mGoogleApiClient.isConnected()){
                Log.e("Map API","not connected");
            }else{
                Log.i("Map API","API connected");
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            stopLocationUpdates();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTitle();
         /*
        show Network Snack Bar If Low
         */
        showNetworkStatus();
        //check location
        if(checkPlayServices()){
            if(!mGoogleApiClient.isConnected() && mGoogleApiClient != null){
                mGoogleApiClient.connect();
            }
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        }else{
            finish();
        }
    }
    public void setTitle(){
         /*
        set Login Ass
         */
        try {
            txtLoginAs.setText("You are login as "+session.getUserdata().getString("Fullname"));
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    /*
    all action click here guys
     */
    @Override
    public void onClick(View v) {
        //choose action by element
        switch (v.getId()){
            case R.id.menuAccount :
                /*
                go to user settings
                 */
                Intent userConfig=new Intent(this,UserSettings.class);
                startActivity(userConfig);
                break;
            case R.id.menuLogout :
                /*
                logout User
                 */
                realm.beginTransaction();
                realm.deleteAll();
                realm.commitTransaction();
                session.setLogin(false);
                session.setSynced(false);
                Intent logout=new Intent(this,Splash.class);
                startActivity(logout);
                finish();
                break;
            case R.id.menuNewOrder :
                /*
                go to new order page
                 */
                Intent newOrder=new Intent(this, NewOrderList.class);
                startActivity(newOrder);
                break;
            case R.id.menuPendingOrder :
                /*
                go to pending order page
                 */
                Intent pendingOrder=new Intent(this, PendingOrder.class);
                startActivity(pendingOrder);
                break;
            case R.id.menuSentOrder :
                /*
                go to sent order page
                 */
                Intent sendtOrder=new Intent(this,SentOrder.class);
                startActivity(sendtOrder);
                break;
            case R.id.menuSavedOrder :
                /*
                go to save order page
                 */
                Intent saveOrder=new Intent(this,SavedOrder.class);
                startActivity(saveOrder);
                break;
        }
    }
    /**
     * Create Api Client
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }
    /**
     check location Service
     **/
    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                int locationRequestCode = 1000;
                GoogleApiAvailability.getInstance().getErrorDialog(this, locationRequestCode, resultCode).show();
            } else {
                Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG).show();
                finish();
            }
            Log.i("Location Service","Location Service Is Turned Off");
            return false;
        }
        Log.i("Location Service","Location Service Is Turned On");
        return true;
    }
    public void showNetworkStatus(){
        /*
        if no connection set offline if not online device
         */
        if(!isConnected()){
            session.setConnected(false);
            final Snackbar snackbar = Snackbar.make(coordinatorLayout, "You're now offline. Reconnect ?", Snackbar.LENGTH_LONG)
                    .setAction("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            refreshNetworkStatus();
                            showNetworkStatus();
                        }
                    });
            snackbar.setActionTextColor(Color.GREEN);
            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        }else{
            session.setConnected(true);
        }
    }
    private void checkLocationTurnOn(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        /*
        on setting is off
         */
        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("Location Service","Location Setting is off");
                        AlertDialog.Builder msg=new AlertDialog.Builder(activity);
                        msg.setCancelable(false);
                        msg.setMessage("Please , turn on location to user survey app !");
                        msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent settings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(settings);
                            }
                        });
                        msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog dialog=msg.create();
                        dialog.show();
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Toast.makeText(getBaseContext(),"No Location Setting Found",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }
    private void updateLocationData() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            session.setLocation(latitude+","+longitude);
//            Log.i("Location Service","Received location "+latitude+","+longitude);
            logLocation();
        }else{
            Log.i("Location Service","Received location nulled");
            if(mGoogleApiClient.isConnected()){
                startLocationUpdates();
            }else{
                mGoogleApiClient.connect();
                startLocationUpdates();
            }
        }
    }
    protected void createLocationRequest() {
        Log.i("Location Service","Create Location Request");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }
    //receive location
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        Log.i("Location Service","Starting Update Location");
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        Log.i("Location Service","Stoping Update Location");
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }
    //Requesting permission location
    private void requestPermissions(){
        Boolean locationCoarse=ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        Boolean locationFine=ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(locationCoarse || locationFine){
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, 110);
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        updateLocationData();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // connect on susspend
        mGoogleApiClient.connect();
        startLocationUpdates();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Location Request", "Connection failed: Error Code = "+ connectionResult.getErrorCode());
        Log.e("Location Request", "Connection failed: Error Message = "+ connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            updateLocationData();
        }
    }

    public void notifBubbleChange(RealmResults<OrderEntry> results){
        textNotifBubble = (TextView)findViewById(R.id.text_notif_bubble);
        imgNotifBubble = (ImageView) findViewById(R.id.img_notif_bubble);
        resultSum=results.size();
        if (resultSum==0){
            textNotifBubble.setVisibility(View.INVISIBLE);
            imgNotifBubble.setVisibility(View.INVISIBLE);
        }
        else if (resultSum>99) {
            textNotifBubble.setText("99+");
            textNotifBubble.setVisibility(View.VISIBLE);
            imgNotifBubble.setVisibility(View.VISIBLE);
        }
        else {
            textNotifBubble.setText(String.valueOf(results.size()));
            textNotifBubble.setVisibility(View.VISIBLE);
            imgNotifBubble.setVisibility(View.VISIBLE);
        }
    }
    public void logLocation(){
        if(requestTrack) {
            Long lastLog = session.getLast_Log();
            if (lastLog==0) {
                Date date=new Date();
                session.setLastLog(date.getTime());
            } else {
                Date lastLogTime = new Date(lastLog);
                Date currentTime =new Date();
                Calendar c = Calendar.getInstance();
                Calendar c2 = Calendar.getInstance();
                c.setTime(currentTime);
                c2.setTime(lastLogTime);
                long diferent = (c.getTimeInMillis()-c2.getTimeInMillis())/1000;
//                Log.i("DIFFERENT", "TIME : " + diferent + " L : " + lastLogTime.getTime() + " & N : " + currentTime.getTime());
                if (diferent >= 600) {
                    session.setLastLog(currentTime.getTime());
                    requestTrack = false;
                    RequestParams params = new RequestParams();
                    params.put("token", session.getToken());
                    params.put("loc", session.getLocation());
                    httpClient.post(session.getUrl() + config.LOG_ENDPOINT, params, new JsonHttpResponseHandler() {
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.i("TRACK", "SUCCESS LOCATION TRACK");
                            requestTrack = true;
                        }
                    });
                }
            }
        }
    }
}
