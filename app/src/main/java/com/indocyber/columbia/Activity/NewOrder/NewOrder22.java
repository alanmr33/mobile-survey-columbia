package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Contract.DateContract;
import com.indocyber.columbia.DialogFragment.DialogFragmentDatePicker;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.CalenderWatcher;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

public class NewOrder22 extends AppFragment implements DateContract, Step {

    private static final String TAG = "NewOrder22";
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private EditText txttempatsurvey1;
    private AppCompatEditText dptglsurvey;
    private ImageView img_Calender;
    private Spinner txtbertemudengan1,chktmptsurvey;

    public static NewOrder22 newInstance() {
        return new NewOrder22();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order22, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // init component
        dptglsurvey= (AppCompatEditText) v.findViewById(R.id.dptglsurvey);
        img_Calender = (ImageView) v.findViewById(R.id.img_calender);
        txttempatsurvey1= (EditText) v.findViewById(R.id.txttempatsurvey1);
        txtbertemudengan1= (Spinner) v.findViewById(R.id.txtbertemudengan1);
        chktmptsurvey= (Spinner) v.findViewById(R.id.chktmptsurvey);
        //end init
        //init value list
        try {
            setAdapterCombo("27062",txtbertemudengan1,true,"");
            setAdapterCombo("27060",chktmptsurvey,true,"");
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }

        txttempatsurvey1.addTextChangedListener(watcher);
        dptglsurvey.addTextChangedListener(watcherTglSurvey);
        txtbertemudengan1.setOnItemSelectedListener(new NewSpinner(txtbertemudengan1));
        chktmptsurvey.setOnItemSelectedListener(new NewSpinner(chktmptsurvey));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txttempatsurvey1.setEnabled(false);
            txtbertemudengan1.setEnabled(false);
            chktmptsurvey.setEnabled(false);
            dptglsurvey.setEnabled(false);
        }else{
            img_Calender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDialog();
                }
            });
        }
        return v;
    }

    private CalenderWatcher watcherTglSurvey = new CalenderWatcher() {
        @Override
        public void typedValue(String date) {
            final String dates = date;
            orderEntry.setSURVEY_DATE(dates);
        }

        @Override
        public AppCompatEditText getView() {
            return dptglsurvey;
        }
    };

    private void callDialog (){
        DialogFragmentDatePicker dialogFragment = new DialogFragmentDatePicker();
        dialogFragment.setTargetFragment(this, 0);
        dialogFragment.show(getActivity().getSupportFragmentManager(), TAG);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        txttempatsurvey1.setText(orderEntry.getSURVEY_PLACE());
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        if (TextUtils.isEmpty(dptglsurvey.getText())){
            dptglsurvey.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSetData(String date) {
        final String dates = date;
        Log.d(TAG, "onSetData: "+dates);
        orderEntry.setSURVEY_DATE(dates);
        dptglsurvey.setText(date);
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String text = s.toString();
            orderEntry.setSTATUS_KUNJUNGAN(text);
        }
    };

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.chktmptsurvey :
                    orderEntry.setSURVEY_PLACE(String.valueOf(chktmptsurvey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+chktmptsurvey.getSelectedItemId());
                    break;
                case R.id.txtbertemudengan1 :
                    orderEntry.setMEET_WITH(String.valueOf(txtbertemudengan1.getSelectedItemId()));
                    Log.d(TAG, "execute: "+txtbertemudengan1.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
