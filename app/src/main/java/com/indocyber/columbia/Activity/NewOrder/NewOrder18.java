package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;


public class NewOrder18 extends AppFragment implements Step {

    private static final String TAG = "NewOrder18";
    private Spinner cb_own_credit_card, cb_credit_card_type, cb_bank_credit_type;
    private EditText txtpenerbitkrtkredit, txtlimitpengkartukredit, txtavgpengkartukredit;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder18 newInstance() {
        return new NewOrder18();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order18, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        cb_own_credit_card = (Spinner) view.findViewById(R.id.cb_own_credit_card);
        cb_credit_card_type = (Spinner) view.findViewById(R.id.cb_credit_card_type);
        cb_bank_credit_type = (Spinner) view.findViewById(R.id.cb_bank_credit_type);
        txtpenerbitkrtkredit = (EditText) view.findViewById(R.id.txtpenerbitkrtkredit);
        txtlimitpengkartukredit = (EditText) view.findViewById(R.id.txtlimitpengkartukredit);
        txtavgpengkartukredit = (EditText) view.findViewById(R.id.txtavgpengkartukredit);

        try {
            setAdapterComboStringAll("24591",cb_own_credit_card,true,"");
            setAdapterCombo("24593",cb_credit_card_type,true,"");
            setAdapterComboStringAll("24594",cb_bank_credit_type,true,"");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        txtavgpengkartukredit.addTextChangedListener(new NewWatcher(txtavgpengkartukredit));
        txtlimitpengkartukredit.addTextChangedListener(new NewWatcher(txtlimitpengkartukredit));
        txtpenerbitkrtkredit.addTextChangedListener(new NewWatcher(txtpenerbitkrtkredit));

        cb_own_credit_card.setOnItemSelectedListener(new NewSpinner(cb_own_credit_card));
        cb_credit_card_type.setOnItemSelectedListener(new NewSpinner(cb_credit_card_type));
        cb_bank_credit_type.setOnItemSelectedListener(new NewSpinner(cb_bank_credit_type));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            cb_own_credit_card.setEnabled(false);
            cb_credit_card_type.setEnabled(false);
            cb_bank_credit_type.setEnabled(false);
            txtpenerbitkrtkredit.setEnabled(false);
            txtlimitpengkartukredit.setEnabled(false);
            txtavgpengkartukredit.setEnabled(false);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        txtpenerbitkrtkredit.setText(orderEntry.getBANK_OF_CREDIT_CARD());
        txtlimitpengkartukredit.setText(orderEntry.getCARD_LIMIT());
        txtavgpengkartukredit.setText(orderEntry.getCARD_AVERAGE_USAGE());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{

        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cb_own_credit_card :
                    orderEntry.setHAVE_CREDIT_CARD(String.valueOf(cb_own_credit_card.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_own_credit_card.getSelectedItemId());
                    break;
                case R.id.cb_credit_card_type :
                    orderEntry.setCARD_SERVICE(String.valueOf(cb_credit_card_type.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_credit_card_type.getSelectedItemId());
                    break;
                case R.id.cb_bank_credit_type :
                    orderEntry.setCARD_TYPE(String.valueOf(cb_bank_credit_type.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_bank_credit_type.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
    private class NewWatcher implements TextWatcher {
        private long result = 0;
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            final String text = s.toString();
            if(view.getId() != R.id.txtpenerbitkrtkredit)
                result = Formating.getCleanResultLong(s.toString());
            Log.d(TAG, "onTextChanged: "+result);
            Log.d(TAG, "onTextChanged: "+text);
            switch (view.getId()){
                case R.id.txtpenerbitkrtkredit :
                    orderEntry.setBANK_OF_CREDIT_CARD(text);
                    break;
                case R.id.txtlimitpengkartukredit :
                    orderEntry.setCARD_LIMIT(String.valueOf(result));
                    break;
                case R.id.txtavgpengkartukredit :
                    orderEntry.setCARD_AVERAGE_USAGE(String.valueOf(result));
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.txtlimitpengkartukredit :
                    txtlimitpengkartukredit.removeTextChangedListener(this);
                    if (!TextUtils.isEmpty(s.toString())){
                        txtlimitpengkartukredit.setText(Formating.currency(result));
                        txtlimitpengkartukredit.setSelection(txtlimitpengkartukredit.getText().length());
                    }
                    txtlimitpengkartukredit.addTextChangedListener(this);
                    break;
                case R.id.txtavgpengkartukredit :
                    txtavgpengkartukredit.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txtavgpengkartukredit.setText(Formating.currency(result));
                        txtavgpengkartukredit.setSelection(txtavgpengkartukredit.getText().length());
                    }
                    txtavgpengkartukredit.addTextChangedListener(this);
                    break;
            }
        }
    }

}
