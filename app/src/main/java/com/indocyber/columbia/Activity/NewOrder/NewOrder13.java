package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Contract.DateContract;
import com.indocyber.columbia.DialogFragment.DialogFragmentDatePicker;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.CalenderWatcher;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

public class NewOrder13 extends AppFragment implements DateContract, Step {

    private static final String TAG = "NewOrder13";
    private EditText txtqty, txtmerkid, txtmodelid, txttypeid, txtotr, txtdpppercent,
            txtdpnominal, txttenor, txtangsuranperbulan;
    private Spinner lstinstallmenttype, lstloantype;
    private  ImageView img_calender;
    private AppCompatEditText dptglorder;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder13 newInstance() {
        return new NewOrder13();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order13, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init
        img_calender = (ImageView) view.findViewById(R.id.img_calender);
        txtqty = (EditText) view.findViewById(R.id.p13_jumlah);
        txtmerkid = (EditText) view.findViewById(R.id.p13_merek);
        txtmodelid = (EditText) view.findViewById(R.id.p13_model);
        txttypeid = (EditText) view.findViewById(R.id.p13_tipe);
        txtotr = (EditText) view.findViewById(R.id.p13_harga_tunai);
        txtdpnominal = (EditText) view.findViewById(R.id.p13_uangMuka);
        txttenor = (EditText) view.findViewById(R.id.p13_tenor);
        txtangsuranperbulan = (EditText) view.findViewById(R.id.p13_angsuran);
        lstinstallmenttype = (Spinner) view.findViewById(R.id.p13_jenisAngsuran);
        lstloantype = (Spinner) view.findViewById(R.id.p13_jenisPinjaman);
        dptglorder = (AppCompatEditText) view.findViewById(R.id.p13_tanggalOrder);


        try {
            setAdapterCombo("24530",lstinstallmenttype,true,"");
            setAdapterCombo("24531",lstloantype,true,"");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dptglorder.addTextChangedListener(calenderWatcher);
        lstinstallmenttype.setOnItemSelectedListener(new NewSpinner(lstinstallmenttype));
        lstloantype.setOnItemSelectedListener(new NewSpinner(lstloantype));

        txtotr.addTextChangedListener(new NewWatcher(txtotr));
        txtdpnominal.addTextChangedListener(new NewWatcher(txtdpnominal));
        txtangsuranperbulan.addTextChangedListener(new NewWatcher(txtangsuranperbulan));
        txtqty.addTextChangedListener(new NewWatcher(txtqty));
        txtmerkid.addTextChangedListener(new NewWatcher(txtmerkid));
        txtmodelid.addTextChangedListener(new NewWatcher(txtmodelid));
        txttypeid.addTextChangedListener(new NewWatcher(txttypeid));
        dptglorder.addTextChangedListener(new NewWatcher(dptglorder));
        txttenor.addTextChangedListener(new NewWatcher(txttenor));

        return  view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            lstinstallmenttype.setEnabled(false);
            lstloantype.setEnabled(false);
            txtotr.setEnabled(false);
            txtdpnominal.setEnabled(false);
            txtangsuranperbulan.setEnabled(false);
            txtqty.setEnabled(false);
            txtmerkid.setEnabled(false);
            txtmodelid.setEnabled(false);
            txttypeid.setEnabled(false);
            dptglorder.setEnabled(false);
            txttenor.setEnabled(false);
        }else{
            img_calender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDialog();
                }
            });

        }
        txtotr.setText(orderEntry.getCASH_PRICE());
        txtdpnominal.setText(orderEntry.getDOWNPAYMENT_AMT());
        txtangsuranperbulan.setText(orderEntry.getINSTALLMENT_AMT());
        txtqty.setText(orderEntry.getJUMLAH());
        txtmerkid.setText(orderEntry.getBRAND());
        txtmodelid.setText(orderEntry.getMODEL());
        txttypeid.setText(orderEntry.getTYPE());
        dptglorder.setText(orderEntry.getORDER_DATE());
        txttenor.setText(orderEntry.getTENOR());
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        if (TextUtils.isEmpty(txtqty.getText())){
            txtqty.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        if (TextUtils.isEmpty(txtangsuranperbulan.getText())){
            txtangsuranperbulan.setError(NewOrderActivity.ErrorMessage);
            errorMessage = NewOrderActivity.ErrorMessageLong;
        }
        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSetData(final String date) {
        orderEntry.setORDER_DATE(date);
        dptglorder.setText(date);
    }

    private CalenderWatcher calenderWatcher = new CalenderWatcher() {
        @Override
        public void typedValue(final String date) {
            orderEntry.setORDER_DATE(date);
        }

        @Override
        public AppCompatEditText getView() {
            return dptglorder;
        }
    };

    private void callDialog (){
        DialogFragmentDatePicker dialogFragment = new DialogFragmentDatePicker();
        dialogFragment.setTargetFragment(this, 0);
        dialogFragment.show(getActivity().getSupportFragmentManager(), TAG);
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;
        private NewSpinner (View view) {
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent,final View view, int position, long id) {
            switch (v.getId()){
                case R.id.p13_jenisAngsuran :
                    orderEntry.setINSTALLMENT_TYPE(String.valueOf(lstinstallmenttype.getSelectedItemId()));
                    Log.d(TAG, "execute: "+lstinstallmenttype.getSelectedItemId());
                    break;
                case R.id.p13_jenisPinjaman :
                    orderEntry.setRENT_PAYMENT_TYPE(String.valueOf(lstloantype.getSelectedItemId()));
                    Log.d(TAG, "execute: "+lstloantype.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher {
        private long result = 0;
        private String value = "";
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if ((view.getId() == R.id.p13_harga_tunai) ||
                    (view.getId() == R.id.p13_uangMuka) ||
                    (view.getId() == R.id.p13_angsuran))
            {
                result = Formating.getCleanResultLong(s.toString());
            } else {
                value = s.toString();
            }
            Log.d(TAG, "onTextChanged: "+value);
            switch (view.getId()){
                case R.id.p13_harga_tunai :
                    orderEntry.setCASH_PRICE(String.valueOf(result));
                    break;
                case R.id.p13_uangMuka :
                    orderEntry.setDOWNPAYMENT_AMT(String.valueOf(result));
                    break;
                case R.id.p13_angsuran :
                    orderEntry.setINSTALLMENT_AMT(String.valueOf(result));
                    break;
                case R.id.p13_jumlah :
                    orderEntry.setJUMLAH(value);
                    break;
                case R.id.p13_merek :
                    orderEntry.setBRAND(value);
                    break;
                case R.id.p13_model :
                    orderEntry.setMODEL(value);
                    break;
                case R.id.p13_tipe :
                    orderEntry.setTYPE(value);
                    break;
                case R.id.p13_tanggalOrder :
                    orderEntry.setORDER_DATE(value);
                    break;
                case R.id.p13_tenor :
                    orderEntry.setTENOR(value);
                    break;

            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.p13_harga_tunai :
                    txtotr.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txtotr.setText(Formating.currency(result));
                        txtotr.setSelection(txtotr.getText().length());
                    }
                    txtotr.addTextChangedListener(this);
                    break;
                case R.id.p13_uangMuka :
                    txtdpnominal.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txtdpnominal.setText(Formating.currency(result));
                        txtdpnominal.setSelection(txtdpnominal.getText().length());
                    }
                    txtdpnominal.addTextChangedListener(this);
                    break;
                case R.id.p13_angsuran :
                    txtangsuranperbulan.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txtangsuranperbulan.setText(Formating.currency(result));
                        txtangsuranperbulan.setSelection(txtangsuranperbulan.getText().length());
                    }
                    txtangsuranperbulan.addTextChangedListener(this);
                    break;

            }
        }
    }
}
