package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class NewOrder16 extends AppFragment implements Step {

    private static final String TAG = "NewOrder16";
    private EditText txtketassetlainnya, txthargapasarasset, txtketasset1;
    private CheckBox chkSecondHouse, chkCar, chkOtherAsset;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder16 newInstance() {
        return new NewOrder16();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order16, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        txtketassetlainnya = (EditText) view.findViewById(R.id.txtketassetlainnya);
        txthargapasarasset = (EditText) view.findViewById(R.id.txthargapasarasset);
        txtketasset1 = (EditText) view.findViewById(R.id.txtketasset1);

        chkSecondHouse = (CheckBox) view.findViewById(R.id.p16_rumahKedua);
        chkCar = (CheckBox) view.findViewById(R.id.p16_mobil);
        chkOtherAsset = (CheckBox) view.findViewById(R.id.p16_lainna);

        txthargapasarasset.addTextChangedListener(new NewWatcher(txthargapasarasset));
        txtketasset1.addTextChangedListener(new NewWatcher(txtketasset1));
        txtketassetlainnya.addTextChangedListener(new NewWatcher(txtketassetlainnya));

        chkSecondHouse.setOnCheckedChangeListener(new NewCheckBox(chkSecondHouse));
        chkCar.setOnCheckedChangeListener(new NewCheckBox(chkCar));
        chkOtherAsset.setOnCheckedChangeListener(new NewCheckBox(chkOtherAsset));
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtketassetlainnya.setEnabled(false);
            txthargapasarasset.setEnabled(false);
            txtketasset1.setEnabled(false);
            chkSecondHouse.setEnabled(false);
            chkCar.setEnabled(false);
            chkOtherAsset.setEnabled(false);
        }
        chkSecondHouse.setChecked(orderEntry.getSECOND_HOUSE() == "y");
        chkCar.setChecked(orderEntry.getCAR() == "y");
        chkOtherAsset.setChecked(orderEntry.getOTHER_ASSET() == "y");

        txtketassetlainnya.setText(orderEntry.getASSET_DESCRIPTION_OTHER());
        txthargapasarasset.setText(orderEntry.getASSET_PRICE());
        txtketasset1.setText(orderEntry.getASSET_DESCRIPTION());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewWatcher implements TextWatcher {
        private View view;
        private long value = 0;

        private NewWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(view.getId() == R.id.txthargapasarasset)
                value = Formating.getCleanResultLong(s.toString());
            Log.d(TAG, "onTextChanged: "+value);
        }

        @Override
        public void afterTextChanged(Editable s) {
            final String text = s.toString();
            Log.d(TAG, "afterTextChanged: "+text);
            if (view.getId() == R.id.txthargapasarasset) {
                txthargapasarasset.removeTextChangedListener(this);
                if (!TextUtils.isEmpty(s.toString())) {
                    txthargapasarasset.setText(Formating.currency(value));
                    txthargapasarasset.setSelection(txthargapasarasset.getText().length());
                }
                txthargapasarasset.addTextChangedListener(this);
            }
            switch (view.getId()){
                case R.id.txtketassetlainnya :
                    orderEntry.setASSET_DESCRIPTION_OTHER(text);
                    break;
                case R.id.txthargapasarasset :
                    orderEntry.setASSET_PRICE(String.valueOf(value));
                    break;
                case R.id.txtketasset1 :
                    orderEntry.setASSET_DESCRIPTION(text);
                    break;
            }
        }
    }

    private class NewCheckBox implements CompoundButton.OnCheckedChangeListener {

        private View view;
        private boolean checked = false;

        private NewCheckBox (View view){
            this.view = view;
            this.checked=((CheckBox) view).isChecked();
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (view.getId()){
                case R.id.p16_rumahKedua :
                    orderEntry.setSECOND_HOUSE(checked ? "y" : "n");
                    break;
                case R.id.p16_mobil :
                    orderEntry.setCAR(checked ? "y" : "n");
                    break;
                case R.id.p16_lainna :
                    orderEntry.setOTHER_ASSET(checked ? "y" : "n");
                    break;
            }
        }
    }

}
