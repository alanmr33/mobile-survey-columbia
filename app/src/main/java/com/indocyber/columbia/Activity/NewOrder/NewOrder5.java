package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.OccupationAdapter;
import com.indocyber.columbia.Adapter.ProvinceAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.Occupation;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.Province;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NewOrder5 extends AppFragment implements Step{

    private static final String TAG = "NewOrder5";
    private EditText txtconameoccupation,txtcoyear,txtcustjobaddr,txtcort,txtcorw,
            txtcustjobkab,txtcustjobzip,txtcophoneareacode,txtcophonenumber;
    private Spinner cbjobidoccupationdata,txtcousaha,cbcustjobprov;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;

    public static NewOrder5 newInstance() {
        return new NewOrder5();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order5, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // init
        txtconameoccupation= (EditText) v.findViewById(R.id.txtconameoccupation);
        txtcustjobzip= (EditText) v.findViewById(R.id.txtcustjobzip);
        txtcustjobaddr= (EditText) v.findViewById(R.id.txtcustjobaddr);
        txtcoyear= (EditText) v.findViewById(R.id.txtcoyear);
        txtcort= (EditText) v.findViewById(R.id.txtcort);
        txtcorw= (EditText) v.findViewById(R.id.txtcorw);
        txtcustjobkab= (EditText) v.findViewById(R.id.txtcustjobkab);
        cbcustjobprov= (Spinner) v.findViewById(R.id.cbcustjobprov);
        txtcophoneareacode= (EditText) v.findViewById(R.id.txtcophoneareacode);
        txtcophonenumber= (EditText) v.findViewById(R.id.txtcophonenumber);
        cbjobidoccupationdata= (Spinner) v.findViewById(R.id.cbjobidoccupationdata);
        txtcousaha= (Spinner) v.findViewById(R.id.txtcousaha);
        //end init

        try {
            setAdapterCombo("25921",txtcousaha,true,"");
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        initJobOccupation(orderEntry.getOCCUPATION());
        initProvince("");

        txtconameoccupation.addTextChangedListener(new NewWatcher(txtconameoccupation));
        txtcoyear.addTextChangedListener(new NewWatcher(txtcoyear));
        txtcustjobaddr.addTextChangedListener(new NewWatcher(txtcustjobaddr));
        txtcort.addTextChangedListener(new NewWatcher(txtcort));
        txtcorw.addTextChangedListener(new NewWatcher(txtcorw));
        txtcustjobkab.addTextChangedListener(new NewWatcher(txtcustjobkab));
        txtcustjobzip.addTextChangedListener(new NewWatcher(txtcustjobzip));
        txtcophoneareacode.addTextChangedListener(new NewWatcher(txtcophoneareacode));
        txtcophonenumber.addTextChangedListener(new NewWatcher(txtcophonenumber));

        cbjobidoccupationdata.setOnItemSelectedListener(new NewSpinner(cbjobidoccupationdata));
        txtcousaha.setOnItemSelectedListener(new NewSpinner(txtcousaha));
        cbcustjobprov.setOnItemSelectedListener(new NewSpinner(cbcustjobprov));

        return v;
    }

    private void initJobOccupation(String value){
        /*setup combo box job title*/
        RealmQuery<Occupation> occupationRealmQuery=realm.where(Occupation.class);
        RealmResults<Occupation> occupationRealmResults=occupationRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<occupationRealmResults.size();i++){
            if(value.equals(occupationRealmResults.get(i).getOCCUPATION_ID())){
                selectedIndex=i;
                break;
            }
        }
        OccupationAdapter occupationAdapter=new OccupationAdapter(getActivity(),R.layout.spinner_item,occupationRealmResults);
        cbjobidoccupationdata.setAdapter(occupationAdapter);
        cbjobidoccupationdata.setSelection(selectedIndex);
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtconameoccupation.setEnabled(false);
            txtcoyear.setEnabled(false);
            txtcustjobaddr.setEnabled(false);
            txtcort.setEnabled(false);
            txtcorw.setEnabled(false);
            txtcustjobkab.setEnabled(false);
            txtcustjobzip.setEnabled(false);
            txtcophoneareacode.setEnabled(false);
            txtcophonenumber.setEnabled(false);
            cbjobidoccupationdata.setEnabled(false);
            txtcousaha.setEnabled(false);
            cbcustjobprov.setEnabled(false);
        }
        txtconameoccupation.setText(orderEntry.getOFFICE_NAME());
        txtcoyear.setText(orderEntry.getBUSSINESS_ESTABLISHED_SINCE());
        txtcustjobaddr.setText(orderEntry.getJOBADDRESS());
        txtcort.setText(orderEntry.getOFFICE_RT());
        txtcorw.setText(orderEntry.getOFFICE_RW());
        txtcustjobkab.setText(orderEntry.getOFFICE_KAB());
        txtcustjobzip.setText(orderEntry.getJOBPOSTALCODE());
        txtcophoneareacode.setText(orderEntry.getOFFICE_TELP_AREACODE());
        txtcophonenumber.setText(orderEntry.getOFFICE_TELP_NO());

    }

    private void initProvince(String value){
        /*
        setup combo box province
         */
        RealmQuery<Province> provinceRealmQuery = realm.where(Province.class);
        RealmResults<Province> provinceRealmResults=provinceRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<provinceRealmResults.size();i++){
            if(value.equals(provinceRealmResults.get(i).getPROVINCE_ID())){
                selectedIndex=i;
                break;
            }
        }
        ProvinceAdapter provinceAdapter=new ProvinceAdapter(getActivity(),R.layout.spinner_item,provinceRealmResults);
        cbcustjobprov.setAdapter(provinceAdapter);
        cbcustjobprov.setSelection(selectedIndex);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtconameoccupation :
                    orderEntry.setOFFICE_NAME(result);
                    break;
                case R.id.txtcoyear :
                    orderEntry.setBUSSINESS_ESTABLISHED_SINCE(result);
                    break;
                case R.id.txtcustjobaddr :
                    orderEntry.setJOBADDRESS(result);
                    break;
                case R.id.txtcort :
                    orderEntry.setOFFICE_RT(result);
                    break;
                case R.id.txtcorw :
                    orderEntry.setOFFICE_RW(result);
                    break;
                case R.id.txtcustjobkab :
                    orderEntry.setOFFICE_KAB(result);
                    break;
                case R.id.txtcustjobzip :
                    orderEntry.setJOBPOSTALCODE(result);
                    break;
                case R.id.txtcophoneareacode :
                    orderEntry.setOFFICE_TELP_AREACODE(result);
                    break;
                case R.id.txtcophonenumber :
                    orderEntry.setOFFICE_TELP_NO(result);
                    break;
            }
        }
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbjobidoccupationdata :
                    orderEntry.setOCCUPATION(String.valueOf(cbjobidoccupationdata.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbjobidoccupationdata.getSelectedItemId());
                    break;
                case R.id.txtcousaha :
                    orderEntry.setBUSSINESS_TYPE(String.valueOf(txtcousaha.getSelectedItemId()));
                    Log.d(TAG, "execute: "+txtcousaha.getSelectedItemId());
                    break;
                case R.id.cbcustjobprov :
                    orderEntry.setOFFICE_PROVINCE(String.valueOf(cbcustjobprov.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbcustjobprov.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
