package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;

public class NewOrder11 extends AppFragment implements Step {

    private static final String TAG = "NewOrder11";
    private EditText cbproductdesc;
    private Spinner rbtflagspa, rbtsalestype, cbitemcondition;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder11 newInstance() {
        return new NewOrder11();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order11, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init Component
        cbproductdesc = (EditText) view.findViewById(R.id.p11_produk);
        rbtflagspa = (Spinner) view.findViewById(R.id.p11_sudahPernahDiambil);
        cbitemcondition = (Spinner) view.findViewById(R.id.p11_kondisiBarang);
        rbtsalestype = (Spinner) view.findViewById(R.id.p11_jenisSales);

        try {
            setAdapterComboStringAll("24345", rbtflagspa, true, "");
            setAdapterComboStringAll("24346", rbtsalestype, true, "");
            setAdapterComboStringAll("24347", cbitemcondition, true, "");
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());

        }

        cbproductdesc.addTextChangedListener(watcher);
        rbtflagspa.setOnItemSelectedListener(new NewSpinner(rbtflagspa));
        cbitemcondition.setOnItemSelectedListener(new NewSpinner(cbitemcondition));
        rbtsalestype.setOnItemSelectedListener(new NewSpinner(rbtsalestype));

        return view;
    }

    private void setupView() {
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            cbproductdesc.setEnabled(false);
            rbtflagspa.setEnabled(false);
            cbitemcondition.setEnabled(false);
            rbtsalestype.setEnabled(false);
        }
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();

    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;
        private NewSpinner (View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.p11_sudahPernahDiambil :
                    orderEntry.setALREADY_TAKE(String.valueOf(rbtflagspa.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbtflagspa.getSelectedItemId());
                    break;
                case R.id.p11_jenisSales :
                    orderEntry.setSALES_TYPE(String.valueOf(rbtsalestype.getSelectedItemId()));
                    Log.d(TAG, "execute: "+rbtsalestype.getSelectedItemId());
                    break;
                case R.id.p11_kondisiBarang :
                    orderEntry.setPRODUCT_CONDITION(String.valueOf(cbitemcondition.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbitemcondition.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            orderEntry.setPRODUCT_CODE(result);
        }
    };
}
