package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.Formating;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class NewOrder19 extends AppFragment implements Step {

    private static final String TAG = "NewOrder19";
    private EditText txt_fixed_income, txt_non_fixed_income, txt_total_income, txt_fixed_expense,
            txt_non_fixed_expense, txt_total_expense;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder19 newInstance() {
        return new NewOrder19();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order19, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        txt_fixed_income = (EditText) view.findViewById(R.id.txt_fixed_income);
        txt_non_fixed_income = (EditText) view.findViewById(R.id.txt_non_fixed_income);
        txt_fixed_expense = (EditText) view.findViewById(R.id.txt_fixed_expense);
        txt_total_income = (EditText) view.findViewById(R.id.txt_total_income);
        txt_non_fixed_expense = (EditText) view.findViewById(R.id.txt_non_fixed_expense);
        txt_total_expense = (EditText) view.findViewById(R.id.txt_total_expense);

        txt_fixed_income.addTextChangedListener(new NewWatcher(txt_fixed_income));
        txt_non_fixed_income.addTextChangedListener(new NewWatcher(txt_non_fixed_income));
        txt_fixed_expense.addTextChangedListener(new NewWatcher(txt_fixed_expense));
        txt_total_income.addTextChangedListener(new NewWatcher(txt_total_income));
        txt_non_fixed_expense.addTextChangedListener(new NewWatcher(txt_non_fixed_expense));
        txt_total_expense.addTextChangedListener(new NewWatcher(txt_total_expense));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txt_fixed_income.setEnabled(false);
            txt_non_fixed_income.setEnabled(false);
            txt_fixed_expense.setEnabled(false);
            txt_non_fixed_expense.setEnabled(false);
        }
        txt_total_expense.setEnabled(false);
        txt_total_income.setEnabled(false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        txt_fixed_income.setText(orderEntry.getPENGHASILAN_TETAP());
        txt_non_fixed_income.setText(orderEntry.getPENGHASILAN_NON_TETAP());
        txt_total_income.setText(orderEntry.getTOTAL_PENDAPATAN());
        txt_fixed_expense.setText(orderEntry.getPENGELUARAN_RUTIN());
        txt_non_fixed_expense.setText(orderEntry.getPENGELUARAN_LAIN());
        txt_total_expense.setText(orderEntry.getPENGELUARAN_TOTAL());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewWatcher implements TextWatcher {
        private long resullt = 0;
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            resullt = Formating.getCleanResultLong(s.toString());
            Log.d(TAG, "onTextChanged: "+resullt);
            switch (view.getId()){
                case R.id.txt_fixed_income:
                    orderEntry.setPENGHASILAN_TETAP(String.valueOf(resullt));
                    countTotalPenghasilan();
                    break;
                case R.id.txt_non_fixed_income:
                    orderEntry.setPENGHASILAN_NON_TETAP(String.valueOf(resullt));
                    countTotalPenghasilan();
                    break;
                case R.id.txt_total_income:
                    orderEntry.setTOTAL_PENDAPATAN(String.valueOf(resullt));
                    break;
                case R.id.txt_fixed_expense:
                    orderEntry.setPENGELUARAN_RUTIN(String.valueOf(resullt));
                    countTotal();
                    break;
                case R.id.txt_non_fixed_expense:
                    orderEntry.setPENGELUARAN_LAIN(String.valueOf(resullt));
                    countTotal();
                    break;
                case R.id.txt_total_expense:
                    orderEntry.setPENGELUARAN_TOTAL(String.valueOf(resullt));
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.txt_fixed_income:
                    txt_fixed_income.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_fixed_income.setText(Formating.currency(resullt));
                        txt_fixed_income.setSelection(txt_fixed_income.getText().length());
                    }
                    txt_fixed_income.addTextChangedListener(this);
                    break;
                case R.id.txt_non_fixed_income:
                    txt_non_fixed_income.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_non_fixed_income.setText(Formating.currency(resullt));
                        txt_non_fixed_income.setSelection(txt_non_fixed_income.getText().length());
                    }
                    txt_non_fixed_income.addTextChangedListener(this);
                    break;
                case R.id.txt_total_income:
                    txt_total_income.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_total_income.setText(Formating.currency(resullt));
                        txt_total_income.setSelection(txt_total_income.getText().length());
                    }
                    txt_total_income.addTextChangedListener(this);
                    break;
                case R.id.txt_fixed_expense:
                    txt_fixed_expense.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_fixed_expense.setText(Formating.currency(resullt));
                        txt_fixed_expense.setSelection(txt_fixed_expense.getText().length());
                    }
                    txt_fixed_expense.addTextChangedListener(this);
                    break;
                case R.id.txt_non_fixed_expense:
                    txt_non_fixed_expense.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_non_fixed_expense.setText(Formating.currency(resullt));
                        txt_non_fixed_expense.setSelection(txt_non_fixed_expense.getText().length());
                    }
                    txt_non_fixed_expense.addTextChangedListener(this);
                    break;
                case R.id.txt_total_expense:
                    txt_total_expense.removeTextChangedListener(this);
                    if(!TextUtils.isEmpty(s.toString())){
                        txt_total_expense.setText(Formating.currency(resullt));
                        txt_total_expense.setSelection(txt_total_expense.getText().length());
                    }
                    txt_total_expense.addTextChangedListener(this);
                    break;
            }
        }
    }
    public void countTotal(){
        long totalFixed=0,totalnonFixed=0;
        if(!txt_fixed_expense.getText().toString().equals("")){
            totalFixed= Long.valueOf(txt_fixed_expense.getText().toString().replace(".",""));
        }
        if(!txt_non_fixed_expense.getText().toString().equals("")){
            totalnonFixed= Long.valueOf(txt_non_fixed_expense.getText().toString().replace(".",""));
        }
        txt_total_expense.setText(String.valueOf(totalFixed+totalnonFixed));
    }
    public void countTotalPenghasilan(){
        long totalFixed=0,totalnonFixed=0;
        if(!txt_fixed_income.getText().toString().equals("")){
            totalFixed= Long.valueOf(txt_fixed_income.getText().toString().replace(".",""));
        }
        if(!txt_non_fixed_income.getText().toString().equals("")){
            totalnonFixed= Long.valueOf(txt_non_fixed_income.getText().toString().replace(".",""));
        }
        txt_total_income.setText(String.valueOf(totalFixed+totalnonFixed));
    }
}
