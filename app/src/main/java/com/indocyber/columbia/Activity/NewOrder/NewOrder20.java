package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;

public class NewOrder20 extends AppFragment implements Step {

    private static final String TAG = "Neworder20";
    private Spinner cb_kendaraan, cb_perabotan, cb_elektronik, cb_jenis_usaha, cb_house_survey,
            cb_duration_survey, cb_building_survey, cb_established_survey, cb_surface_area_survey,
            cb_electrical_power_survey, cb_pam_survey;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder20 newInstance() {
        return new NewOrder20();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order20, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        cb_kendaraan = (Spinner) view.findViewById(R.id.cb_kendaraan);
        cb_perabotan = (Spinner) view.findViewById(R.id.cb_perabotan);
        cb_elektronik = (Spinner) view.findViewById(R.id.cb_elektronik);
        cb_jenis_usaha = (Spinner) view.findViewById(R.id.cb_jenis_usaha);
        cb_house_survey = (Spinner) view.findViewById(R.id.cb_house_survey);
        cb_duration_survey = (Spinner) view.findViewById(R.id.cb_duration_survey);
        cb_building_survey = (Spinner) view.findViewById(R.id.cb_building_survey);
        cb_established_survey = (Spinner) view.findViewById(R.id.cb_established_survey);
        cb_surface_area_survey = (Spinner) view.findViewById(R.id.cb_surface_area_survey);
        cb_electrical_power_survey = (Spinner) view.findViewById(R.id.cb_electrical_power_survey);
        cb_pam_survey = (Spinner) view.findViewById(R.id.cb_pam_survey);

        try {
            setAdapterComboStringAll("25918",cb_kendaraan,true,"");
            setAdapterComboStringAll("25919",cb_perabotan,true,"");
            setAdapterComboStringAll("25920",cb_elektronik,true,"");
            setAdapterComboStringAll("25921",cb_jenis_usaha,true,"");
            setAdapterComboStringAll("24617",cb_house_survey,true,"");
            setAdapterComboStringAll("24618",cb_duration_survey,true,"");
            setAdapterComboStringAll("24619",cb_building_survey,true,"");
            setAdapterComboStringAll("24620",cb_established_survey,true,"");
            setAdapterComboStringAll("24621",cb_surface_area_survey,true,"");
            setAdapterComboStringAll("24622",cb_electrical_power_survey,true,"");
            setAdapterComboStringAll("25889",cb_pam_survey,true,"");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cb_kendaraan.setOnItemSelectedListener(new NewSpinner(cb_kendaraan));
        cb_perabotan.setOnItemSelectedListener(new NewSpinner(cb_perabotan));
        cb_elektronik.setOnItemSelectedListener(new NewSpinner(cb_elektronik));
        cb_jenis_usaha.setOnItemSelectedListener(new NewSpinner(cb_jenis_usaha));
        cb_house_survey.setOnItemSelectedListener(new NewSpinner(cb_house_survey));
        cb_duration_survey.setOnItemSelectedListener(new NewSpinner(cb_duration_survey));
        cb_building_survey.setOnItemSelectedListener(new NewSpinner(cb_building_survey));
        cb_established_survey.setOnItemSelectedListener(new NewSpinner(cb_established_survey));
        cb_surface_area_survey.setOnItemSelectedListener(new NewSpinner(cb_surface_area_survey));
        cb_electrical_power_survey.setOnItemSelectedListener(new NewSpinner(cb_electrical_power_survey));
        cb_pam_survey.setOnItemSelectedListener(new NewSpinner(cb_pam_survey));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            cb_kendaraan.setEnabled(false);
            cb_perabotan.setEnabled(false);
            cb_elektronik.setEnabled(false);
            cb_jenis_usaha.setEnabled(false);
            cb_house_survey.setEnabled(false);
            cb_duration_survey.setEnabled(false);
            cb_building_survey.setEnabled(false);
            cb_established_survey.setEnabled(false);
            cb_surface_area_survey.setEnabled(false);
            cb_electrical_power_survey.setEnabled(false);
            cb_pam_survey.setEnabled(false);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    private void setupView () {
        Log.i(TAG, "setupView: ");

    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner(View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cb_house_survey :
                    orderEntry.setHOUSE(String.valueOf(cb_house_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_house_survey.getSelectedItemId());
                    break;
                case R.id.cb_duration_survey :
                    orderEntry.setOLD_OCCUPY(String.valueOf(cb_duration_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_duration_survey.getSelectedItemId());
                    break;
                case R.id.cb_building_survey :
                    orderEntry.setBUILDING_CONDITION(String.valueOf(cb_building_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_building_survey.getSelectedItemId());
                    break;
                case R.id.cb_established_survey :
                    orderEntry.setBUILDING_AREA(String.valueOf(cb_established_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_established_survey.getSelectedItemId());
                    break;
                case R.id.cb_surface_area_survey :
                    orderEntry.setLAND_AREA(String.valueOf(cb_surface_area_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_surface_area_survey.getSelectedItemId());
                    break;
                case R.id.cb_electrical_power_survey :
                    orderEntry.setELECTRICITY(String.valueOf(cb_electrical_power_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_electrical_power_survey.getSelectedItemId());
                    break;
                case R.id.cb_pam_survey :
                    orderEntry.setWATER(String.valueOf(cb_pam_survey.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_pam_survey.getSelectedItemId());
                    break;
                case R.id.cb_kendaraan :
                    orderEntry.setTRANSPORT(String.valueOf(cb_kendaraan.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_kendaraan.getSelectedItemId());
                    break;
                case R.id.cb_perabotan :
                    orderEntry.setFURNITURE(String.valueOf(cb_perabotan.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_perabotan.getSelectedItemId());
                    break;
                case R.id.cb_elektronik :
                    orderEntry.setELECTRONIC(String.valueOf(cb_elektronik.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_elektronik.getSelectedItemId());
                    break;
                case R.id.cb_jenis_usaha :
                    orderEntry.setBUSSINESS_TYPE_P20(String.valueOf(cb_jenis_usaha.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cb_jenis_usaha.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
