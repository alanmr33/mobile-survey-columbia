package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.ReligionAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Contract.DateContract;
import com.indocyber.columbia.DialogFragment.DialogFragmentDatePicker;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.Religion;
import com.indocyber.columbia.R;
import com.indocyber.columbia.Util.CalenderWatcher;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class NewOrder7 extends AppFragment implements DateContract,Step {
    private EditText txtspousename,txtspousenickname,txtidnumberspouse,txtbirthplacespouse;
    private Spinner cbtitlespouse,cbsexspouse,cbidtypespousedata,cbreligionspouse;
    private AppCompatEditText dpbirthdatespouse;
    private ImageView img_calender;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;

    public static NewOrder7 newInstance() {
        return new NewOrder7();
    }
    private static final String TAG = "NewOrder7";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order7, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        img_calender = (ImageView) v.findViewById(R.id.img_calender);
        txtspousename= (EditText) v.findViewById(R.id.txtspousename);
        txtspousenickname= (EditText) v.findViewById(R.id.txtspousenickname);
        txtidnumberspouse= (EditText) v.findViewById(R.id.txtidnumberspouse);
        txtbirthplacespouse= (EditText) v.findViewById(R.id.txtbirthplacespouse);
        dpbirthdatespouse= (AppCompatEditText) v.findViewById(R.id.dpbirthdatespouse);
        cbsexspouse= (Spinner) v.findViewById(R.id.cbsexspouse);
        cbtitlespouse= (Spinner) v.findViewById(R.id.cbtitlespouse);
        cbidtypespousedata= (Spinner) v.findViewById(R.id.cbidtypespousedata);
        cbreligionspouse= (Spinner) v.findViewById(R.id.cbreligionspouse);
        //end
        dpbirthdatespouse.addTextChangedListener(calenderBirthDate);

        //set value list
        initReligion("");
        try {
            setAdapterCombo("25730",cbsexspouse,true,"");
            setAdapterCombo("25703",cbtitlespouse,true,"");
            setAdapterCombo("25707",cbidtypespousedata,true,"");

        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }

        //add Watcher EditText and Spinner
        cbtitlespouse.setOnItemSelectedListener(new NewSpinner(cbtitlespouse));
        cbsexspouse.setOnItemSelectedListener(new NewSpinner(cbsexspouse));
        cbidtypespousedata.setOnItemSelectedListener(new NewSpinner(cbidtypespousedata));
        cbreligionspouse.setOnItemSelectedListener(new NewSpinner(cbreligionspouse));

        txtspousename.addTextChangedListener(new NewWatcher(txtspousename));
        txtspousenickname.addTextChangedListener(new NewWatcher(txtspousenickname));
        txtidnumberspouse.addTextChangedListener(new NewWatcher(txtidnumberspouse));
        txtbirthplacespouse.addTextChangedListener(new NewWatcher(txtbirthplacespouse));
        dpbirthdatespouse.addTextChangedListener(new NewWatcher(dpbirthdatespouse));
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtspousename.setEnabled(false);
            txtspousenickname.setEnabled(false);
            txtidnumberspouse.setEnabled(false);
            txtbirthplacespouse.setEnabled(false);
            cbtitlespouse.setEnabled(false);
            cbsexspouse.setEnabled(false);
            cbidtypespousedata.setEnabled(false);
            cbreligionspouse.setEnabled(false);
        }else{
            img_calender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDialog();
                }
            });

        }
        return v;
    }

    private void callDialog (){
        DialogFragmentDatePicker dialogFragment = new DialogFragmentDatePicker();
        dialogFragment.setTargetFragment(this, 0);
        dialogFragment.show(getActivity().getSupportFragmentManager(), TAG);
    }

    private void initReligion(String value){
        /*
        setup combo box religion
         */
        RealmQuery<Religion> religionRealmQuery = realm.where(Religion.class);
        RealmResults<Religion> religionRealmResults = religionRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<religionRealmResults.size();i++){
            if(value.equals(religionRealmResults.get(i).getRELIGION_ID())){
                selectedIndex=i;
                break;
            }
        }
        ReligionAdapter religionAdapter=new ReligionAdapter(getActivity(),R.layout.spinner_item,religionRealmResults);
        cbreligionspouse.setAdapter(religionAdapter);
        cbreligionspouse.setSelection(selectedIndex);

    }

    private CalenderWatcher calenderBirthDate = new CalenderWatcher() {
        @Override
        public void typedValue(String date) {

        }

        @Override
        public AppCompatEditText getView() {
            return dpbirthdatespouse;
        }
    };

    private void setupView (){

    }

    @Override
    public void onSetData(String date) {
        dpbirthdatespouse.setText(date);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;
        private NewSpinner(View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbtitlespouse :
                    orderEntry.setSPOUSE_TITLE(String.valueOf(cbtitlespouse.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbtitlespouse.getSelectedItemId());
                    break;
                case R.id.cbsexspouse :
                    orderEntry.setSPOUSE_GENDER(String.valueOf(cbsexspouse.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbsexspouse.getSelectedItemId());
                    break;
                case R.id.cbidtypespousedata :
                    orderEntry.setSPOUSE_ID_TYPE(String.valueOf(cbidtypespousedata.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbidtypespousedata.getSelectedItemId());
                    break;
                case R.id.cbreligionspouse :
                    orderEntry.setSPOUSE_RELIGION(String.valueOf(cbreligionspouse.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbreligionspouse.getSelectedItemId());
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtspousename :
                    orderEntry.setSPOUSE_NAME(result);
                    break;
                case R.id.txtspousenickname :
                    orderEntry.setSPOUSE_NICK_NAME(result);
                    break;
                case R.id.txtidnumberspouse :
                    orderEntry.setSPOUSE_ID_NO(result);
                    break;
                case R.id.txtbirthplacespouse :
                    orderEntry.setSPOUSE_BIRTH_PLACE(result);
                    break;
                case R.id.dpbirthdatespouse :
                    orderEntry.setSPOUSE_BIRTH_DATE(result);
                    break;
            }
        }
    }
}
