package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import io.realm.Realm;

public class NewOrder10 extends AppFragment implements Step {
    private static final String TAG = "NewOrder10";
    private EditText txtdepdname,txtschoolname,txtschooladdress,txtschoolrt,txtschoolrw,
            txtschoolkel,txtschoolkec,txtschoolcity,txtschoolzip,txtschoolphonearea1,
            txtschoolphonenb1,txtschoolphonearea2,txtschoolphonenb2;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    public static NewOrder10 newInstance() {
        return new NewOrder10();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {super.onCreate(savedInstanceState);}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order10, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init Component
        txtdepdname = (EditText) view.findViewById(R.id.txtdepdname) ;
        txtschoolname = (EditText) view.findViewById(R.id.txtschoolname) ;
        txtschooladdress = (EditText) view.findViewById(R.id.txtschooladdress) ;
        txtschoolrt = (EditText) view.findViewById(R.id.txtschoolrt) ;
        txtschoolrw = (EditText) view.findViewById(R.id.txtschoolrw) ;
        txtschoolkel = (EditText) view.findViewById(R.id.txtschoolkel) ;
        txtschoolkec = (EditText) view.findViewById(R.id.txtschoolkec) ;
        txtschoolcity = (EditText) view.findViewById(R.id.txtschoolcity) ;
        txtschoolzip = (EditText) view.findViewById(R.id.txtschoolzip) ;
        txtschoolphonearea1 = (EditText) view.findViewById(R.id.txtschoolphonearea1) ;
        txtschoolphonenb1 = (EditText) view.findViewById(R.id.txtschoolphonenb1) ;
        txtschoolphonearea2 = (EditText) view.findViewById(R.id.txtschoolphonearea2) ;
        txtschoolphonenb2 = (EditText) view.findViewById(R.id.txtschoolphonenb2) ;

        txtdepdname.addTextChangedListener(new NewWatcher(txtdepdname));
        txtschoolname.addTextChangedListener(new NewWatcher(txtschoolname));
        txtschooladdress.addTextChangedListener(new NewWatcher(txtschooladdress));
        txtschoolrt.addTextChangedListener(new NewWatcher(txtschoolrt));
        txtschoolrw.addTextChangedListener(new NewWatcher(txtschoolrw));
        txtschoolkel.addTextChangedListener(new NewWatcher(txtschoolkel));
        txtschoolkec.addTextChangedListener(new NewWatcher(txtschoolkec));
        txtschoolcity.addTextChangedListener(new NewWatcher(txtschoolcity));
        txtschoolzip.addTextChangedListener(new NewWatcher(txtschoolzip));
        txtschoolphonearea1.addTextChangedListener(new NewWatcher(txtschoolphonearea1));
        txtschoolphonenb1.addTextChangedListener(new NewWatcher(txtschoolphonenb1));
        txtschoolphonearea2.addTextChangedListener(new NewWatcher(txtschoolphonearea2));
        txtschoolphonenb2.addTextChangedListener(new NewWatcher(txtschoolphonenb2));

        return view;
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtdepdname.setEnabled(false);
            txtschoolname.setEnabled(false);
            txtschooladdress.setEnabled(false);
            txtschoolrt.setEnabled(false);
            txtschoolrw.setEnabled(false);
            txtschoolkel.setEnabled(false);
            txtschoolkec.setEnabled(false);
            txtschoolcity.setEnabled(false);
            txtschoolzip.setEnabled(false);
            txtschoolphonearea1.setEnabled(false);
            txtschoolphonenb1.setEnabled(false);
            txtschoolphonearea2.setEnabled(false);
            txtschoolphonenb2.setEnabled(false);
        }
        txtdepdname.setText(orderEntry.getCHILD_NAME());
        txtschoolname.setText(orderEntry.getCHILD_SCHOOL_NAME());
        txtschooladdress.setText(orderEntry.getCHILD_SCHOOL_NAME());
        txtschoolrt.setText(orderEntry.getCHILD_RT());
        txtschoolrw.setText(orderEntry.getCHILD_RW());
        txtschoolkel.setText(orderEntry.getCHILD_KELURAHAN());
        txtschoolkec.setText(orderEntry.getCHILD_KECAMATAN());
        txtschoolcity.setText(orderEntry.getCHILD_CITY());
        txtschoolzip.setText(orderEntry.getCHILD_POSCODE());
        txtschoolphonearea1.setText(orderEntry.getCHILD_PHONEONEAREA());
        txtschoolphonenb1.setText(orderEntry.getCHILD_PHONEONE());
        txtschoolphonearea2.setText(orderEntry.getCHILD_PHONETWOAREA());
        txtschoolphonenb2.setText(orderEntry.getCHILD_PHONETWO());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();

    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){

                case R.id.txtdepdname :
                    orderEntry.setCHILD_NAME(result);
                    break;
                case R.id.txtschoolname :
                    orderEntry.setCHILD_SCHOOL_NAME(result);
                    break;
                case R.id.txtschooladdress :
                    orderEntry.setCHILD_SCHOOL_NAME(result);
                    break;
                case R.id.txtschoolrt :
                    orderEntry.setCHILD_RT(result);
                    break;
                case R.id.txtschoolrw :
                    orderEntry.setCHILD_RW(result);
                    break;
                case R.id.txtschoolkel :
                    orderEntry.setCHILD_KELURAHAN(result);
                    break;
                case R.id.txtschoolkec :
                    orderEntry.setCHILD_KECAMATAN(result);
                    break;
                case R.id.txtschoolcity :
                    orderEntry.setCHILD_CITY(result);
                    break;
                case R.id.txtschoolzip :
                    orderEntry.setCHILD_POSCODE(result);
                    break;
                case R.id.txtschoolphonearea1 :
                    orderEntry.setCHILD_PHONEONEAREA(result);
                    break;
                case R.id.txtschoolphonenb1 :
                    orderEntry.setCHILD_PHONEONE(result);
                    break;
                case R.id.txtschoolphonearea2 :
                    orderEntry.setCHILD_PHONETWOAREA(result);
                    break;
                case R.id.txtschoolphonenb2 :
                    orderEntry.setCHILD_PHONETWO(result);
                    break;
            }
        }
    }
}

