package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class NewOrder12 extends AppFragment implements Step {

    private static final String TAG = "NewOrder12";
    private EditText txtdealername, txtbranchname, txtkota;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    public static NewOrder12 newInstance() {
        return new NewOrder12();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order12, container, false);
        orderEntry = newOrderInterface.getEntry();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //Init
        txtbranchname = (EditText) view.findViewById(R.id.p12_namaCabang);
        txtdealername = (EditText) view.findViewById(R.id.p12_namaDealer);
        txtkota = (EditText) view.findViewById(R.id.p12_kota);

        txtbranchname.addTextChangedListener(new NewWatcher(txtbranchname));
        txtdealername.addTextChangedListener(new NewWatcher(txtdealername));
        txtkota.addTextChangedListener(new NewWatcher(txtkota));

        return view;
    }

    private void setupView (){
        Log.i(TAG, "setupView: ");
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtbranchname.setEnabled(false);
            txtdealername.setEnabled(false);
            txtkota.setEnabled(false);
        }
        txtbranchname.setText(orderEntry.getBRANCH());
        txtdealername.setText(orderEntry.getDEALER_NAME());
        txtkota.setText(orderEntry.getCITY());
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.p12_namaDealer :
                    orderEntry.setDEALER_NAME(result);
                    break;
                case R.id.p12_namaCabang :
                    orderEntry.setBRANCH(result);
                    break;
                case R.id.p12_kota :
                    orderEntry.setCITY(result);
                    break;
            }
        }
    }
}
