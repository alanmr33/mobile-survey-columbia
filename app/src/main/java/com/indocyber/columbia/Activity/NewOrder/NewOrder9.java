package com.indocyber.columbia.Activity.NewOrder;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.indocyber.columbia.Abstract.AppFragment;
import com.indocyber.columbia.Adapter.ProvinceAdapter;
import com.indocyber.columbia.Adapter.RelationAdapter;
import com.indocyber.columbia.Contract.NewOrderInterface;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.Model.Province;
import com.indocyber.columbia.Model.Relation;
import com.indocyber.columbia.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class NewOrder9 extends AppFragment implements Step {

    private static final String TAG = "NewOrder9";
    private EditText txtcontactname,txtcontactaddress,txtrt,txtrw,txtkabupaten,rbtzip,
            txtphoneareacontactperson,txtphonecontactperson;
    private Spinner cbhascontactperson,cbpropinsi,cbreferencerelation;
    private LinearLayout layoutContainerHidden;
    private JSONArray has;
    private NewOrderInterface newOrderInterface;
    private OrderEntry orderEntry;
    private Realm realm;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        newOrderInterface = (NewOrderInterface) context;
    }

    public static NewOrder9 newInstance() {
        return new NewOrder9();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_new_order9, container, false);
        orderEntry = newOrderInterface.getEntry();
        realm = newOrderInterface.getRealm();
        //hide Keyboard
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //init component
        txtcontactname= (EditText) v.findViewById(R.id.txtcontactname);
        txtcontactaddress= (EditText) v.findViewById(R.id.txtcontactaddress);
        txtrt= (EditText) v.findViewById(R.id.txtrt);
        txtrw= (EditText) v.findViewById(R.id.txtrw);
        txtkabupaten= (EditText) v.findViewById(R.id.txtkabupaten);
        rbtzip= (EditText) v.findViewById(R.id.rbtzip);
        txtphoneareacontactperson= (EditText) v.findViewById(R.id.txtphoneareacontactperson);
        txtphonecontactperson= (EditText) v.findViewById(R.id.txtphonecontactperson);
        cbhascontactperson= (Spinner) v.findViewById(R.id.cbhascontactperson);
        cbpropinsi= (Spinner) v.findViewById(R.id.cbpropinsi);
        cbreferencerelation= (Spinner) v.findViewById(R.id.cbreferencerelation);
        layoutContainerHidden= (LinearLayout) v.findViewById(R.id.layoutContainerHidden);

        //init list value
        try {
            setAdapterCombo("25073",cbhascontactperson,true,"");
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        initProvince("");
        initRelation("");

        //add Watcher EditText and Spinner
        cbpropinsi.setOnItemSelectedListener(new NewSpinner(cbpropinsi));
        cbreferencerelation.setOnItemSelectedListener(new NewSpinner(cbreferencerelation));
        cbhascontactperson.setOnItemSelectedListener(new NewSpinner(cbhascontactperson));

        txtcontactname.addTextChangedListener(new NewWatcher(txtcontactname));
        txtcontactaddress.addTextChangedListener(new NewWatcher(txtcontactaddress));
        txtrt.addTextChangedListener(new NewWatcher(txtrt));
        txtrw.addTextChangedListener(new NewWatcher(txtrw));
        txtkabupaten.addTextChangedListener(new NewWatcher(txtkabupaten));
        rbtzip.addTextChangedListener(new NewWatcher(rbtzip));
        txtphoneareacontactperson.addTextChangedListener(new NewWatcher(txtphoneareacontactperson));
        txtphonecontactperson.addTextChangedListener(new NewWatcher(txtphonecontactperson));

        return v;
    }

    private void setupView (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            txtcontactname.setEnabled(false);
            txtcontactaddress.setEnabled(false);
            txtrt.setEnabled(false);
            txtrw.setEnabled(false);
            txtkabupaten.setEnabled(false);
            rbtzip.setEnabled(false);
            txtphoneareacontactperson.setEnabled(false);
            txtphonecontactperson.setEnabled(false);
            cbpropinsi.setEnabled(false);
            cbreferencerelation.setEnabled(false);
            cbhascontactperson.setEnabled(false);
        }
        txtcontactname.setText(orderEntry.getFAM_NAME());
        txtcontactaddress.setText(orderEntry.getFAM_ADDRESS());
        txtrt.setText(orderEntry.getFAM_RT());
        txtrw.setText(orderEntry.getFAM_RW());
        txtkabupaten.setText(orderEntry.getFAM_KAB());
        rbtzip.setText(orderEntry.getFAM_POSCODE());
        txtphoneareacontactperson.setText(orderEntry.getFAM_TELP_AREACODE());
        txtphonecontactperson.setText(orderEntry.getFAM_TELP_NO());
    }

    private void initProvince(String value){
        /*
        setup combo box province
         */
        RealmQuery<Province> provinceRealmQuery = realm.where(Province.class);
        RealmResults<Province> provinceRealmResults = provinceRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<provinceRealmResults.size();i++){
            if(value.equals(provinceRealmResults.get(i).getPROVINCE_ID())){
                selectedIndex=i;
                break;
            }
        }
        ProvinceAdapter provinceAdapter=new ProvinceAdapter(getActivity(),R.layout.spinner_item,provinceRealmResults);
        cbpropinsi.setAdapter(provinceAdapter);
        cbpropinsi.setSelection(selectedIndex);
    }
    private void initRelation(String value){
        /*
        setup combo box province
         */
        RealmQuery<Relation> relationRealmQuery = realm.where(Relation.class);
        RealmResults<Relation> relationRealmResults=relationRealmQuery.findAll();
        //set selected Item
        int selectedIndex=0;
        for (int i=0;i<relationRealmResults.size();i++){
            if(value.equals(relationRealmResults.get(i).getRELATION_ID())){
                selectedIndex=i;
                break;
            }
        }
        RelationAdapter relationAdapter=new RelationAdapter(getActivity(),R.layout.spinner_item,relationRealmResults);
        cbreferencerelation.setAdapter(relationAdapter);
        cbreferencerelation.setSelection(selectedIndex);
    }

    private VerificationError validate (){
        if(orderEntry.getMOBILE_STATUS().equals("PENDING") || orderEntry.getMOBILE_STATUS().equals("SUBMIT")){
            return null;
        }
        String errorMessage = "";
        try {
            if (has == null){
                has = (JSONArray) cbhascontactperson.getSelectedItem();
            }
            if ((has != null) && (has.getString(0).equals("ya"))) {
                if (TextUtils.isEmpty(txtphoneareacontactperson.getText())) {
                    txtphoneareacontactperson.setError(NewOrderActivity.ErrorMessage);
                    errorMessage = NewOrderActivity.ErrorMessageLong;
                }
                if (TextUtils.isEmpty(txtphonecontactperson.getText())) {
                    txtphonecontactperson.setError(NewOrderActivity.ErrorMessage);
                    errorMessage = NewOrderActivity.ErrorMessageLong;
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        return !TextUtils.isEmpty(errorMessage) ? new VerificationError(errorMessage) : null;
    }

    @Override
    public VerificationError verifyStep() {
        return validate();
    }

    @Override
    public void onSelected() {
        setupView();
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        Toast.makeText(getContext(),error.getErrorMessage(),Toast.LENGTH_SHORT).show();
    }

    private class NewSpinner implements AdapterView.OnItemSelectedListener{
        private View v;

        private NewSpinner(View view){
            this.v = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
            switch (v.getId()){
                case R.id.cbhascontactperson :
                    has = (JSONArray) cbhascontactperson.getSelectedItem();
                    try {
                        if(has.getString(0).equals("ya")){
                            layoutContainerHidden.setVisibility(View.VISIBLE);
                        }else{
                            layoutContainerHidden.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        Log.e(getClass().getSimpleName(),e.getMessage());
                    }
                    orderEntry.setFAM_CONTACT(String.valueOf(cbhascontactperson.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbhascontactperson.getSelectedItemId());
                    break;
                case R.id.cbpropinsi :
                    orderEntry.setFAM_PROVINCE(String.valueOf(cbpropinsi.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbpropinsi.getSelectedItemId());
                    break;
                case R.id.cbreferencerelation :
                    orderEntry.setFAM_RELATION(String.valueOf(cbreferencerelation.getSelectedItemId()));
                    Log.d(TAG, "execute: "+cbreferencerelation.getSelectedItemId());
                    break;
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class NewWatcher implements TextWatcher {
        private View view;

        private NewWatcher (View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String result = s.toString();
            Log.d(TAG, "afterTextChanged: "+result);
            switch (view.getId()){
                case R.id.txtcontactname :
                    orderEntry.setFAM_NAME(result);
                    break;
                case R.id.txtcontactaddress :
                    orderEntry.setFAM_ADDRESS(result);
                    break;
                case R.id.txtrt :
                    orderEntry.setFAM_RT(result);
                    break;
                case R.id.txtrw :
                    orderEntry.setFAM_RW(result);
                    break;
                case R.id.txtkabupaten :
                    orderEntry.setFAM_KAB(result);
                    break;
                case R.id.rbtzip :
                    orderEntry.setFAM_POSCODE(result);
                    break;
                case R.id.txtphoneareacontactperson :
                    orderEntry.setFAM_TELP_AREACODE(result);
                    break;
                case R.id.txtphonecontactperson :
                    orderEntry.setFAM_TELP_NO(result);
                    break;
            }
        }
    }
}
