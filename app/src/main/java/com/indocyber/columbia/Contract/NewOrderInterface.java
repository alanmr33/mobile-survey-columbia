package com.indocyber.columbia.Contract;

import com.indocyber.columbia.Model.OrderEntry;

import io.realm.Realm;

/**
 * Created by Frendi on 04/08/2017.
 */

public interface NewOrderInterface {
    OrderEntry getEntry();
    Realm getRealm();
}
