package com.indocyber.columbia.Contract;

/**
 * Created by Frendi on 10/08/2017.
 */

public interface DialogOnClick {
    void onPositiveClick(Object object);
}
