package com.indocyber.columbia;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

/**
 * Created by Indocyber on 19/07/2017.
 */

public class App extends Application {
    //if build debug some function is bypass.
    public static final boolean DEBUG = BuildConfig.DEBUG;
    public static final String KEY_MESSAGE = "responseMessage";
    public static final String KEY_TITLE = "title";
    /*
    init multi dex when attach context
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

}
