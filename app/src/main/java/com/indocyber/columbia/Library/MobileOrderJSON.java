package com.indocyber.columbia.Library;

import com.indocyber.columbia.Model.OrderEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmResults;

/**
 * Created by Indocyber on 28/07/2017.
 */

public class MobileOrderJSON {
    private RealmResults<OrderEntry> orderEntries;
    private JSONArray jsonArray;
    public MobileOrderJSON(RealmResults<OrderEntry> orderEntries){
        this.orderEntries=orderEntries;
    }
    public String toJSON() throws JSONException {
        for (int i=0;i<jsonArray.length();i++){
            JSONObject temp=new JSONObject();
            temp.put("DATA_MAP_NAME",orderEntries.get(i).getDATA_MAP_NAME());
            temp.put("ORDER_ID",orderEntries.get(i).getORDERID());
            temp.put("MODULE_ID",orderEntries.get(i).getMODULE_ID());
            temp.put("ITEM_TYPE",orderEntries.get(i).getITEM_TYPE());
            temp.put("USER_ID",orderEntries.get(i).getUSER_ID());
            temp.put("STATUS",orderEntries.get(i).getSTATUS());
            temp.put("STATUS_UPDATE",orderEntries.get(i).getSTATUS_UPDATE());
            temp.put("TIMESTAMPS",orderEntries.get(i).getTIMESTAMPS());
            temp.put("LAST_TRX",orderEntries.get(i).getLAST_TRX());
            temp.put("BRANCH_ID",orderEntries.get(i).getBRANCH_ID());
            temp.put("STATUS_KUNJUNGAN",orderEntries.get(i).getSTATUS_KUNJUNGAN());
            temp.put("BLANK",orderEntries.get(i).getDATA_MAP_NAME());
            temp.put("NAME",orderEntries.get(i).getBLANK());
            temp.put("PERS_IDENTIFY_TYPE",orderEntries.get(i).getPERS_IDENTIFY_TYPE());
            temp.put("IDENTIFY_NO",orderEntries.get(i).getIDENTIFY_NO());
            temp.put("NPWP_NO",orderEntries.get(i).getNPWP_NO());
            temp.put("CUST_BIRTH_DATE",orderEntries.get(i).getCUST_BIRTH_DATE());
            temp.put("PERS_RELIGION",orderEntries.get(i).getPERS_RELIGION());
            temp.put("RT",orderEntries.get(i).getRT());
            temp.put("RW",orderEntries.get(i).getRW());
            temp.put("KELURAHAN",orderEntries.get(i).getKELURAHAN());
            temp.put("KECAMATAN",orderEntries.get(i).getKECAMATAN());
            temp.put("KABKODYA",orderEntries.get(i).getKABKODYA());
            temp.put("PROVINCE",orderEntries.get(i).getPROVINCE());
            temp.put("PERSPOSTALCODE",orderEntries.get(i).getPERSPOSTALCODE());
            temp.put("SANDIDATIII",orderEntries.get(i).getSANDIDATIII());
            temp.put("HOME_STATUS",orderEntries.get(i).getHOME_STATUS());
            temp.put("TELEPHONE_AREA",orderEntries.get(i).getTELEPHONE_AREA());
            temp.put("TELEPHONE_NUMBER",orderEntries.get(i).getTELEPHONE_NUMBER());
            temp.put("HANDPHONE_PREFIX",orderEntries.get(i).getHANDPHONE_PREFIX());
            temp.put("HANDPHONE_NUMBER",orderEntries.get(i).getHANDPHONE_NUMBER());
            temp.put("OCCUPATION",orderEntries.get(i).getOCCUPATION());
            temp.put("JOBADDRESS",orderEntries.get(i).getJOBADDRESS());
            temp.put("JOBPOSTALCODE",orderEntries.get(i).getJOBPOSTALCODE());
            temp.put("LENGHOFWORK",orderEntries.get(i).getLENGHOFWORK());
            temp.put("FAM_IDENTIFY_TYPE",orderEntries.get(i).getFAM_IDENTIFY_TYPE());
            temp.put("FAM_BIRTH_DATE",orderEntries.get(i).getFAM_BIRTH_DATE());
            temp.put("GUAR_ADDRESS",orderEntries.get(i).getGUAR_ADDRESS());
            temp.put("GUAR_RELIGION",orderEntries.get(i).getGUAR_RELIGION());
            temp.put("PENJ_ADDRESS",orderEntries.get(i).getPENJ_ADDRESS());
            temp.put("GROSS_FIX_INCOME",orderEntries.get(i).getGROSS_FIX_INCOME());
            temp.put("ADDITIONAL_INCOME",orderEntries.get(i).getADDITIONAL_INCOME());
            temp.put("PRODUCT_CODE",orderEntries.get(i).getPRODUCT_CODE());
            temp.put("DEALER_NAME",orderEntries.get(i).getDEALER_NAME());
            temp.put("BRANCH",orderEntries.get(i).getBRANCH());
            temp.put("CITY",orderEntries.get(i).getCITY());
            temp.put("BRAND",orderEntries.get(i).getBRAND());
            temp.put("MODEL",orderEntries.get(i).getMODEL());
            temp.put("TYPE",orderEntries.get(i).getTYPE());
            temp.put("YEAR",orderEntries.get(i).getYEAR());
            temp.put("DESCRIPTION",orderEntries.get(i).getDESCRIPTION());
            temp.put("ORDER_DATE",orderEntries.get(i).getORDER_DATE());
            temp.put("CASH_PRICE",orderEntries.get(i).getCASH_PRICE());
            temp.put("TENOR",orderEntries.get(i).getTENOR());
            temp.put("JENIS_PEKERJAAN",orderEntries.get(i).getJENIS_PEKERJAAN());
            temp.put("PENGHASILAN_TETAP",orderEntries.get(i).getPENGHASILAN_TETAP());
            temp.put("ADD_INCOME_SRC",orderEntries.get(i).getADD_INCOME_SRC());
            temp.put("FLAG_DELIVER",orderEntries.get(i).getFLAG_DELIVER());
            temp.put("FLAG_UPDATE_CLOSE",orderEntries.get(i).getFLAG_UPDATE_CLOSE());
            temp.put("NICKNAME",orderEntries.get(i).getNICKNAME());
            temp.put("MOTHER_NAME",orderEntries.get(i).getMOTHER_NAME());
            temp.put("FIRST_TITLE",orderEntries.get(i).getFIRST_TITLE());
            temp.put("GENDER",orderEntries.get(i).getGENDER());
            temp.put("BIRTH_PLACE",orderEntries.get(i).getBIRTH_PLACE());
            temp.put("DURATION_STAY_YEAR",orderEntries.get(i).getDURATION_STAY_YEAR());
            temp.put("DURATION_STAY_MONTH",orderEntries.get(i).getDURATION_STAY_MONTH());
            temp.put("INDUSTRY_CODE",orderEntries.get(i).getINDUSTRY_CODE());
            temp.put("OFFICE_NAME",orderEntries.get(i).getOFFICE_NAME());
            temp.put("TGL_DELIVERED",orderEntries.get(i).getTGL_DELIVERED());
            temp.put("TGL_CLOSE",orderEntries.get(i).getTGL_CLOSE());
            temp.put("TGL_CLOSE_SEND",orderEntries.get(i).getTGL_CLOSE_SEND());
            temp.put("MARITAL_STATUS",orderEntries.get(i).getMARITAL_STATUS());
            temp.put("OFFICE_PROVINCE",orderEntries.get(i).getPROVINCE());
            temp.put("OFFICE_CITY",orderEntries.get(i).getOFFICE_CITY());
            temp.put("CUSTOMER_EDUCATION",orderEntries.get(i).getCUSTOMER_EDUCATION());
            temp.put("DURATION_JOB_MONTH",orderEntries.get(i).getDURATION_JOB_MONTH());
            temp.put("CUSTOMER_RESI_ADDRESS",orderEntries.get(i).getCUSTOMER_RESI_ADDRESS());
            temp.put("CUSTOMER_RESI_RT",orderEntries.get(i).getCUSTOMER_RESI_RT());
            temp.put("CUSTOMER_RESI_RW",orderEntries.get(i).getCUSTOMER_RESI_RW());
            temp.put("CUSTOMER_RESI_VILLAGE",orderEntries.get(i).getCUSTOMER_RESI_VILLAGE());
            temp.put("CUSTOMER_RESI_DISTRICT",orderEntries.get(i).getCUSTOMER_RESI_DISTRICT());
            temp.put("CUSTOMER_RESI_REGENCY",orderEntries.get(i).getCUSTOMER_RESI_REGENCY());
            temp.put("CUSTOMER_RESI_PROVINCE",orderEntries.get(i).getCUSTOMER_RESI_PROVINCE());
            temp.put("CUSTOMER_RESI_POSTCODE",orderEntries.get(i).getCUSTOMER_RESI_POSTCODE());
            temp.put("CUSTOMER_RESI_CITY",orderEntries.get(i).getCUSTOMER_RESI_CITY());
            temp.put("DOWNPAYMENT_PERC",orderEntries.get(i).getDOWNPAYMENT_PERC());
            temp.put("DOWNPAYMENT_AMT",orderEntries.get(i).getDOWNPAYMENT_AMT());
            temp.put("INSTALLMENT_AMT",orderEntries.get(i).getINSTALLMENT_AMT());
            temp.put("INSTALLMENT_TYPE",orderEntries.get(i).getINSTALLMENT_TYPE());
            temp.put("RENT_PAYMENT_TYPE",orderEntries.get(i).getRENT_PAYMENT_TYPE());
            temp.put("ASSET_NO",orderEntries.get(i).getASSET_NO());
            temp.put("ADD_INCOME_SOURCE",orderEntries.get(i).getASSET_CONDITION());
            temp.put("SPOUSE_NAME",orderEntries.get(i).getSPOUSE_NAME());
            temp.put("SPOUSE_ID_TYPE",orderEntries.get(i).getSPOUSE_ID_TYPE());
            temp.put("SPOUSE_ID_NO",orderEntries.get(i).getSPOUSE_ID_NO());
            temp.put("CHILD_NAME",orderEntries.get(i).getCHILD_NAME());
            temp.put("CHILD_SCHOOL_NAME",orderEntries.get(i).getCHILD_NAME());
            temp.put("CHILD_SCHOOL_ADDRESS",orderEntries.get(i).getCHILD_SCHOOL_ADDRESS());
            temp.put("SAME_CUSTOMER_ID_ADDRESS",orderEntries.get(i).getSAME_CUSTOMER_ID_ADDRESS());
            temp.put("ASSET_CONDITION",orderEntries.get(i).getASSET_CONDITION());
            temp.put("INVOICE_CORRESPONDENCE_NO",orderEntries.get(i).getINVOICE_CORRESPONDENCE_NO());
            jsonArray.put(i,temp);
        }
        return jsonArray.toString();
    }
}
