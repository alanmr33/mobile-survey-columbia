package com.indocyber.columbia.Service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.indocyber.columbia.Activity.Main;
import com.indocyber.columbia.Activity.Reconnect;
import com.indocyber.columbia.Config.Global;
import com.indocyber.columbia.Library.Session;
import com.indocyber.columbia.Model.OrderEntry;
import com.indocyber.columbia.R;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.JsonStreamerEntity;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Indocyber on 24/07/2017.
 */

public class SyncRecord extends IntentService {
    private SyncHttpClient httpClient;
    private Realm realm;
    private Global config;
    private Session session;
    public SyncRecord() {
        super("Sync Record");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
         /*
        create all library on create service
         */
        Realm.init(getApplicationContext());
        RealmConfiguration configRealm = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        this.realm=Realm.getInstance(configRealm);
        this.httpClient=new SyncHttpClient();
        this.httpClient.setConnectTimeout(300000);
        this.httpClient.setResponseTimeout(300000);
        this.httpClient.setMaxRetriesAndTimeout(1,300000);
        this.config=new Global();
        this.session=new Session(this);
        //query result
        RealmQuery<OrderEntry> query=realm.where(OrderEntry.class);
        query.equalTo("MOBILE_STATUS","PENDING");
        final RealmResults<OrderEntry> results=query.findAll();
        /*
        get latest order entry
         */
        final List<OrderEntry> orderEntries=realm.copyFromRealm(results);
        RequestParams params = new RequestParams();
        params.put("token", session.getToken());
        if(orderEntries.size()>0) {
            params.put("toSync", new Gson().toJson(orderEntries));
            try {
                Log.i("JSON Sync","Panjang "+new JSONArray(new Gson().toJson(orderEntries)).length());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            params.put("toSync", new JSONArray().toString());
            Log.i("JSON Sync","No Data");
        }
        httpClient.post(this, session.getUrl() + config.SEND_ENDPOINT, params, new JsonHttpResponseHandler() {
            /*
            get all order
            */
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                try {
                    if(response.getBoolean("status")){
                        int nexData=0;
                        JSONArray orderList=response.getJSONArray("data");
                        //loop get data from server
                        realm.beginTransaction();
                        for (int i=0;i<orderList.length();i++){
                            RealmQuery<OrderEntry> query=realm.where(OrderEntry.class);
                            query.equalTo("ORDER_ID",orderList.getJSONObject(i).getString("ORDER_ID"));
                            RealmResults<OrderEntry> check = query.findAll();
                            // if isset result don't insert it
                            if(check.size()<1){
                                orderList.getJSONObject(i).put("MOBILE_STATUS","NEW");
                                realm.createObjectFromJson(OrderEntry.class,orderList.getJSONObject(i));
                                nexData++;
                            }else if(orderList.getJSONObject(i).get("STATUS").equals("SUBMIT")){
                                realm.createOrUpdateObjectFromJson(OrderEntry.class,orderList.getJSONObject(i));
                                nexData++;
                            }else if(orderList.getJSONObject(i).get("STATUS").equals("REROUTE")){
                                check.get(i).deleteFromRealm();
                                nexData++;
                            }
                        }
                        for(int i=0;i<orderEntries.size();i++){
                            if(results.size()>0) {
                                results.get(i).setMOBILE_STATUS("SUBMIT");
//                            realm.copyToRealmOrUpdate(results.get(i));
                                sendNotification((i + 1) + " data has been send.");
                            }
                        }
                        if(nexData>0){
                            sendNotification("you got new "+nexData+" data");
                        }
                        realm.commitTransaction();
                        realm.close();
                    }
                } catch (JSONException e) {
                    Log.e(getClass().getSimpleName(), e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                sendNotification("Fail to sync. Check your connection or tap to reconnect !");
                stopSelf();
                Log.e(getClass().getSimpleName(), throwable.getMessage());
            }

            @Override
            public void onFailure(int status, Header[] headers, Throwable throwable, JSONObject response) {
                sendNotification("Fail to sync. Check your connection or tap to reconnect !");
                stopSelf();
                Log.e(getClass().getSimpleName(), throwable.getMessage());
            }
        });
    }
    private void sendNotification(String message){
        Intent intent = new Intent(this, Reconnect.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0 /* request code */, intent,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notif)
                .setContentTitle("Surveryor App")
                .setContentText(message)
                .setAutoCancel(true)
                .setOngoing(false)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1001, notificationBuilder.build());
    }
}
