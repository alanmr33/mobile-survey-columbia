package com.indocyber.columbia.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.indocyber.columbia.Library.Session;

/**
 * Created by Indocyber on 18/08/2017.
 */

public class Autostart extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      CreateIntent(context);
    }
    public void CreateIntent(final Context context){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent sync = new Intent(context, SyncRecord.class);
                context.startService(sync);
            }
        },600000);
    }
}
