package com.indocyber.columbia.Abstract;

import android.content.Intent;
import android.os.Bundle;
import com.indocyber.columbia.Service.SyncRecord;

/**
 * Created by Indocyber on 24/07/2017.
 */

public class UserLoginActivity extends AppActivity {
    private Intent syncRecord;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        start service order entry
         */
        syncRecord = new Intent(this, SyncRecord.class);
        startService(syncRecord);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(syncRecord);
    }

}
