package com.indocyber.columbia.Abstract;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.indocyber.columbia.Activity.NewOrder.NewOrderActivity;
import com.indocyber.columbia.Adapter.AdapterList;
import com.indocyber.columbia.Model.Combo;
import com.indocyber.columbia.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Indocyber on 24/07/2017.
 */

public class AppFragment extends android.support.v4.app.Fragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i(getClass().getSimpleName(),"===onAttach==");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(getClass().getSimpleName(),"===onAttach==");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(getClass().getSimpleName(),"===onCreateView==");

        return inflater.inflate(R.layout.fragment_new_order1, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(getClass().getSimpleName(),"===onActivityCreated==");

    }
    @Override
    public void onStart() {
        super.onStart();
        Log.i(getClass().getSimpleName(),"===onStart==");
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.i(getClass().getSimpleName(),"===onPause==");
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.i(getClass().getSimpleName(),"===onResume==");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.i(getClass().getSimpleName(),"===onStop==");
    }
    @Override
    public void onDestroyView(){
        super.onDestroyView();
        Log.i(getClass().getSimpleName(),"===onDestroyView==");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i(getClass().getSimpleName(),"===onDestroy==");
    }
    @Override
    public void onDetach(){
        super.onDetach();
        Log.i(getClass().getSimpleName(),"===onDestroy==");
    }
    public void setAdapterCombo(String idcombo, Spinner spinner, Boolean enabled, String selected) throws JSONException {
        //query from realm then set adapter
        RealmQuery<Combo> query=((NewOrderActivity)getActivity()).realm.where(Combo.class);
        query.equalTo("ComboxID",idcombo);
        RealmResults<Combo> results=query.findAll();
        Log.i(idcombo,results.toString());
        if(results.size()>0) {
            /*
            store to json array
             */
            JSONArray valueList=new JSONArray(results.get(0).getValueList());
            int selectedIndex=0;
            //loop to set value
            for (int i=0;i<valueList.length();i++){
                JSONArray value=valueList.getJSONArray(i);
                if(value.getString(0).equals(selected)){
                    selectedIndex=i;
                    break;
                }
            }
            spinner.setEnabled(enabled);
            AdapterList adapterList = new AdapterList(getActivity(), R.layout.spinner_item, valueList, 1, 0);
            spinner.setAdapter(adapterList);
            spinner.setSelection(selectedIndex);
        }else{
            Log.e(getClass().getSimpleName(),"Combobox ID "+idcombo+" No Data");
        }
    }
    public void setAdapterComboStringAll(String idcombo, Spinner spinner, Boolean enabled, String selected) throws JSONException {
        //query from realm then set adapter
        RealmQuery<Combo> query=((NewOrderActivity)getActivity()).realm.where(Combo.class);
        query.equalTo("ComboxID",idcombo);
        RealmResults<Combo> results=query.findAll();
        Log.i(idcombo,results.toString());
        if(results.size()>0) {
            /*
            store to json array
             */
            ArrayList<String> values=new ArrayList<>();
            JSONArray valueList=new JSONArray(results.get(0).getValueList());
            int selectedIndex=0;
            //loop to set value
            for (int i=0;i<valueList.length();i++){
                JSONArray value=valueList.getJSONArray(i);
                if(value.getString(0).equals(selected)){
                    selectedIndex=i;
                }
                values.add(i,value.getString(0));
            }
            spinner.setEnabled(enabled);
            ArrayAdapter<String> adapter= new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, values);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
            spinner.setSelection(selectedIndex);
        }else{
            Log.e(getClass().getSimpleName(),"Combobox ID "+idcombo+" No Data");
        }
    }
}
