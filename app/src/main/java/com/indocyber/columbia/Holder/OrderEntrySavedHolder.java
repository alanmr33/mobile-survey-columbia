package com.indocyber.columbia.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.columbia.R;

/**
 * Created by Indocyber on 26/07/2017.
 */

public class OrderEntrySavedHolder extends RecyclerView.ViewHolder {
    public TextView txtOrderID,txtNama,txtAlamat;
    public LinearLayout mainLayout;
    public OrderEntrySavedHolder(View itemView) {
        super(itemView);
        txtOrderID= (TextView) itemView.findViewById(R.id.txtNewOrderID);
        txtNama= (TextView) itemView.findViewById(R.id.txtNewOrderNama);
        txtAlamat= (TextView) itemView.findViewById(R.id.txtNewOrderAlamat);
        mainLayout= (LinearLayout) itemView.findViewById(R.id.itemNewOrder);
    }
}
