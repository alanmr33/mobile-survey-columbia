package com.indocyber.columbia.DialogFragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.indocyber.columbia.R;

import com.indocyber.columbia.App;
import com.indocyber.columbia.Contract.DialogOnClick;

/**
 * Created by Frendi on 10/08/2017.
 */

public class DialogFragmentConfirmation extends DialogFragment {
    private DialogOnClick dialogOnclick;

    private String message, title;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        message = getArguments().getString(App.KEY_MESSAGE);
        title = getArguments().getString(App.KEY_TITLE);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_dialog_confirmation, null);
        TextView titleTextView = (TextView) view.findViewById(R.id.dialog_confirmation_title);
        TextView messageTextView = (TextView) view.findViewById(R.id.dialog_confirmation_message);
        titleTextView.setText(title);
        messageTextView.setText(message);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                        if (dialogOnclick != null) {
                            dialogOnclick.onPositiveClick(new Object());
                        }
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
        return builder.create();
    }

    public void setDialogOnclick(DialogOnClick dialogOnclick) {
        this.dialogOnclick = dialogOnclick;
    }
}
