package com.indocyber.columbia.DialogFragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;

import com.indocyber.columbia.Contract.DateContract;
import com.indocyber.columbia.Util.DateStringUtil;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.indocyber.columbia.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Frendi on 28/07/2017.
 */

public class DialogFragmentDatePicker extends DialogFragment implements OnDateSelectedListener {

    private static final String TAG = DialogFragmentDatePicker.class.getSimpleName();
    private MaterialCalendarView materialCalendarView;
    private static final DateFormat FORMATTER = new SimpleDateFormat(DateStringUtil.DATE_FORMAT);
    private DateContract dateContract;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        View view = getActivity()
                .getLayoutInflater()
                .inflate(R.layout.custom_date_picker, null);

        materialCalendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        materialCalendarView.setOnDateChangedListener(this);

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton( "ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dateContract != null) {
                            dateContract.onSetData(getSelectedDatesString());
                        }
                        dismiss();
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .create();
        try {
            alertDialog.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return alertDialog ;
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        if (dateContract != null) {
            dateContract.onSetData(getSelectedDatesString());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dateContract = (DateContract) getTargetFragment();
    }

    private String getSelectedDatesString() {
        CalendarDay date = materialCalendarView.getSelectedDate();
        if (date == null) {
            return "No Selection";
        }
        return FORMATTER.format(date.getDate());
    }
}
