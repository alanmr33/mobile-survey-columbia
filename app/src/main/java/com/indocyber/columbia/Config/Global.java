package com.indocyber.columbia.Config;

/**
 * Created by Indocyber on 14/07/2017.
 */

public class Global {
    public final String PREF_NAME="columbia-app";
    public final int SPLASH=1000;
    public final int CODE_PERMISSION=1000;
    public final String LOGIN_ENDPOINT="/user/login";
    public final String CPASSWORD_ENDPOINT="/user/change_settings";
    public final String SYNC_ENDPOINT="/sync/all";
    public final String SEND_ENDPOINT="/sync/send";
    public final String OBJECT_ENDPOINT="/sync/get_object";
    public final String LOG_ENDPOINT="/track";
}
