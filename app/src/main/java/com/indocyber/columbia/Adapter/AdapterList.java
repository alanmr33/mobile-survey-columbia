package com.indocyber.columbia.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.indocyber.columbia.Model.Combo;
import com.indocyber.columbia.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Indocyber on 25/07/2017.
 */

public class AdapterList extends ArrayAdapter<JSONArray> {
    private JSONArray data;
    private int layout;
    private Context context;
    private LayoutInflater inflater;
    private int index;
    private int value;
    public AdapterList(Context context, int resource,JSONArray data,int index,int value) {
        super(context, resource);
        this.layout=resource;
        this.context=context;
        this.data=data;
        this.index=index;
        this.value=value;
        this.inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(layout, parent, false);
        /*
        get list data
         */
        try {
            JSONArray arrayValue=data.getJSONArray(position);
            TextView text1= (TextView) row.findViewById(R.id.textIndex);
            text1.setText(arrayValue.getString(index));
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        return row;
    }
    @Override
    public int getCount() {
        return data.length();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public JSONArray getItem(int position){
        try {
            return data.getJSONArray(position);
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        return null;
    }
}
