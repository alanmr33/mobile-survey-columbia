package com.indocyber.columbia.Adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

/**
 * Created by Frendi on 31/07/2017.
 */

public class ViewpagerRunner extends ViewPager {

    private Context context;
    private boolean isPageEnabled;

    public ViewpagerRunner (Context context){
        super(context);
    }

    public ViewpagerRunner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.isPageEnabled = false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (this.isPageEnabled) {
            return super.onTouchEvent(ev);
        }
        Toast.makeText(context, "Please fill in the details, then swipe !",
                Toast.LENGTH_LONG).show();
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return this.isPageEnabled && super.onInterceptTouchEvent(ev);

    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return this.isPageEnabled && super.canScrollHorizontally(direction);
    }

    public boolean getpageEnabled (){
        return isPageEnabled;
    }

    public void setPageEnabled (boolean value){
        isPageEnabled = value;
    }
}
