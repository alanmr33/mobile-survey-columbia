package com.indocyber.columbia.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indocyber.columbia.Holder.OrderEntryNewHolder;
import com.indocyber.columbia.Model.OrderEntry;

import io.realm.RealmResults;

/**
 * Created by Indocyber on 26/07/2017.
 */

public class OrderEntryAdapter extends RecyclerView.Adapter<OrderEntryNewHolder> {
    private int layout;
    private RealmResults<OrderEntry> orderEntries;
    private Context context;
    private Activity activity;
    public OrderEntryAdapter(RealmResults<OrderEntry> orderEntries, int layout, Context context, Activity activity){
        this.orderEntries=orderEntries;
        this.layout=layout;
        this.context=context;
        this.activity=activity;
    }
    @Override
    public OrderEntryNewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new OrderEntryNewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderEntryNewHolder holder, int position) {
        /*
        set item data by active item
         */
        final OrderEntry orderEntry=orderEntries.get(position);
        holder.txtOrderID.setText(orderEntry.getORDERID());
        holder.txtNama.setText(orderEntry.getNAME());
        holder.txtAlamat.setText(orderEntry.getCUSTOMER_RESI_ADDRESS());
    }

    @Override
    public int getItemCount() {
        return this.orderEntries.size();
    }
}

