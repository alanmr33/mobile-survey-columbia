package com.indocyber.columbia.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.indocyber.columbia.Model.Relation;
import com.indocyber.columbia.Model.StatusKunjungan;
import com.indocyber.columbia.R;

import io.realm.RealmResults;

/**
 * Created by Indocyber on 27/07/2017.
 */

public class ReasonAdapter extends ArrayAdapter<StatusKunjungan> {
    private RealmResults<StatusKunjungan> statusKunjungen;
    private Context context;
    private int layout;
    private LayoutInflater inflater;
    public ReasonAdapter(Context context,int layout,RealmResults<StatusKunjungan> statusKunjungen) {
        super(context, layout);
        this.context=context;
        this.layout=layout;
        this.statusKunjungen=statusKunjungen;
        this.inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(layout, parent, false);
        TextView text1= (TextView) row.findViewById(R.id.textIndex);
        text1.setText(statusKunjungen.get(position).getSTATUS_NAME());
        return row;
    }
    @Override
    public int getCount() {
        return statusKunjungen.size();
    }
    @Override
    public long getItemId(int position) {
        return Long.valueOf(statusKunjungen.get(position).getSTATUS_ID());
    }
}

